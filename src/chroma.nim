#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import macros, json, os, marshal, streams, tables, times, typetraits, sequtils, random
import sdl2, sdl2/[ttf, image, audio]
import chroma/[jankcrypt, displayable]
import chroma/displayables/[characterd, simpleimage, animation, audioanimation, dialoguedisp, dialoguetext, vbox, hbox, menudisp, backgrounddisp, frame, imagebutton]
import chroma/private/[imagecache, fontcache, dialogueaction, asthelpers, audiomanager, keycodedefs]

export dialogueaction

type
  ChromaInternal*[T] = ref object
    renderer*: RendererPtr      # root SDL2 renderer
    window*: WindowPtr          # root SDL2 window
    counter*: int               # address of the next script line to execute
    run*: bool                  # set to false to stop the game
    kpause*: bool                # pause execution
    gamevar*: T                 # user-defined gamevar structure
    saveFile*: string           # file where Chroma will look for saves, relative to the application directory.
    saves*: seq[SaveData]       # seq of all saves
    persistsFile*: string       # path to the persistents file
    gameFile*: string           # path to the the game-state file
    encKey*: string             # encryption key, ignore for now
    character*: OrderedTableRef[string, Character[T]] # table of all characters.
    dialoguedisp*: OrderedTableRef[string, DialogueDisp] # table of all dialogues.
    animations*: AnimationTable # all running animations. No guarantee on ordering.
    fontCache*: FontCache       # cache of all loaded fonts.
    imageCache*: ImageCache     # the global image cache. used for sprites, mostly.
    characterFrame*: ContainerDisplayable # the Frame where character displayables are rendered
    dialogueFrame*: ContainerDisplayable  # the Frame where dialogue displayables are rendered
    gameUiFrame*: ContainerDisplayable    # the Frame with all in-game UI elements
    menuFrame*: ContainerDisplayable      # the Frame that shows buttons when the script reaches a menu block
    backgroundDisp*: Background           # the background
    savedContexts*: seq[GameContext[T]]   # saved game contexts
    showMenuProc*: proc(options: seq[string])
    chosenMenuOption*: string
    backgrounds*: TableRef[string, proc(internal: ChromaInternal[T]): string]
    guiElements*: OrderedTableRef[string, Displayable]
    audioManager*: AudioManager
    audioDevice*: AudioOutputDevice
    reversing*: bool
    textInputReceiver*: Displayable # when in text-input mode, this displayable will receive text input events
    fullScreenEnabled*: bool
    fastForwardEnabled: bool
    autoplayEnabled*: bool
    dispModeShadow: DisplayMode

  SDLException = object of Exception
  
  ScriptActionKind* = enum
    saDialogue,
    saRaw,
    saJump,
    saReturn,
    saShow,
    saShowDialogue,
    saShowBackground,
    saShowUi,
    saHide,
    saHideDialogue,
    saHideBackground,
    saHideUi,
    saMenu,
    saPlay,
    saStop
  
  ShowArgKind* = enum
    sarLayerMod,   # for a character, change the currently-shown variant of a particular layer
    sarAnimation,  # for a character or displayable, apply an animation
    sarBehind,     # for a show character, ensure the character is shown behind another character
    sarNothing,    # for a character or displayable, no special action besides calling the ShowArg function
  
  PlayArgKind* = enum
    parAnimation,
    parNothing
  
  ScriptAction*[T] = object
    blocking*: bool
    case kind*: ScriptActionKind
    of saMenu:
      mItems*: seq[string]
    of saDialogue:
      dCharacter*: Character[T]
      dAction*: DialogueAction
      dDialogue*: DialogueDisp
      dOnDefault*: bool
      dFlush*: bool
    of saRaw, saHideUi, saShowUi:
      rExec*: proc(internal: ChromaInternal[T])
    of saJump:
      jTo*: int
    of saShow:
      sCharacter*: Character[T]
      sArgs*: seq[ShowArg]
    of saShowDialogue:
      sdDisp*: seq[DialogueDisp]
      sdArgs*: seq[ShowArg]
    of saShowBackground:
      sbDisp*: Background
      sbArgs*: seq[ShowArg]
    of saHide:
      hCharacter*: Character[T]
      hArgs*: seq[ShowArg]
    of saHideDialogue:
      hdDisp*: seq[DialogueDisp]
      hdArgs*: seq[ShowArg]
    of saHideBackground:
      hbDisp*: Background
      hbArgs*: seq[ShowArg]
    of saPlay:
      pChannel*: AudioChannel
      pWav*: WavData
      pArgs*: seq[PlayArg]
    of saStop:
      scChannels*: seq[AudioChannel]
      scArgs*: seq[PlayArg]
    of saReturn:
      discard
  
  ShowArg* = object
    case kind*: ShowArgKind
    of sarLayerMod:
      layer*, variant*: string
    of sarAnimation:
      animation*: Animation
    of sarBehind:
      character*: string
    of sarNothing:
      discard
  
  PlayArg* = object
    case kind*: PlayArgKind
    of parAnimation:
      animation*: Animation
    of parNothing:
      discard
  
  Character*[T] = object
    name*: string
    shortname*: string
    fullname*: string
    # each layer, in an ordered table by render order. Each variant represented by a proc
    # that evaluates to the path of the image to be shown when that variant is activated.
    layers*: OrderedTableRef[string, OrderedTableRef[string, proc(internal: ChromaInternal[T]): string]]
    # key: layer name. value: the currently selected variant.
    layerOpts*: OrderedTableRef[string, string]
    # displayable on which the character is drawn.
    displayable*: Characterd
  
  CharacterState* = object
    name*: string
    layerOpts*: OrderedTableRef[string, string]
    x*, y*, w*: int
    alpha*: uint8
    enabled*, visible*: bool
  
  GameContext*[T] = ref object
    counter*: int
    characters*: seq[CharacterState]
    gamevar*: T
    dialogues*: seq[(string, DialogueDispState)]
    background*: BackgroundState
#     audioChannels*: seq[(string, AudioChannelState)]
  
  ExecStat* = object
    ran*, waitForMenu*, waitForClick*, waitForAnimations*: bool
    waitUntilTime*: uint32
  
  HideAnimation[T] = ref object of Animation
    # a super-secret animation for internal purposes
    internal: ChromaInternal[T]
  
  HideDontDisableAnimation[T] = ref object of Animation
    # a super-secret animation for internal purposes
    internal: ChromaInternal[T]
  
  StopAnimation[T] = ref object of Animation
    # a super-secret animation for internal purposes
    internal: ChromaInternal[T]
  
  SaveData* = object
    timestamp*: DateTime
    name*: string
    path*: string

type
  ArgPack* = ref object of RootObj
    discard

template sdlFailIf*(cond: typed, reason: string) =
  if cond: raise SDLException.newException(
    reason & ", SDL error: " & $getError())

proc newChromaInternal[T]: ChromaInternal[T] =
  new result

proc newHideAnimation*[T](d: Displayable, internal: ChromaInternal[T]): HideAnimation[T] =
  new result
  result.tCreated = gfxTime()
  result.internal = internal
  result.displayable = d
  result.nruns = 0
  result.done = false
  # HideAnimation is the only animation whose `hide` flag should be true
  result.sems = AnimationSemaphore(hide: true)

method run*[T](a: HideAnimation[T]) =
  if not a.done:
    var hasActiveAnims: bool = false
    for it in a.internal.animations.animationsFor(a.displayable):
      var res = not it.done
      if res:
        try:
          let ff = HideAnimation[T](it)
          res = false
        except: discard
      if res:
        hasActiveAnims = true
        break
    if not hasActiveAnims:
      a.displayable.visible = false
      a.displayable.enabled = false
      a.done = true


proc newHideDontDisableAnimation*[T](d: Displayable, internal: ChromaInternal[T]): HideDontDisableAnimation[T] =
  new result
  result.tCreated = gfxTime()
  result.internal = internal
  result.displayable = d
  result.nruns = 0
  result.done = false
  # HideAnimation is the only animation whose `hide` flag should be true
  result.sems = AnimationSemaphore(hide: true)

method run*[T](a: HideDontDisableAnimation[T]) =
  if not a.done:
    var hasActiveAnims: bool = false
    for it in a.internal.animations.animationsFor(a.displayable):
      var res = not it.done
      if res:
        try:
          let ff = HideDontDisableAnimation[T](it)
          res = false
        except: discard
      if res:
        hasActiveAnims = true
        break
    if not hasActiveAnims:
      a.displayable.visible = false
      a.done = true



proc newStopAnimation*[T](d: WavData, internal: ChromaInternal[T]): StopAnimation[T] =
  new result
  result.tCreated = gfxTime()
  result.internal = internal
  result.displayable = d
  result.nruns = 0
  result.done = false
  result.sems = AnimationSemaphore(hide: true)
  echo "new stop animation!"


method run*[T](a: StopAnimation[T]) =
  if not a.done:
    var hasActiveAnims: bool = false
    for it in a.internal.animations.animationsFor(a.displayable):
      var res = not it.done
      if res:
        try:
          let ff = StopAnimation[T](it)
          res = false
        except: discard
      if res:
        hasActiveAnims = true
        break
    if not hasActiveAnims:
      echo "stopping a wav!"
      WavData(a.displayable).stop()
      a.done = true




macro getUi*[T](internal: ChromaInternal[T], id: string): untyped =
  # retrieves a ui element and casts it to its superclass of GameUiRoot.
  var typIdent = newEmptyNode()
  for data in guiTypeData:
    if id.strVal == data.name:
      typIdent = data.objTyIdent
      break
  if typIdent.kind == nnkEmpty:
    error "no ui with name " & id.strVal, id
  result = quote do:
    `typIdent`(`internal`.guiElements[`id`])


# this bootstraps Chroma. initializes SDL2, runs the game, and uses
# defer statements to make sure SDL2 is killed off correctly.
proc bootstrap*[T](
    title: string,
    game: proc(internal: ChromaInternal[T])) =
  var internal = newChromaInternal[T]()
  
  sdlFailIf(sdl2.init(INIT_VIDEO or INIT_TIMER or INIT_EVENTS or INIT_AUDIO) == SdlError):
    "SDL2 initialization failed"
  defer: sdl2.quit()

  sdlFailIf(ttfInit() == SdlError):
    "SDL2 TTF initialization failed"
  defer: ttfQuit()

  sdlFailIf(image.init(IMG_INIT_PNG) == SdlError.cint):
    "SDL2 img initialization failed"
  defer: image.quit()

  sdlFailIf(not setHint("SDL_RENDER_SCALE_QUALITY", "2")):
    "Linear texture filtering could not be enabled"
    
  var dispMode: DisplayMode
  sdlFailIf(getDesktopDisplayMode(0.cint, dispMode) == SdlError):
    "Failed to retrieve display info"

  internal.window = createWindow(title = title,
    x = SDL_WINDOWPOS_CENTERED, y = SDL_WINDOWPOS_CENTERED,
    w = (dispMode.w.toFloat * 0.8).toInt.cint,
    h = (dispMode.w.toFloat * 0.45).toInt.cint,
    flags = SDL_WINDOW_SHOWN or SDL_WINDOW_RESIZABLE)
  sdlFailIf(internal.window.isNil):
    "Window could not be created"
  defer: internal.window.destroy()

  internal.renderer = internal.window.createRenderer(index = -1,
    flags = Renderer_Accelerated or Renderer_PresentVsync)
  sdlFailIf(internal.renderer.isNil):
    "Renderer could not be created"
  defer: internal.renderer.destroy()
  
  assert internal.renderer.setLogicalSize(3840.cint, 2160.cint) == 0
  
  internal.savedContexts = @[]
  
  internal.renderer.setDrawColor(r = 0, g = 0, b = 0)
  internal.animations = newAnimationTable()
  
  internal.imageCache = newImageCache()
  defer: internal.imageCache.destroy()
  
  internal.backgroundDisp = newBackground(internal.renderer, internal.imageCache)
  defer: internal.backgroundDisp.destroy()
  
  internal.characterFrame = newFrame(internal.renderer)
  defer: internal.characterFrame.destroy()
  
  internal.dialogueFrame = newFrame(internal.renderer)
  defer: internal.dialogueFrame.destroy()
  
  internal.gameUiFrame = newFrame(internal.renderer)
  defer: internal.gameUiFrame.destroy()
  
  internal.menuFrame = newVbox()
  defer: internal.menuFrame.destroy()
  internal.menuFrame.xpos = 1020
  Vbox(internal.menuFrame).spacing = 60
  
  internal.character = newOrderedTable[string, Character[T]]()
  
  internal.guiElements = newOrderedTable[string, Displayable]()
  
  internal.audioManager = newAudioManager()
  
  for ao in internal.audioManager.availableOutputs:
    if internal.audioDevice == nil:
      try:
        internal.audioDevice = ao.open()
        echo "opened audio device: ", ao.name
        break
      except:
        echo "failed to open audio device: ", ao.name
        internal.audioDevice = nil
  defer: internal.audioDevice.close()
  
  stopTextInput()
  
  randomize()
  
  game(internal)


proc getState*[T](c: Character[T]): CharacterState =
  result.name = c.name
  result.layerOpts = c.layerOpts.deepCopy
  result.x = c.displayable.xpos
  result.y = c.displayable.ypos
  result.w = c.displayable.width
  result.alpha = c.displayable.alpha
  result.enabled = c.displayable.enabled
  result.visible = c.displayable.visible

proc loadCharacterState*[T](internal: ChromaInternal[T], state: CharacterState) =
  var c = internal.character[state.name]
  c.displayable.xpos = state.x
  c.displayable.ypos = state.y
  c.displayable.width = state.w
  c.displayable.alpha = state.alpha
  c.displayable.enabled = state.enabled
  c.displayable.visible = state.visible
  for k, v in state.layerOpts.pairs:
    c.layerOpts[k] = v
    let handle = c.layers[k][v]
    c.displayable[k] = handle(internal)

proc updateCharacterds[T](internal: ChromaInternal[T]) =
  for ch in internal.character.values:
    let d = ch.displayable
    for layer, opts in ch.layers:
      let handle = opts[ch.layerOpts[layer]]
      d[layer] = handle(internal)

proc updateCharacter*[T](internal: ChromaInternal[T], character: Character[T]) =
  ## update a character's layers.
  let d = character.displayable
  for layer, opts in character.layers:
    let handle = opts[character.layerOpts[layer]]
    d[layer] = handle(internal)

proc chrStates*[T](internal: ChromaInternal[T]): seq[CharacterState] =
  result = @[]
  for c in internal.character.values:
    result.add c.getState()


proc pushContext*[T](internal: ChromaInternal[T]) =
  ## push the current game context. This is called at the beginning of every dialogue line,
  ## and right before every raw block in the script.
  if internal.savedContexts.len >= 100:
    internal.savedContexts.delete(0)
  var ctxt = GameContext[T](
    counter: internal.counter,
    characters: internal.chrStates(),
    gamevar: deepCopy[T](internal.gamevar),
    dialogues: @[],
    background: internal.backgrounddisp.getState())
  for k, v in internal.dialoguedisp.pairs:
    var state = v.getState()
    for a in internal.animations.animationsFor(v):
      try:
        let ff = HideAnimation[T](a)
        state.visible = false
        state.enabled = false
      except: discard
    ctxt.dialogues.add (k, state)
  internal.savedContexts.add ctxt

proc popContext*[T](internal: ChromaInternal[T]) =
  ## get the most recent game context and load it. This is effectively a "go back" function.
  if internal.savedContexts.len > 0:
    let context = internal.savedContexts.pop()
    internal.counter = context.counter
    deepCopy[T](internal.gamevar, context.gamevar)
    for cs in context.characters:
      loadCharacterState(internal, cs)
    internal.menuFrame.destroyAllChildren()
    
    for (k, v) in context.dialogues:
      internal.dialoguedisp[k].loadState(v)
    internal.backgrounddisp.loadState(context.background)

proc startFastForward*[T](internal: ChromaInternal[T]) =
  internal.reversing = false
  internal.autoplayEnabled = false
  internal.fastForwardEnabled = true

proc stopFastForward*[T](internal: ChromaInternal[T]) =
  internal.fastForwardEnabled = false

proc startAutoplay*[T](internal: ChromaInternal[T]) =
  internal.reversing = false
  internal.autoplayEnabled = true
  internal.fastForwardEnabled = false

proc stopAutoplay*[T](internal: ChromaInternal[T]) =
  internal.autoplayEnabled = false

proc `fastForward`*[T](internal: ChromaInternal[T]): bool =
  internal.fastForwardEnabled

proc `autoPlay`*[T](internal: ChromaInternal[T]): bool =
  internal.autoplayEnabled

proc `autoplaySpeed=`*[T](internal: ChromaInternal[T], nmillis: int) =
  let nmillis = minmax(0, nmillis, 200)
  for d in internal.dialoguedisp.mvalues:
    d.ktimer = nmillis

proc `autoplaySpeed`*[T](internal: ChromaInternal[T]): int =
  for d in internal.dialoguedisp.values:
    return d.ktimer

proc `gameLoaded`*[T](internal: ChromaInternal[T]): bool =
  internal.gameFile.len > 0 and internal.run

proc isLoaded*[T](internal: ChromaInternal[T], save: SaveData): bool =
  internal.gameFile == save.path


proc `pause=`*[T](internal: ChromaInternal[T], b: bool) =
  internal.kpause = b
  if b:
    internal.stopAutoplay()
    internal.stopFastForward()

proc `pause`*[T](internal: ChromaInternal[T]): bool =
  internal.kpause

proc exec*[T](internal: ChromaInternal[T], sa: ScriptAction[T]): ExecStat =
  case sa.kind:
  of saDialogue:
    var dialogue = if sa.dDialogue != nil: sa.dDialogue else: internal.dialoguedisp["default"]
    if sa.dFlush:
      dialogue.dialogueText.flush()
      dialogue.nameText.text = sa.dCharacter.shortname
    if not dialogue.visible:
      dialogue.visible = true
      dialogue.enabled = true
      dialogue.alpha = 255
    dialogue.dialogueText.queue(sa.dAction)
    if not internal.reversing:
      internal.counter += 1
    if sa.blocking:
      dialogue.dialogueText.setTimer()
    result.waitForClick = sa.blocking
  of saJump:
    internal.counter = sa.jTo
  of saReturn:
    internal.run = false
  of saRaw:
    sa.rExec(internal)
    updateCharacterds(internal)
    if not internal.reversing:
      internal.counter += 1
  of saShow:
    sa.sCharacter.displayable.visible = true
    sa.sCharacter.displayable.enabled = true
    for a in sa.sArgs:
      case a.kind:
      of sarBehind:
        let idx = internal.characterFrame.findChild(internal.character[a.character].displayable)
        if idx >= 0:
          internal.characterFrame.detachChild(sa.sCharacter.displayable)
          internal.characterFrame.insertChild(sa.sCharacter.displayable, idx)
      of sarAnimation:
        internal.animations.add a.animation
        a.animation.run()
      of sarLayerMod:
        echo a.layer, "=", a.variant
        sa.sCharacter.layerOpts[a.layer] = a.variant
        let handle = sa.sCharacter.layers[a.layer][a.variant]
        sa.sCharacter.displayable[a.layer] = handle(internal)
      of sarNothing:
        discard
    if not internal.reversing:
      internal.counter += 1
    updateCharacter(internal, sa.sCharacter)
    result.waitForAnimations = sa.blocking
  of saHide:
    for a in sa.hArgs:
      case a.kind:
      of sarBehind:
        let idx = internal.characterFrame.findChild(internal.character[a.character].displayable)
        if idx >= 0:
          internal.characterFrame.detachChild(sa.sCharacter.displayable)
          internal.characterFrame.insertChild(sa.sCharacter.displayable, idx)
      of sarAnimation:
        internal.animations.add a.animation
      of sarLayerMod:
        echo a.layer, "=", a.variant
        sa.sCharacter.layerOpts[a.layer] = a.variant
        let handle = sa.sCharacter.layers[a.layer][a.variant]
        sa.sCharacter.displayable[a.layer] = handle(internal)
      of sarNothing:
        discard
    internal.animations.add newHideAnimation(sa.hCharacter.displayable, internal)
    if not internal.reversing:
      internal.counter += 1
    result.waitForAnimations = sa.blocking
  of saShowDialogue:
    for d in sa.sdDisp:
      d.enabled = true
      d.visible = true
      d.alpha = 255
    for a in sa.sdArgs:
      case a.kind:
      of sarAnimation:
        internal.animations.add a.animation
        a.animation.run()
      of sarNothing, sarBehind, sarLayerMod:
        discard
    if not internal.reversing:
      internal.counter += 1
    result.waitForAnimations = sa.blocking
  of saHideDialogue:
    for a in sa.hdArgs:
      case a.kind:
      of sarAnimation:
        internal.animations.add a.animation
      of sarNothing, sarBehind, sarLayerMod:
        discard
    for d in sa.hdDisp:
      internal.animations.add newHideAnimation(d, internal)
    internal.counter += 1
    result.waitForAnimations = sa.blocking
  of saShowBackground:
    sa.sbDisp.enabled = true
    sa.sbDisp.visible = true
    sa.sbDisp.alpha = 255
    for a in sa.sbArgs:
      case a.kind:
      of sarLayerMod:
        discard sa.sbDisp.update(internal.backgrounds[a.variant](internal))
      of sarAnimation:
        internal.animations.add a.animation
        a.animation.run()
      of sarNothing, sarBehind:
        discard
    if not internal.reversing:
      internal.counter += 1
    result.waitForAnimations = sa.blocking
  of saHideBackground:
    for a in sa.hbArgs:
      case a.kind:
      of sarAnimation:
        internal.animations.add a.animation
      of sarNothing, sarBehind, sarLayerMod:
        discard
    internal.animations.add newHideAnimation(sa.hbDisp, internal)
    internal.counter += 1
    result.waitForAnimations = sa.blocking
  of saShowUi, saHideUi:
    internal.counter += 1
    result.waitForAnimations = sa.blocking
  of saMenu:
    internal.showMenuProc(sa.mItems)
    result.waitForMenu = true
    if not internal.reversing:
      internal.counter += 1
  of saPlay:
    for p in sa.pArgs:
      case p.kind:
      of parAnimation:
        internal.animations.add p.animation
      of parNothing:
        discard
    result.waitForAnimations = sa.blocking
    if not internal.reversing:
      internal.counter += 1
  of saStop:
    for p in sa.scArgs:
      case p.kind:
      of parAnimation:
        internal.animations.add p.animation
      of parNothing:
        discard
    for c in sa.scChannels:
      for wd in c.wavDatas.mitems:
        internal.animations.add newStopAnimation(wd, internal)
    result.waitForAnimations = sa.blocking
    if not internal.reversing:
      internal.counter += 1
  
  result.ran = true
  result.waitUntilTime = (cpuTime() * 1000).uint32


proc newCharacter*[T](name, shortname, fullname: string): Character[T] =
  result = Character[T](
    name: name,
    shortname: shortname,
    fullname: fullname,
    layers: newOrderedTable[string, OrderedTableRef[string, proc(internal: ChromaInternal[T]): string]]())

proc newCharacter*[T](name: string): Character[T] =
  result = Character[T](
    name: name,
    layers: newOrderedTable[string, OrderedTableRef[string, proc(internal: ChromaInternal[T]): string]](),
    layerOpts: newOrderedTable[string, string]())

proc newCharacterd*[T](
    internal: ChromaInternal[T],
    layers: OrderedTableRef[string, OrderedTableRef[string, proc(internal: ChromaInternal[T]): string]]): Characterd =
  new result
  result.renderer = internal.renderer
  result.srcRect = rect(0.cint, 0.cint, 0.cint, 0.cint)
  result.currentImgs = initOrderedTable[string, SimpleImage]()
  result.kalpha = 255
  # determine the resolution of the displayable so that a software renderer and cache can be made
  for layername, opts in layers.pairs:
    for opt, handle in opts.pairs:
      let impath = handle(internal)
      var imsurf = image.load(impath)
      if imsurf == nil:
        raise Exception.newException("File not found: " & impath)
      result.baseHeight = imsurf.h
      result.baseWidth = imsurf.w
      imsurf.freeSurface()
      break
    break
  if layers.len == 0:
    result.baseHeight = 1
    result.baseWidth = 1
  result.srcRect.w = result.baseWidth.cint
  result.srcRect.h = result.baseHeight.cint
  # create the surface and software renderer
  result.surface = createRgbSurface(0.cint,
    result.baseWidth.cint, result.baseHeight.cint, 32.cint,
    0xff.uint32, 0xff00.uint32, 0xff0000.uint32, 0xff000000.uint32)
  result.childRenderer = result.surface.createSoftwareRenderer()
  result.texture = nil
  # create the image cache for the character displayable
  result.imageCache = internal.imageCache
  # load and cache images
  for layername, opts in layers.pairs:
    var firstVariant = true
    for opt, handle in opts.pairs:
      if firstVariant:
        # first variant immediately gets turned into a simpleimage
        # and is put into the layer table (currentImgs)
        result[layername] = handle(internal)
#         result.currentImgs[layername] = newSimpleImage(result.childRenderer, result.imageCache, handle(internal))
        firstVariant = false
      else:
        when defined(debug):
          # load and release as many image files as we can at the outset, to improve
          # the chance of dying immediately when an invalid file path is given
          result.imageCache.release result.imageCache.load(handle(internal))
        else:
          # ...but don't do that in the release version since it eats RAM.
          discard
  result.destRect = rect(
    result.srcRect.x,
    result.srcRect.y,
    result.srcRect.w,
    result.srcRect.h)
  result.baseHeight = result.surface.h
  result.baseWidth = result.surface.w
  result.doUpdate = true
  result.visible = false
  result.enabled = false


proc stopTextEntry*[T](internal: ChromaInternal[T])

proc startTextEntry*[T](internal: ChromaInternal[T], hook: Displayable) =
  ## start the text-entry mode. While in text-entry mode, the only
  ## displayable that will receive text-entry events is the one
  ## specified by `hook`.
  if isTextInputActive():
    internal.stopTextEntry()
  
  var candidateRect = 
    if hook == nil:
      rect(0.cint, 0.cint, 500.cint, 500.cint)
    else: rect(
      hook.xpos.cint,
      if hook.ypos.cint + hook.height.cint < 3340:
        hook.ypos.cint + hook.height.cint
      else:
        hook.ypos.cint - 500.cint,
      500.cint, 500.cint)
  internal.textInputReceiver = hook
  startTextInput()
  setTextInputRect(addr candidateRect)

proc stopTextEntry*[T](internal: ChromaInternal[T]) =
  ## stop text-entry mode.
  if isTextInputActive():
    if internal.textInputReceiver != nil:
      if not internal.textInputReceiver.event ChromaEvent(kind: eventTextFinished):
        stopTextInput()
    else:
      stopTextInput()

proc toggleTextEntry*[T](internal: ChromaInternal[T], hook: Displayable) =
  ## toggle the text-entry mode
  if isTextInputActive():
    stopTextEntry(internal)
  else:
    startTextEntry(internal, hook)


proc stop*[T](channel: AudioChannel,
    internal: ChromaInternal[T],
    playArgs: seq[PlayArg]) =
  for p in playArgs:
    case p.kind:
    of parAnimation:
      internal.animations.add p.animation
    of parNothing:
      discard
  for wd in channel.wavDatas.mitems:
    internal.animations.add newStopAnimation(wd, internal)

proc play*[T](channel: AudioChannel,
    internal: ChromaInternal[T],
    playArgs: seq[PlayArg]) =
  for p in playArgs:
    case p.kind:
    of parAnimation:
      internal.animations.add p.animation
    of parNothing:
      discard

proc enableFullScreen*[T](internal: ChromaInternal[T]) =
  ## enables full-screen mode, if it is not already enabled.
  if not internal.fullScreenEnabled:
    let winIndex = internal.window.getDisplayIndex()
    assert 0 == getCurrentDisplayMode(winIndex, internal.dispModeShadow).int
    var dispMode: DisplayMode
    assert 0 == getDesktopDisplayMode(winIndex, dispMode).int
    assert 0 == internal.window.setDisplayMode(addr dispMode).int
    discard internal.window.setFullscreen(SDL_WINDOW_FULLSCREEN)
    internal.fullScreenEnabled = true

proc disableFullScreen*[T](internal: ChromaInternal[T]) =
  ## disabled full-screen mode, if the game is currently full-screen.
  if internal.fullScreenEnabled:
    discard internal.window.setFullscreen(0.uint32)
    internal.fullScreenEnabled = false
    assert 0 == internal.window.setDisplayMode(addr internal.dispModeShadow).int

proc toggleFullScreen*[T](internal: ChromaInternal[T]) =
  ## toggles between full-screen and not full-screen.
  if internal.fullScreenEnabled:
    internal.disableFullScreen()
  else:
    internal.enableFullScreen()

proc quitGame*[T](internal: ChromaInternal[T]) =
  ## quits the game. Does not save!!!
  internal.run = false

macro chroma*(ast: untyped): untyped =
  ## the {.chroma.} pragma can be used to write functions that
  ## interface with Chroma.
  let internalSym = ident("internal")
  var fparams = nnkFormalParams.newTree()
  fparams.add ast[3][0]
  fparams.add nnkIdentDefs.newTree(
    internalSym,
    nnkBracketExpr.newTree(
      ident("ChromaInternal"),
      ident("GameVar")),
    newNilLit())
  for arg in ast[3][1..^1]:
    assert arg.kind == nnkIdentDefs
    if arg[0].kind == nnkIdent and arg[0].strVal == "internal":
      error "a chroma function cannot have an argument with name 'internal', since this would overlap with the the engine's 'internal' variable."
    fparams.add arg
  let funcName: string =
    if ast[0].kind == nnkPostfix:
      ast[0][1].strVal
    else:
      ast[0].strVal
  chromaFunctions.add funcName
  result = ast.kind.newTree(
    ast[0],
    ast[1],
    ast[2],
    fparams,
    ast[4],
    ast[5],
    convTree(internalSym, ast[6]))
  echo result.toStrLit

proc setChosenMenuOption*[T](internal: ChromaInternal[T], item: string): proc(button: ImageButton) =
  return proc(button: ImageButton) =
    internal.chosenMenuOption = item


# defining animations for the script's show command:
# say we want to define a "move" animation, which will be used like this:
# show alice, move(x=100, y=100, t=1000)
# 1) define a new proc. The proc's name should be <name_of_animation>ShowArg.
#    e.g if you are making an animation called "move," the proc should be called moveShowArg
#    or if you're making an animation called "helloWorld," the proc would be helloWorldShowArg
# 2) the proc's first argument MUST be a displayable. when the engine calls your proc, it will give
#    you the displayable associated with the thing you're calling the animation for. The second argument
#    to the proc MUST be a string. This string will contain the name of the thing you called this
#    animation on. The remaining arguments are whatever you want them to be.
#    Example: if your script has the line `show alice, move(100, 200, 300)`, the engine will try
#    to call the function `moveShowArg(d, "alice", 100, 200, 300)`.
# 3) the proc's return type must be ShowArg.
# 4) the proc must return a ShowArg object, whose `kind` field is set to sarAnimation, and whose
#    `animation` field is filled with a new Animation object.
proc moveShowArg*(d: Displayable, name: string,
    x: int = -2147483647,
    y: int = -2147483647,
    w: int = -2147483647,
    h: int = -2147483647,
    t = 1,
    delay = 0): ShowArg =
  let
    xx = if x == -2147483647: d.xpos else: x
    yy = if y == -2147483647: d.ypos else: y
    ww = if w == -2147483647: d.width else: w
    hh = if h == -2147483647: d.height else: h
  result = ShowArg(
    kind: sarAnimation,
    animation: newMoveAnimation(d, xx, yy, ww, hh, t, delay))

proc pidMove*(d: Displayable, name: string,
    x: int = -2147483647,
    y: int = -2147483647,
    w: int = -2147483647,
    h: int = -2147483647,
    kp = 0.1,
    ki = 0.01,
    kd = 0.001): ShowArg =
  let
    xx = if x == -2147483647: d.xpos else: x
    yy = if y == -2147483647: d.ypos else: y
    ww = if w == -2147483647: d.width else: w
    hh = if h == -2147483647: d.height else: h
  result = ShowArg(
    kind: sarAnimation,
    animation: newPidMoveAnimation(d, xx, yy, ww, hh, kp, ki, kd))



proc smoothMoveShowArg*(d: Displayable, name: string,
    x: int = -2147483647,
    y: int = -2147483647,
    w: int = -2147483647,
    h: int = -2147483647,
    speed: range[0.0..1.0] = 0.5): ShowArg =
  let
    xx = if x == -2147483647: d.xpos else: x
    yy = if y == -2147483647: d.ypos else: y
    ww = if w == -2147483647: d.width else: w
    hh = if h == -2147483647: d.height else: h
    kp = 1.0 + (3.0 * speed)
    ki = 1.5 + (3.5 * speed)
    kd = 0.5 + (1.5 * speed)
  result = ShowArg(
    kind: sarAnimation,
    animation: newPidMoveAnimation(d, xx, yy, ww, hh, kp, ki, kd))


proc behindShowArg*(d: Displayable, name: string, behind: string): ShowArg =
  result = ShowArg(
    kind: sarBehind,
    character: behind)


proc xShowArg*(d: Displayable, name: string, x: int): ShowArg =
  d.xpos = x
  result = ShowArg(kind: sarNothing)


proc yShowArg*(d: Displayable, name: string, y: int): ShowArg =
  d.ypos = y
  result = ShowArg(kind: sarNothing)


proc wShowArg*(d: Displayable, name: string, w: int): ShowArg =
  d.width = w
  result = ShowArg(kind: sarNothing)


proc hShowArg*(d: Displayable, name: string, h: int): ShowArg =
  d.height = h
  result = ShowArg(kind: sarNothing)

proc aShowArg(d: Displayable, name: string, a: uint8): ShowArg =
  d.alpha = a
  result = ShowArg(kind: sarNothing)

proc fadeInShowArg*(d: Displayable, name: string, t: int, to: uint8 = 255): ShowArg =
  d.alpha = 0
  result = ShowArg(
    kind: sarAnimation,
    animation: newFadeInAnimation(d, t, to))

proc fadeOutShowArg*(d: Displayable, name: string, t: int, to: uint8 = 0): ShowArg =
  result = ShowArg(
    kind: sarAnimation,
    animation: newFadeOutAnimation(d, t, to))

proc fadeInPlayArg*(wd: WavData, name: string, t: int, to: range[0..100] = 100): PlayArg =
  wd.volume = 0
  result = PlayArg(
    kind: parAnimation,
    animation: newAudioFadeAnimation(wd, t, to))

proc fadeOutPlayArg*(wd: WavData, name: string, t: int, to: range[0..100] = 0): PlayArg =
  result = PlayArg(
    kind: parAnimation,
    animation: newAudioFadeAnimation(wd, t, to))
