#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import chroma
import macros, tables, sequtils
import os, times, experimental/diff, strutils

import chroma/displayable, chroma/private/[asthelpers, dialogueaction, audiomanager]
import chroma/displayables/[dialoguedisp, backgrounddisp]

# called by generated code
proc cgDialogue*[T](who: Character[T], flush: bool, blocking: bool, onDefault: bool, action: DialogueAction, target: DialogueDisp): ScriptAction[T] =
  ScriptAction[T](kind: saDialogue, dCharacter: who, dAction: action, 
    dFlush: flush,
    dOnDefault: onDefault,
    blocking: blocking,
    dDialogue: target)

proc cgJump*[T](to: int): ScriptAction[T] =
  ScriptAction[T](kind: saJump, jTo: to)

proc cgRaw*[T](exec: proc(internal: ChromaInternal[T])): ScriptAction[T] =
  ScriptAction[T](kind: saRaw, rExec: exec)

proc cgRawMayBlock*[T](blocking: bool, exec: proc(internal: ChromaInternal[T])): ScriptAction[T] =
  ScriptAction[T](kind: saRaw, rExec: exec, blocking: blocking)

proc cgRet*[T](): ScriptAction[T] =
  ScriptAction[T](kind: saReturn)

proc cgShow*[T](who: Character[T], blocking: bool, params: varargs[ShowArg]): ScriptAction[T] =
  var args: seq[ShowArg] = @[]
  for p in params:
    args.add p
  result = ScriptAction[T](kind: saShow, sCharacter: who, sArgs: args, blocking: blocking)

proc cgShowDialogue*[T](id: seq[DialogueDisp], blocking: bool, params: varargs[ShowArg]): ScriptAction[T] =
  var args: seq[ShowArg] = @[]
  for p in params:
    args.add p
  result = ScriptAction[T](kind: saShowDialogue, sdDisp: id, sdArgs: args, blocking: blocking)

proc cgShowBackground*[T](disp: Background, blocking: bool, params: varargs[ShowArg]): ScriptAction[T] =
  var args: seq[ShowArg] = @[]
  for p in params:
    args.add p
  result = ScriptAction[T](kind: saShowBackground,
    sbDisp: disp,
    sbArgs: args,
    blocking: blocking)

proc cgHide*[T](who: Character[T], blocking: bool, params: varargs[ShowArg]): ScriptAction[T] =
  var args: seq[ShowArg] = @[]
  for p in params:
    args.add p
  result = ScriptAction[T](kind: saHide, hCharacter: who, hArgs: args)

proc cgHideDialogue*[T](id: seq[DialogueDisp], blocking: bool, params: varargs[ShowArg]): ScriptAction[T] =
  var args: seq[ShowArg] = @[]
  for p in params:
    args.add p
  result = ScriptAction[T](kind: saHideDialogue, hdDisp: id, hdArgs: args)

proc cgHideBackground*[T](disp: Background, blocking: bool, params: varargs[ShowArg]): ScriptAction[T] =
  var args: seq[ShowArg] = @[]
  for p in params:
    args.add p
  result = ScriptAction[T](kind: saHideBackground,
    hbDisp: disp,
    blocking: blocking,
    hbArgs: args)

proc cgMenu*[T](options: varargs[string]): ScriptAction[T] =
  var args: seq[string] = @[]
  for o in options:
    args.add o
  result = ScriptAction[T](kind: saMenu, mItems: args)

proc cgPlay*[T](channel: AudioChannel, wavData: WavData, blocking: bool, params: varargs[PlayArg]): ScriptAction[T] =
  var args: seq[PlayArg] = @[]
  for p in params:
    args.add p
  result = ScriptAction[T](kind: saPlay,
    pChannel: channel,
    pWav: wavData,
    blocking: blocking,
    pArgs: args)
  
proc cgStop*[T](channels: seq[AudioChannel], blocking: bool, params: seq[PlayArg] = @[]): ScriptAction[T] =
  result = ScriptAction[T](kind: saStop,
    blocking: blocking,
    scChannels: channels,
    scArgs: params)

# called by code generator
proc buildScriptTree(accumulator: var int, labelConstDefs: var seq[NimNode], lineCases: var NimNode, ast: NimNode)


proc constDefFromLabel(accumulator: int, ast: NimNode): NimNode =
  # const script_label_<ast[1]> = <accumulator + 1>
  result = nnkConstSection.newTree(
    nnkConstDef.newTree(
      nnkPostfix.newTree(
        newIdentNode("*"),
        newIdentNode("script_label_" & ast[1].strVal)),
      newEmptyNode(),
      newLit(accumulator + 1)))


proc dialogueActionFromArg(ast: NimNode): NimNode =
  case ast.kind:
  of nnkStrLit:
    result = quote do:
      textDialogueAction(`ast`)
  of nnkCall:
    result = ast.copy
    
    # this `if` doesn't affect the output AST. it just makes a more readable
    # error message for an incorrectly-defined font in the font() dialogue acton.
    if result[0].strVal == "font" and (result[1].strVal notin fontNames):
      error "A font with name '" & result[1].strVal & "' does not exist!", ast
    
    if result[0].strVal == "raw":
      result = result[1]
      result = quote do:
        textDialogueAction($`result`)
    else:
      for i in 1..(result.len-1):
        result[i] = convTree(result[i])
      result[0] = ident(result[0].strVal & "DialogueAction")
  else:
    let astText = convTree(ast)
    result = quote do:
      textDialogueAction($`astText`)

proc lineCaseFromDialogue(
    accumulator: int,
    flush, lend, onDefault: bool,
    chr, ast: NimNode,
    target = newNilLit()): NimNode =
  # case <accumulator>: return cgDialogue...
  var call = nnkCall.newTree()
  call.add nnkBracketExpr.newTree(
    newIdentNode("cgDialogue"),
    newIdentNode("T"))
  call.add nnkStmtList.newTree(
    nnkBracketExpr.newTree(
      nnkDotExpr.newTree(
        newIdentNode("internal"),
        newIdentNode("character")),
      newLit(chr.strVal)))
  call.add if flush: newLit(true) else: newLit(false)
  call.add if lend: newLit(true) else: newLit(false)
  call.add newLit(onDefault)
  call.add dialogueActionFromArg(ast)
  call.add target
  
  result = nnkOfBranch.newTree(
    newLit(accumulator),
    nnkStmtList.newTree(
      nnkReturnStmt.newTree(
        call)))


proc lineCaseFromJump(accumulator: int, ast: NimNode): NimNode =
  # case <accumulator>: return jump(<ast[1]>)
  result = nnkOfBranch.newTree(
    newLit(accumulator),
    nnkStmtList.newTree(
      nnkReturnStmt.newTree(
        nnkCall.newTree(
          nnkBracketExpr.newTree(
            newIdentNode("cgJump"),
            newIdentNode("T")
          ),
          newIdentNode("script_label_" & ast[1].strVal)))))


proc lineCaseFromJumpLit(accumulator: int, to: int): NimNode =
  # case <accumulator>: return jump(to)
  result = nnkOfBranch.newTree(
    newLit(accumulator),
    nnkStmtList.newTree(
      nnkReturnStmt.newTree(
        nnkCall.newTree(
          nnkBracketExpr.newTree(
            newIdentNode("cgJump"),
            newIdentNode("T")
          ),
          newLit(to)))))


proc lineCaseFromRaw(accumulator: int, ast: NimNode): NimNode =
  # case <accumulator>: return raw(proc() = <ast[1]>)
  result = nnkOfBranch.newTree(
    newLit(accumulator),
    nnkStmtList.newTree(
      nnkReturnStmt.newTree(
        nnkCall.newTree(
          newIdentNode("cgRaw"),
          nnkLambda.newTree(
            newEmptyNode(),
            newEmptyNode(),
            newEmptyNode(),
            nnkFormalParams.newTree(
              newEmptyNode(),
              nnkIdentDefs.newTree(
                newIdentNode("internal"),
#                 nnkVarTy.newTree(
                  nnkBracketExpr.newTree(
                    newIdentNode("ChromaInternal"),
                    newIdentNode("T")),
#                     ),
                newEmptyNode())),
            newEmptyNode(),
            newEmptyNode(),
            ast[1])))))


proc lineCaseFromOp(accumulator: int, ast: NimNode): NimNode =
  # same as raw, but with prefixing
  result = nnkOfBranch.newTree(
    newLit(accumulator),
    nnkStmtList.newTree(
      nnkReturnStmt.newTree(
        nnkCall.newTree(
          newIdentNode("cgRaw"),
          nnkLambda.newTree(
            newEmptyNode(),
            newEmptyNode(),
            newEmptyNode(),
            nnkFormalParams.newTree(
              newEmptyNode(),
              nnkIdentDefs.newTree(
                newIdentNode("internal"),
#                 nnkVarTy.newTree(
                  nnkBracketExpr.newTree(
                    newIdentNode("ChromaInternal"),
                    newIdentNode("T")),
#                     ),
                newEmptyNode())),
            newEmptyNode(),
            newEmptyNode(),
            convTree(ast))))))


proc lineCaseFromIf(at: int, ifTrue: int, ifFalse: int, ast: NimNode): NimNode =
  # case <at>: if <ast[0]>: return jump(ifTrue) else: return jump(ifFalse)
  result = nnkOfBranch.newTree(
    newLit(at),
    nnkStmtList.newTree(
      nnkReturnStmt.newTree(
        nnkIfExpr.newTree(
          nnkElifExpr.newTree(
            convTree(ast[0]),
            nnkStmtList.newTree(
              nnkCall.newTree(
                nnkBracketExpr.newTree(
                  newIdentNode("cgJump"),
                  newIdentNode("T")
                ),
                newLit(ifTrue)))),
          nnkElseExpr.newTree(
            nnkStmtList.newTree(
              nnkCall.newTree(
                nnkBracketExpr.newTree(
                  newIdentNode("cgJump"),
                  newIdentNode("T")
                ),
                newLit(ifFalse)
              )))))))

proc caseShowCharacter(id: NimNode, ast: NimNode, blocking: bool): NimNode =
  # internal.character["charactername"]
  let chrRef = nnkBracketExpr.newTree(
    nnkDotExpr.newTree(
      newIdentNode("internal"),
      newIdentNode("character")),
    newLit(id.strVal))
  # cgShow[T](internal.character["charactername"], blocking)
  result = nnkCall.newTree()
  result.add nnkBracketExpr.newTree(
      newIdentNode("cgShow"),
      newIdentNode("T"))
  result.add chrRef
  result.add newLit(blocking)
  for a in ast[2..^1]:
    # nnkIdents and nnkCalls
    if a.kind == nnkIdent:
      let layerName = characterAttrs[ast[1].strVal][a.strVal]
      # ShowArg(kind: sarLayerMod, layer: layerName, variant: "variant")
      result.add nnkObjConstr.newTree(
        newIdentNode("ShowArg"),
        nnkExprColonExpr.newTree(
          newIdentNode("kind"),
          newIdentNode("sarLayerMod")),
        nnkExprColonExpr.newTree(
          newIdentNode("layer"),
          newLit(layerName)),
        nnkExprColonExpr.newTree(
          newIdentNode("variant"),
          newLit(a.strVal)))
    elif a.kind == nnkCall:
      processCallShowArg(result,
        nnkDotExpr.newTree(chrRef, newIdentNode("displayable")),
        newLit(id.strVal),
        a)
    else:
      error("illegal argument for 'show' command: ", a)

proc caseShowDialogue(id: NimNode, ast: NimNode, blocking: bool): NimNode =
  # @[internal.dialoguedisp["dialogueName"]]
  var disprefs = nnkBracket.newTree()
  for a in ast[2..^1]:
    if a.kind == nnkIdent:
      let dialogueName = a.strVal
      if dialogueName notin dialogueDispNames:
        error "there is no dialogue with name '" & dialogueName & "'"
      let k =  nnkBracketExpr.newTree(
        nnkDotExpr.newTree(
          ident("internal"),
          ident("dialoguedisp")),
        a.toStrLit)
      dispRefs.add k
  if disprefs.len == 0:
    error "at least one dialogue must be specified in a 'show dialogue' command", ast
  
  var dispRefSeq = nnkPrefix.newTree(
    ident("@"),
    disprefs)
  # cgShowDialogue[T](internal.dialoguedisp["dialogueName"])
  result = nnkCall.newTree()
  result.add nnkBracketExpr.newTree(
    newIdentNode("cgShowDialogue"),
    newIdentNode("T"))
  result.add dispRefSeq
  result.add newLit(blocking)
  for dispRef in disprefs:
    for a in ast[2..^1]:
      if a.kind == nnkCall:
        processCallShowArg(result,
          dispRef,
          newLit(id.strVal),
          a)

proc caseShowBackground(id, ast: NimNode, blocking: bool): NimNode =
  let bgRef = nnkDotExpr.newTree(
    ident("internal"),
    ident("backgroundDisp"))
  
  result = nnkCall.newTree()
  result.add nnkBracketExpr.newTree(
    newIdentNode("cgShowBackground"),
    newIdentNode("T"))
  result.add bgRef
  result.add newLit(blocking)
  var gotBgLayerMod = false
  for a in ast[2..^1]:
    # nnkIdents and nnkCalls
    if a.kind == nnkIdent:
      let layerName = "id"
      if a.strVal notin backgroundNames:
        error "there is no background with name '" & a.strVal & "'", a
      if gotBgLayerMod:
        error "multiple backgrounds specified in a single show command", a
      # ShowArg(kind: sarLayerMod, layer: layerName, variant: "variant")
      result.add nnkObjConstr.newTree(
        newIdentNode("ShowArg"),
        nnkExprColonExpr.newTree(
          newIdentNode("kind"),
          newIdentNode("sarLayerMod")),
        nnkExprColonExpr.newTree(
          newIdentNode("layer"),
          newLit(layerName)),
        nnkExprColonExpr.newTree(
          newIdentNode("variant"),
          newLit(a.strVal)))
      gotBgLayerMod = true
    elif a.kind == nnkCall:
      processCallShowArg(result,
        bgRef,
        newLit(id.strVal),
        a)
    else:
      error("illegal argument for 'show' command: ", a)
  

proc caseShowUi(id, ast: NimNode, blocking: bool): NimNode =
#   echo id.treeRepr
#   echo ast.treeRepr
  expect ast[2], nnkCall
  expect ast[2][0], nnkIdent
  let
    internalIdent = ident("internal")
    uiNameStr = newLit(ast[2][0].strVal)
    animationsVar = genSym(nskVar, "animations")
    uiRef = quote do: `internalIdent`.getUi(`uiNameStr`)
    blockIdent = newLit(blocking)
  
  var argpackVals: seq[NimNode] = @[]
  for a in ast[2][1..^1]:
    argpackVals.add convTree(a)
  
  var showArgSeq = nnkBracket.newTree()
  for a in ast[3..^1]:
    if a.kind == nnkCall:
      processCallShowArg(showArgSeq, uiRef, newLit(id.strVal), a)
    else:
      error "illegal argument for show ui: " & a.toStrLit.strVal, a
  showArgSeq = nnkPrefix.newTree(
    ident("@"),
    showArgSeq)
  
  result = nnkCall.newTree(
    ident("show"),
    uiRef,
    internalIdent,
    showArgSeq)
  for a in argpackVals:
    result.add a
  
  result = quote do:
    cgRawMayBlock[T](`blockIdent`) do (`internalIdent`: ChromaInternal[T]):
#       let showArgs: seq[ShowArg] = `showArgSeq`
#       var `animationsVar`: seq[Animation] = @[]
#       for sa in showArgs:
#         if sa.kind == sarAnimation:
#           `animationsVar`.add sa.animation
      `result`

proc lineCaseFromShow(accumulator: int, ast: NimNode, blocking: bool): NimNode =
  let mainIdent = ast[1]    # character name, background name, dialogue name, etc
  var procCall: NimNode = newEmptyNode()
  
  if mainIdent.kind == nnkIdent and
     mainIdent.strVal in characterNames:
    procCall = caseShowCharacter(mainIdent, ast, blocking)
  elif mainIdent.kind == nnkIdent and
     mainIdent.strVal in ["background", "bg"]:
    procCall = caseShowBackground(mainIdent, ast, blocking)
  elif mainIdent.kind == nnkIdent and
     mainIdent.strVal in ["dialogue"]:
    procCall = caseShowDialogue(mainIdent, ast, blocking)
  elif mainIdent.kind == nnkIdent and
     mainIdent.strVal == "ui":
    procCall = caseShowUi(mainIdent, ast, blocking)
  else:
    error("unrecognized argument: " & mainIdent.toStrLit.strVal, mainIdent)
  
  result = nnkOfBranch.newTree(
    newLit(accumulator),
    nnkStmtList.newTree(
      nnkReturnStmt.newTree(
        procCall)))

proc caseHideCharacter(id: NimNode, ast: NimNode, blocking: bool): NimNode =
  # internal.character["charactername"]
  let chrRef = nnkBracketExpr.newTree(
    nnkDotExpr.newTree(
      newIdentNode("internal"),
      newIdentNode("character")),
    newLit(id.strVal))
  # cgShow[T](internal.character["charactername"], blocking)
  result = nnkCall.newTree()
  result.add nnkBracketExpr.newTree(
      newIdentNode("cgHide"),
      newIdentNode("T"))
  result.add chrRef
  result.add newLit(blocking)
  for a in ast[2..^1]:
    # nnkIdents and nnkCalls
    if a.kind == nnkIdent:
      let layerName = characterAttrs[ast[1].strVal][a.strVal]
      # ShowArg(kind: sarLayerMod, layer: layerName, variant: "variant")
      result.add nnkObjConstr.newTree(
        newIdentNode("ShowArg"),
        nnkExprColonExpr.newTree(
          newIdentNode("kind"),
          newIdentNode("sarLayerMod")),
        nnkExprColonExpr.newTree(
          newIdentNode("layer"),
          newLit(layerName)),
        nnkExprColonExpr.newTree(
          newIdentNode("variant"),
          newLit(a.strVal)))
    elif a.kind == nnkCall:
      processCallShowArg(result,
        nnkDotExpr.newTree(chrRef, newIdentNode("displayable")),
        newLit(id.strVal),
        a)
    else:
      error("illegal argument for 'hide' command: ", a)

proc caseHideBackground(id: NimNode, ast: NimNode, blocking: bool): NimNode =
  # internal.character["dialogueName"]
  let bgRef = nnkDotExpr.newTree(
    ident("internal"),
    ident("backgroundDisp"))
  # cgHide[T](internal.character["charactername"], blocking)
  result = nnkCall.newTree()
  result.add nnkBracketExpr.newTree(
      newIdentNode("cgHideBackground"),
      newIdentNode("T"))
  result.add bgRef
  result.add newLit(blocking)
  for a in ast[2..^1]:
    if a.kind == nnkCall:
      processCallShowArg(result,
        bgRef,
        newLit(id.strVal),
        a)
    else:
      error "invalid argument for 'hide background': " & a.toStrLit.strVal, a

proc caseHideDialogue(id: NimNode, ast: NimNode, blocking: bool): NimNode =
  # internal.dialoguedisp["dialogue"]
  var disprefs = nnkBracket.newTree()
  for a in ast[2..^1]:
    if a.kind == nnkIdent:
      let dialogueName = a.strVal
      if dialogueName notin dialogueDispNames:
        error "there is no dialogue with name '" & dialogueName & "'"
      dispRefs.add nnkBracketExpr.newTree(
        nnkDotExpr.newTree(
          ident("internal"),
          ident("dialoguedisp")),
        newLit(dialogueName))
  if disprefs.len == 0:
    error "at least one dialogue must be specified in a 'hide dialogue' command", ast
  
  var dispRefSeq = nnkPrefix.newTree(
    ident("@"),
    disprefs)
  
  # cgHideDialogue[T](internal.dialoguedisp["dialogue"], blocking)
  result = nnkCall.newTree()
  result.add nnkBracketExpr.newTree(
      newIdentNode("cgHideDialogue"),
      newIdentNode("T"))
  result.add dispRefSeq
  result.add newLit(blocking)
  
  for a in ast[2..^1]:
    if a.kind == nnkCall:
      for dispRef in disprefs:
        processCallShowArg(result,
          dispRef,
          newLit(id.strVal),
          a)
    elif a.kind == nnkIdent:
      discard # a dialogue node, already processed
    else:
      error "invalid argument for hide statement: " & a.toStrLit.strVal, a

proc caseHideUi(id, ast: NimNode, blocking: bool): NimNode =
#   echo id.treeRepr
#   echo ast.treeRepr
  expect ast[2], nnkIdent
  let
    internalIdent = ident("internal")
    uiNameStr = newLit(ast[2].strVal)
    animationsVar = genSym(nskVar, "animations")
    uiRef = quote do: `internalIdent`.getUi(`uiNameStr`)
    blockIdent = newLit(blocking)
  
  var showArgSeq = nnkBracket.newTree()
  for a in ast[3..^1]:
    if a.kind == nnkCall:
      processCallShowArg(showArgSeq, uiRef, newLit(id.strVal), a)
    else:
      error "illegal argument for show ui: " & a.toStrLit.strVal, a
  showArgSeq = nnkPrefix.newTree(
    ident("@"),
    showArgSeq)
  
  result = nnkCall.newTree(
    ident("hide"),
    uiRef,
    internalIdent,
    animationsVar)
  
  result = quote do:
    cgRawMayBlock[T](`blockIdent`) do (`internalIdent`: ChromaInternal[T]):
      let showArgs = `showArgSeq`
      var `animationsVar`: seq[Animation] = @[]
      for sa in showArgs:
        if sa.kind == sarAnimation:
          `animationsVar`.add sa.animation
      `result`
  

proc lineCaseFromHide(accumulator: int, ast: NimNode, blocking: bool): NimNode =
  let mainIdent = ast[1]    # character name, background name, dialogue name, etc
  var procCall: NimNode = newEmptyNode()
  
  if mainIdent.kind == nnkIdent and
     mainIdent.strVal in characterNames:
    procCall = caseHideCharacter(mainIdent, ast, blocking)
  elif mainIdent.kind == nnkIdent and
     mainIdent.strVal in ["background", "bg"]:
    procCall = caseHideBackground(mainIdent, ast, blocking)
  elif mainIdent.kind == nnkIdent and
     mainIdent.strVal in ["dialogue"]:
    procCall = caseHideDialogue(mainIdent, ast, blocking)
  elif mainIdent.kind == nnkIdent and
     mainIdent.strVal == "ui":
    procCall = caseHideUi(mainIdent, ast, blocking)
  else:
    error("unrecognized argument: " & mainIdent.toStrLit.strVal, mainIdent)
  
  result = nnkOfBranch.newTree(
    newLit(accumulator),
    nnkStmtList.newTree(
      nnkReturnStmt.newTree(
        procCall)))

proc lineCaseFromMenu(accumulator: int, ast: NimNode): NimNode =
  # of <accumulator>: return cgMenu[T]("each", "menu", "option")
  assert ast.kind == nnkStmtList
  result = nnkCall.newTree(
    nnkBracketExpr.newTree(
      newIdentNode("cgMenu"),
      newIdentNode("T")))
  for a in ast:
    result.add a[0] # add each string option
  result = nnkOfBranch.newTree(
    newLit(accumulator),
    nnkStmtList.newTree(
      nnkReturnStmt.newTree(
        result)))

proc ifStmtFromMenuAlts(ast: NimNode): NimNode =
  assert ast.kind == nnkStmtList
  result = nnkIfStmt.newTree()
  for a in ast:
    if a.kind == nnkCall and a.len == 2 and a[0].kind == nnkStrLit and a[1].kind == nnkStmtList:
      # if raw(internal.chosenMenuOption) == <a[0]>: <a[1]>
      result.add nnkElifBranch.newTree(
        nnkInfix.newTree(
          ident("=="),
          nnkCall.newTree(
            ident("raw"),
            nnkDotExpr.newTree(
              ident("internal"),
              ident("chosenMenuOption"))),
          a[0]),
        a[1])
    else:
      echo a.toStrLit
      error "invalid syntax in menu option", a

proc lineCaseFromPlay(accumulator: int, ast: NimNode, blocking: bool): NimNode =
  if ast.len < 2:
    error "play command missing channel identifier", ast
  expect(ast[1], nnkIdent)
  if ast[1].strVal notin ["queue", "music", "sound", "voice"]:
    error ast[1].strVal & " is not an audio channel!", ast[1]
  if ast.len < 3:
    error "play command missing audio file path", ast
  
  let internalIdent = ident("internal")
  let chanIndexIdent = ident(ast[1].strVal & "ChannelIndex")
  let wavPath = ast[2]
  
  let chanArg = quote do:
    `internalIdent`.audioDevice.getChannel(`chanIndexIdent`)
  let wavArg = quote do:
    `chanArg`.loadWav(`wavPath`)
  let blockingArg = newLit(blocking)
  
  result = nnkCall.newTree()
  result.add nnkBracketExpr.newTree(
    newIdentNode("cgPlay"),
    newIdentNode("T"))
  result.add chanArg
  result.add wavArg
  result.add blockingArg
  
  for a in ast[3..^1]:
    expect a, nnkCall
    processCallPlayArg(result,
      wavArg,
      wavPath,
      a)
  
  result = nnkOfBranch.newTree(
    newLit(accumulator),
    nnkStmtList.newTree(
      nnkReturnStmt.newTree(
        result)))
  
  # cgPlay*[T](channel: AudioChannel, wavData: WavData, blocking: bool, params: varargs[PlayArg])

proc lineCaseFromStop(accumulator: int, ast: NimNode, blocking: bool): NimNode =
  # make StopAnimations for every WavData in the specified channels
  let internalIdent = ident("internal")
  
  var chanrefs = nnkBracket.newTree()
  var channames: seq[NimNode] = @[]
  for a in ast[1..^1]:
    if a.kind == nnkIdent:
      let chanIndexIdent = ident(a.strVal & "ChannelIndex")
      chanRefs.add quote do:
        `internalIdent`.audioDevice.getChannel(`chanIndexIdent`)
      channames.add a.toStrLit
  if chanrefs.len == 0:
    error "at least one audio channel must be specified in a 'stop' command", ast
  
  let chanRefSeq = nnkPrefix.newTree(
    ident("@"),
    chanrefs)
  
  let playArgSeqIdent = genSym(nskVar, "playArgs")
  
  result = nnkStmtList.newTree()
  
  result.add quote do:
    var `playArgSeqIdent`: seq[PlayArg] = @[]
  
  for a in ast[1..^1]:
    if a.kind == nnkCall:
      var i = 0
      for chanRef in chanrefs:
        let wdsym = genSym(nskForVar, "wavData")
        
        var call = nnkCall.newTree(
          ident("add"),
          playArgSeqIdent)
        
        processCallPlayArg(call,
          wdsym,
          channames[i],
          a)
        
        result.add quote do:
          for `wdsym` in `chanRef`.wavDatas:
            `call`
        
        i += 1
    elif a.kind == nnkIdent:
      discard # a channel node, already processed
    else:
      error "invalid argument for stop statement: " & a.toStrLit.strVal, a
  
  let blockval = newLit(blocking)

  result = nnkOfBranch.newTree(
    newLit(accumulator),
    quote do:
      `result`
      return cgStop[T](`chanRefSeq`, `blockval`, `playArgSeqIdent`))

proc buildScriptTree(accumulator: var int, labelConstDefs: var seq[NimNode], lineCases: var NimNode, ast: NimNode) =
  # main proc for converting the script AST into a long chain of case-of statements.
  if ast.kind == nnkStmtList: # recurse into stmt lists
    for node in ast:
      buildScriptTree(accumulator, labelConstDefs, lineCases, node)
  elif ast.kind == nnkCommand and ast[0].kind == nnkIdent: # command calls- label, jump, show, etc
    if ast[0].strVal == "label":
      echo "found label with name (", ast[1].strVal, "), binding to address ", accumulator + 1
      labelConstDefs.add constDefFromLabel(accumulator, ast)
      if ast.len > 2:
        for node in ast[2..<ast.len]:
          buildScriptTree(accumulator, labelConstDefs, lineCases, node)
    elif ast[0].strVal == "jump":
      accumulator += 1
      lineCases.add lineCaseFromJump(accumulator, ast)
    elif ast[0].strVal == "show":
      accumulator += 1
      lineCases.add lineCaseFromShow(accumulator, ast, true)
    elif ast[0].strVal == "hide":
      accumulator += 1
      lineCases.add lineCaseFromHide(accumulator, ast, true)
    elif ast[0].strVal == "play":
      accumulator += 1
      lineCases.add lineCaseFromPlay(accumulator, ast, false)
#     elif ast[0].strVal == "pause":
#     elif ast[0].strVal == "resume":
    elif ast[0].strVal == "stop":
      accumulator += 1
      lineCases.add lineCaseFromStop(accumulator, ast, false)
    elif ast[0].strVal in characterNames: # dialogue command with no dialoguedisp specified
      var flush = true
      for a in ast[1..^2]:
        accumulator += 1
        lineCases.add lineCaseFromDialogue(accumulator, flush, false, true, ast[0], a)
        flush = false
      accumulator += 1
      lineCases.add lineCaseFromDialogue(accumulator, flush, true, true, ast[0], ast[^1])
    else:
      error("unrecognized command: " & ast[0].strVal, ast[0])
  elif ast.kind == nnkCommand and ast[0].kind == nnkBracketExpr: # command[...] ...
    let command = ast[0][0]
    let cmdParams = ast[0][1..^1]
    let cmdArgs = ast[1..^1]
    if command.kind == nnkIdent and command.strVal in characterNames:
      # dialogue with bracket parameters (dialoguedisp and blocking/nonblocking are allowed)
      if cmdParams.len < 1:
        error "at least one parameter should be specified", ast[0]
      elif cmdParams.len > 2:
        error "too many parameters", ast[0]
      
      var
        blockingParam = newEmptyNode()
        dialogueParam = newNilLit()
      
      # figure out which param is which
      for param in cmdParams:
        if param.kind != nnkIdent:
          errorGotExpected(param, ident(""))
        if param.strVal in ["nonblocking", "blocking"]:
          if blockingParam.kind == nnkEmpty:
            blockingParam = param
          else:
            error "multiple blocking/nonblocking parameters given", ast[0]
        else:
          if dialogueParam.kind == nnkNilLit:
            dialogueParam = param
          else:
            error "multiple dialogue parameters given", ast[0]
      
      let doBlocking = blockingParam.kind == nnkEmpty or
        blockingParam.strVal == "blocking"
      
      let onDefault =
        dialogueParam.kind == nnkNilLit or
        dialogueParam.strVal == "default"
      
      if dialogueParam.kind != nnkNilLit:
        dialogueParam = nnkBracketExpr.newTree(
          nnkDotExpr.newTree(
            newIdentNode("internal"),
            newIdentNode("dialoguedisp")),
          newLit(dialogueParam.strVal))
      
      var flush = true
      for a in ast[1..^2]:
        accumulator += 1
        lineCases.add lineCaseFromDialogue(accumulator, flush, false, onDefault, command, a, dialogueParam)
        flush = false
      accumulator += 1
      lineCases.add lineCaseFromDialogue(accumulator, flush, doBlocking, onDefault, command, ast[^1], dialogueParam)
    elif command.kind == nnkIdent and command.strVal == "show":
      if cmdParams.len < 1:
        error "at least one parameter should be specified", ast[0]
      if cmdParams.len > 1:
        error "too many parameters, only one of blocking/nonblocking is allowed", ast[0]
      if cmdParams[0].kind != nnkIdent:
        errorGotExpected(cmdParams[0], ident(""))
      
      if cmdParams[0].strVal notin ["blocking", "nonblocking"]:
        error "invalid parameter for show command, expected blocking or nonblocking", ast[0][1]
      
      let isBlocking = cmdParams[0].strVal == "blocking"
      accumulator += 1
      lineCases.add lineCaseFromShow(accumulator, ast, isBlocking)
    elif command.kind == nnkIdent and command.strVal == "hide":
      if cmdParams.len < 1:
        error "at least one parameter should be specified", ast[0]
      if cmdParams.len > 1:
        error "too many parameters, only one of blocking/nonblocking is allowed", ast[0]
      expect cmdParams[0], nnkIdent
      if cmdParams[0].strVal notin ["blocking", "nonblocking"]:
        error "invalid parameter for hide command, expected blocking or nonblocking", ast[0][1]
      
      let isBlocking = cmdParams[0].strVal == "blocking"
      accumulator += 1
      lineCases.add lineCaseFromHide(accumulator, ast, isBlocking)
    elif command.kind == nnkIdent and command.strVal == "play":
      if cmdParams.len < 1:
        error "at least one parameter should be specified", ast[0]
      if cmdParams.len > 1:
        error "too many parameters, only one of blocking/nonblocking is allowed", ast[0]
      expect cmdParams[0], nnkIdent
      if cmdParams[0].strVal notin ["blocking", "nonblocking"]:
        error "invalid parameter for hide command, expected blocking or nonblocking", ast[0][1]
      
      let isBlocking = cmdParams[0].strVal == "blocking"
      accumulator += 1
      lineCases.add lineCaseFromPlay(accumulator, ast, isBlocking)
#     elif command.kind == nnkIdent and command.strVal == "stop":
    else:
      error "unrecognized command: " & ast[0].toStrLit.strVal, ast[0]
      
  elif ast.kind == nnkCall and ast[0].kind == nnkIdent:
    if ast[0].strVal == "raw":
      accumulator += 1
      lineCases.add lineCaseFromRaw(accumulator, ast)
    elif ast[0].strVal == "menu":
      accumulator += 1
      lineCases.add lineCaseFromMenu(accumulator, ast[1])
      buildScriptTree(accumulator, labelConstDefs, lineCases, ifStmtFromMenuAlts(ast[1]))
    else:
      error("unrecognized token: " & ast[0].strVal, ast[0])
  elif ast.kind == nnkIfStmt:
    var ifs: seq[int] = @[]
    var jumps: seq[int] = @[]
    var hasElse = false
    for branch in ast:
      if branch.kind == nnkElifBranch:
        accumulator += 1
        ifs.add accumulator
        buildScriptTree(accumulator, labelConstDefs, lineCases, branch[1])
        accumulator += 1
        jumps.add accumulator
      else:
        hasElse = true
        ifs.add accumulator + 1
        buildScriptTree(accumulator, labelConstDefs, lineCases, branch[0])
    for p in jumps:
      # at the end of each if block, a jump to the end of the if-else structure
      lineCases.add lineCaseFromJumpLit(p, accumulator + 1)
    if not hasElse:
      # the final entry of ifs is where the last if jumps to if it's false
      # if there's an else block, that should be the else block, and it
      # was already added in the loop. if there is no else block, the last
      # if jumps to the next line of code, i.e. accumulator + 1.
      ifs.add accumulator + 1
    for i in 0..ifs.high-1:
      let ifTrue = ifs[i] + 1
      let ifFalse = ifs[i + 1]
      lineCases.add lineCaseFromIf(ifs[i], ifTrue, ifFalse, ast[i])
  elif ast.kind == nnkAsgn and ast[0].kind == nnkPrefix and ast[0][0].kind == nnkIdent and ast[0][0].strVal == "$":
    # $ statements, assignment
    accumulator += 1
    var astAlt = nnkAsgn.newTree()
    astAlt.add ast[0][1]
    astAlt.add ast[1]
    lineCases.add lineCaseFromOp(accumulator, astAlt)
  elif ast.kind == nnkInfix and ast[1].kind == nnkPrefix and ast[1][0].kind == nnkIdent and ast[1][0].strVal == "$":
    # $ statements, infix
    accumulator += 1
    var astAlt = nnkInfix.newTree()
    astAlt.add ast[0]
    astAlt.add ast[1][1]
    astAlt.add ast[2]
    lineCases.add lineCaseFromOp(accumulator, astAlt)
  elif ast.kind == nnkPrefix and ast[0].kind == nnkIdent and ast[1].kind == nnkCall:
    # $ statements, prefix
    accumulator += 1
    lineCases.add lineCaseFromOp(accumulator, ast[1])
  else:
    error "unrecognized expression: " & ast.toStrLit.strVal, ast

var scriptGenProc {.compileTime.}: NimNode = newEmptyNode()

macro script*(userScript: untyped): untyped =
  var labelConstDefs: seq[NimNode] = @[]
  var lineCases =
    nnkCaseStmt.newTree(
      nnkDotExpr.newTree(
        newIdentNode("internal"),
        newIdentNode("counter")))
  
  var n: int = 0
  buildScriptTree(n, labelConstDefs, lineCases, userScript)
  
  # else: return ret()
  lineCases.add nnkElse.newTree(
    nnkStmtList.newTree(
      nnkReturnStmt.newTree(
        nnkCall.newTree(
          nnkBracketExpr.newTree(
            newIdentNode("cgRet"),
            newIdentNode("T")
          )))))
  
#   echo lineCases.astGenRepr()
  
  let procDef = nnkProcDef.newTree(
    nnkPostfix.newTree(
      newIdentNode("*"),
      newIdentNode("generatedScript")),
    newEmptyNode(),
    nnkGenericParams.newTree(
      nnkIdentDefs.newTree(
        newIdentNode("T"),
        newEmptyNode(),
        newEmptyNode()
      )
    ),
    nnkFormalParams.newTree(
      nnkBracketExpr.newTree(
        newIdentNode("ScriptAction"),
        newIdentNode("T")
      ),
      nnkIdentDefs.newTree(
        newIdentNode("internal"),
#         nnkVarTy.newTree(
          nnkBracketExpr.newTree(
            newIdentNode("ChromaInternal"),
            newIdentNode("T")),
#             ),
        newEmptyNode())),
    newEmptyNode(),
    newEmptyNode(),
    nnkStmtList.newTree(
      lineCases))
  
  result = nnkStmtList.newTree()
  for lcd in labelConstDefs:
    result.add lcd
  scriptGenProc = procDef

macro genGeneratedScript*(): untyped =
  result = scriptGenProc
