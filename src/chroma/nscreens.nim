#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import macros
import chroma/private/asthelpers
import chroma/displayables/[dialoguedisp, simpletext, rectangle, simpleimage, menudisp]
import chroma/ngui
export menudisp

var dialogueProcSyms {.compileTime.}: seq[NimNode] = @[]
var backgroundProcSyms {.compileTime.}: seq[NimNode] = @[]

proc createDialogueProc(id, ast: NimNode): NimNode =
  assert id.kind == nnkIdent
  assert ast.kind == nnkStmtList
  let
    procSym = genSym(nskProc, id.strVal & "DialogueInit")
    internalSym = ident("internal")
    dispSym = genSym(nskVar, "disp")
    dialogueNameStr = newLit(id.strVal)
  dialogueProcSyms.add procSym
  
  result = nnkStmtList.newTree()
  for a in ast:
    if a.kind == nnkCall and a.len == 2:
      if a[0].strVal notin ["dialogueRect", "nameRect", "dialogueText", "nameText"]:
        error "unexpected token: " & a[0].strVal, a[0]
      if a[1].kind != nnkStmtList:
        error "expecting a list of item properties here"
      collapseTree(result, dispSym, a)
    elif a.kind == nnkDiscardStmt:
      discard # todo: add a block where defaults get set
    else:
      error "syntax error in dialogue definition: " & a.toStrLit.strVal, a
  result = quote do:
    proc `procSym`*[T](`internalSym`: ChromaInternal[T]) =
      assert `internalSym`.fontCache != nil
      echo "loading dialogue '", `dialogueNameStr`, "'"
      var `dispSym` = newDialogueDisp(`internalSym`.renderer, `internalSym`.fontCache)
      `result`
      `internalSym`.dialoguedisp[`dialogueNameStr`] = `dispSym`



proc createDialogueProc2(id, ast: NimNode): NimNode =
  assert id.kind == nnkIdent
  assert ast.kind == nnkStmtList
  let
    procSym = genSym(nskProc, id.strVal & "DialogueInit")
    internalSym = ident("internal")
    dispSym = genSym(nskVar, "disp")
    dialogueNameStr = newLit(id.strVal)
  dialogueProcSyms.add procSym
  
  var preamble = nnkStmtList.newTree()
  var mainStmts = nnkStmtList.newTree()
  
  for a in ast:
    if a.kind == nnkCall and a.len == 2 and a[0].kind == nnkIdent and a[0].strVal == "dialogueText":
      let dtsym = quote do: `dispSym`.dialogueText
      mainStmts.add processAst(preamble, internalSym, dtsym, dispSym, a[1])
    elif a.kind == nnkCall and a.len == 2 and a[0].kind == nnkIdent and a[0].strVal == "nameText":
      let dtsym = quote do: `dispSym`.nameText
      mainStmts.add processAst(preamble, internalSym, dtsym, dispSym, a[1])
    else:
      mainStmts.add processAst(preamble, internalSym, dispSym, dispSym, a)
  
  result = quote do:
    proc `procSym`*[T](`internalSym`: ChromaInternal[T]) =
      assert `internalSym`.fontCache != nil
      assert `internalSym`.imageCache != nil
      echo "loading dialogue '", `dialogueNameStr`, "'"
      var `dispSym` = newDialogueDisp(`internalSym`.renderer, `internalSym`.fontCache, `internalSym`.imageCache)
      `preamble`
      `mainStmts`
      `internalSym`.dialoguedisp[`dialogueNameStr`] = `dispSym`
#   echo result.toStrLit
#       `internalSym`.dialoguedisp[`dialogueNameStr`] = `dispSym`

macro dialogue*(id, ast: untyped): untyped =
  expect(id, nnkIdent)
  expect(ast, nnkStmtList)
  
  let
    idStr = id.strVal
    idStrLit = newLit(id.strVal)
  
  if idStr in dialogueDispNames:
    error "a dialogue with the name '" & idStr & "' already exists!", id
  dialogueDispNames.add idStr
  result = createDialogueProc2(id, ast)
  

macro dialogue*(ast: untyped): untyped =
  expect(ast, nnkStmtList)
  
  if "default" in dialogueDispNames:
    error "a default dialogue already exists!", ast
  
  dialogueDispNames.add "default"
  result = createDialogueProc2(ident("default"), ast)



macro menubutton*(ast: untyped): untyped =
  if ast.kind != nnkStmtList:
    error "invalid syntax for a dialogue definition", ast
  let procSym = ident("initMenuButton")
  let mbSym = genSym(nskVar, "menuButton")
  let internalSym = genSym(nskParam, "internal")
  
  result = nnkStmtList.newTree()
  for a in ast:
    collapseTree(result, mbSym, a)
  result = quote do:
    proc `procSym`*[T](`internalSym`: ChromaInternal[T], s: string): MenuButton =
      assert `internalSym`.fontCache != nil
      var `mbSym` = newMenuButton(`internalSym`.renderer, `internalSym`.imageCache, `internalSym`.fontCache)
      `result`
      `mbSym`.textArea.text = s
      result = `mbSym`
#   echo result.toStrLit

macro background*(id, ast: untyped): untyped =
  if id.kind != nnkIdent:
    error "expected an identifier for background declaration", id
  if ast.kind != nnkStmtList:
    error "invalid syntax for background definition", ast
  if (id.strVal in reservedKeywords) or (id.strVal in reservedCommands):
    error "cannot name a background '" & id.strVal & "' because " & id.strVal & " is a reserved keyword.", id
  if id.strVal in backgroundNames:
    error "a background with name '" & id.strVal & "' already exists!", id
  backgroundNames.add id.strVal
  
  let procSym = genSym(nskProc, id.strVal & "BackgroundInit")
  let bgSym = genSym(nskVar, "background")
  let internalSym = genSym(nskParam, "internal")
  let internalSym2 = ident("internal")
  let bgNameStr = newLit(id.strVal)
  let astExpr = convTree(internalSym2, ast)
  backgroundProcSyms.add procSym
  
  result = quote do:
    proc `procSym`*[T](`internalSym`: ChromaInternal[T]) =
      echo "loading background '", `bgNameStr`, "'"
      `internalSym`.backgrounds[`bgnameStr`] =
        proc(`internalSym2`: ChromaInternal[T]): string =
          `astExpr`


macro genDialogueInitializer*(): untyped =
  let iprocsym = ident("generatedDialogue")
  let internalSym = genSym(nskParam, "internal")
  result = nnkStmtList.newTree()
  for s in dialogueProcSyms:
    result.add quote do:
      `s`(`internalSym`)

  result = quote do:
    proc `iprocsym`[T](`internalSym`: ChromaInternal[T]) =
      `internalSym`.dialoguedisp = newOrderedTable[string, DialogueDisp]()
      `result`
#   echo result.toStrLit

macro genBackgroundInitializer*(): untyped =
  let iprocsym = ident("generatedBackgrounds")
  let internalSym = genSym(nskParam, "internal")
  result = nnkStmtList.newTree()
  for s in backgroundProcSyms:
    result.add quote do:
      `s`[T](`internalSym`)
  
  result = quote do:
    proc `iprocsym`[T](`internalSym`: ChromaInternal[T]) =
      `internalSym`.backgrounds = newTable[string, proc(internal: ChromaInternal[T]): string]()
      `result`
#   echo result.toStrLit
