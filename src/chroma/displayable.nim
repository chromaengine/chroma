#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import sdl2, sdl2/[ttf, image]
import chroma/jankcrypt
import typetraits, times, hashes, macros

import chroma/private/keycodedefs
export keycodedefs

type
  Displayable* = ref object of RootObj
    parent*: ContainerDisplayable
    surface*: SurfacePtr
    kalpha*: uint8
    kDoUpdate*: bool
    srcRect*: Rect
    destRect*: Rect
    onEvent*: proc(self: Displayable, event: ChromaEvent): bool
    texture*: TexturePtr
    xoff*: cint
    yoff*: cint
    renderer*: RendererPtr
    doNotDestroySurface*: bool
    doNotDestroyTexture*: bool
    debugName*: string
    tenabled: uint32
    kenabled: bool
    kvisible: bool
  
  ContainerDisplayable* = ref object of Displayable
    children*: seq[Displayable]
  
  ChromaEventKind* = enum
    eventMouseDown,
    eventMouseUp,
    eventKeyDown,
    eventKeyUp,
    eventMouseWheel,
    eventQuit,
    eventMouseMotion,
    eventTextEditing,
    eventTextInput,
    eventTextFinished
  
  ChromaEvent* = object
    case kind*: ChromaEventKind
    of eventMouseDown, eventMouseUp, eventMouseWheel, eventMouseMotion:
      button*: MouseButton
      position*: tuple[x: int, y: int]
      motion*: tuple[x: int, y: int] # eventMouseMotion: the mouse movement vector. eventMouseWheel: the wheel movement vector
      flipped*: bool # only applicable to mouseWheel
    of eventKeyDown, eventKeyUp:
      keycode*: ChromaKeyCode
      repeat*: bool
      shiftPressed*: bool
      ctrlPressed*: bool
      altPressed*: bool
      guiPressed*: bool
      mousePos*: tuple[x: int, y: int]
    of eventTextInput, eventTextEditing:
      text*: string
    of eventQuit, eventTextFinished:
      discard
    timestamp*: uint32
  
  MouseButton* = enum
    buttonUnknown,
    buttonLeft,
    buttonRight,
    buttonMiddle,
    buttonSpecial1,
    buttonSpecial2
  
  TextAlignment* = enum
    Left,
    Right,
    Center

macro printDebug*(d: Displayable, message: varargs[untyped]): untyped =
  var call = nnkCall.newTree(
    ident("echo"))
  for m in message:
    call.add m
  result = quote do:
    if `d`.debugName.len > 0:
      `call`

proc hash*(d: Displayable): Hash =
  result = 0
  result = result !& cast[int](addr d.kalpha)
  result = !$result

func minmax*(i, x, a: int): int =
  if x < i: i
  elif x > a: a
  else: x

func minmax*(i, x, a: float): float =
  if x < i: i
  elif x > a: a
  else: x

proc gfxTime*: uint32 = (cpuTime() * 1000).uint32

proc scaledMousePos*(window: WindowPtr): tuple[x, y: int] =
  # SDL2's getMouseState returns mouse position as an absolute value relative to
  # the window position. We want the position to be scaled as if the window
  # was 3840 x 2160. This proc does that scaling.
  var x, y, w, h: cint
  discard getMouseState(x, y)
  window.getSize(w, h)
  # max() functions prevent any divide-by-zero shenanigans
  result.x = (x.float * (3840.0 / max(w.float, 1.0))).int
  result.y = (y.float * (2160.0 / max(h.float, 1.0))).int

proc sdlEventToChromaEvent*(
    window: WindowPtr,
    sdlEvent: Event,
    chromaEvent: var ChromaEvent): bool =
  # converts an SDL2 event to a Chroma event. returns true if the SDL2 event
  # has a Chroma equivalent. false otherwise.
  result = false
  case sdlEvent.kind:
  of TextInput:
    result = true
    chromaEvent = ChromaEvent(kind: eventTextInput)
    chromaEvent.timestamp = sdlEvent.text.timestamp
    chromaEvent.text = ""
    for i, c in sdlEvent.text.text:
      if c == '\0': break
      chromaEvent.text.add c
  of TextEditing:
    result = true
    chromaEvent = ChromaEvent(kind: eventTextEditing)
    chromaEvent.timestamp = sdlEvent.edit.timestamp
    chromaEvent.text = ""
    for i, c in sdlEvent.edit.text:
      if c == '\0': break
      chromaEvent.text.add c
  of KeyDown:
    result = true
    chromaEvent = ChromaEvent(kind: eventKeyDown)
    chromaEvent.timestamp = sdlEvent.key.timestamp
    chromaEvent.keycode = cast[ChromaKeyCode](sdlEvent.key.keySym.sym)
    chromaEvent.repeat = sdlEvent.key.repeat
    let kmod = getModState()
    chromaEvent.shiftPressed = (kmod and KMOD_SHIFT) > 0
    chromaEvent.ctrlPressed = (kmod and KMOD_CTRL) > 0
    chromaEvent.altPressed = (kmod and KMOD_ALT) > 0
    chromaEvent.guiPressed = (kmod and KMOD_GUI) > 0
    let pos = window.scaledMousePos()
    chromaEvent.mousePos.x = pos.x
    chromaEvent.mousePos.y = pos.y
  of KeyUp:
    result = true
    chromaEvent = ChromaEvent(kind: eventKeyUp)
    chromaEvent.timestamp = sdlEvent.key.timestamp
    chromaEvent.keycode = cast[ChromaKeyCode](sdlEvent.key.keySym.sym)
    chromaEvent.repeat = sdlEvent.key.repeat
    let kmod = getModState()
    chromaEvent.shiftPressed = (kmod and KMOD_SHIFT) > 0
    chromaEvent.ctrlPressed = (kmod and KMOD_CTRL) > 0
    chromaEvent.altPressed = (kmod and KMOD_ALT) > 0
    chromaEvent.guiPressed = (kmod and KMOD_GUI) > 0
    let pos = window.scaledMousePos()
    chromaEvent.mousePos.x = pos.x
    chromaEvent.mousePos.y = pos.y
  of MouseButtonDown:
    result = true
    chromaEvent = ChromaEvent(kind: eventMouseDown)
    chromaEvent.timestamp = sdlEvent.button.timestamp
    chromaEvent.button = case sdlEvent.button.button:
      of BUTTON_LEFT: buttonLeft
      of BUTTON_RIGHT: buttonRight
      of BUTTON_MIDDLE: buttonMiddle
      of BUTTON_X1: buttonSpecial1
      of BUTTON_X2: buttonSpecial2
      else: buttonUnknown
    chromaEvent.position.x = sdlEvent.button.x
    chromaEvent.position.y = sdlEvent.button.y
  of MouseButtonUp:
    result = true
    chromaEvent = ChromaEvent(kind: eventMouseUp)
    chromaEvent.timestamp = sdlEvent.button.timestamp
    chromaEvent.button = case sdlEvent.button.button:
      of BUTTON_LEFT: buttonLeft
      of BUTTON_RIGHT: buttonRight
      of BUTTON_MIDDLE: buttonMiddle
      of BUTTON_X1: buttonSpecial1
      of BUTTON_X2: buttonSpecial2
      else: buttonUnknown
    chromaEvent.position.x = sdlEvent.button.x
    chromaEvent.position.y = sdlEvent.button.y
  of MouseWheel:
    result = true
    chromaEvent = ChromaEvent(kind: eventMouseWheel)
    chromaEvent.timestamp = sdlEvent.wheel.timestamp
    chromaEvent.motion.x = sdlEvent.wheel.x
    chromaEvent.motion.y = sdlEvent.wheel.y
    let pos = window.scaledMousePos()
    chromaEvent.position.x = pos.x
    chromaEvent.position.y = pos.y
    chromaEvent.flipped = sdlEvent.wheel.direction == SDL_MOUSEWHEEL_FLIPPED
  of QuitEvent:
    result = true
    chromaEvent = ChromaEvent(kind: eventQuit)
    chromaEvent.timestamp = sdlEvent.quit.timestamp
  of MouseMotion:
    result = true
    chromaEvent = ChromaEvent(kind: eventMouseMotion)
    chromaEvent.timestamp = sdlEvent.motion.timestamp
    chromaEvent.position.x = sdlEvent.motion.x
    chromaEvent.position.y = sdlEvent.motion.y
    chromaEvent.motion.x = sdlEvent.motion.xrel
    chromaEvent.motion.y = sdlEvent.motion.yrel
  else:
    discard


method `width=`*(d: Displayable, width: int) {.base.} = d.destRect.w = width.cint
method `height=`*(d: Displayable, height: int) {.base.} = d.destRect.h = height.cint
method `alpha=`*(d: Displayable, alpha: uint8) {.base.} = d.kalpha = alpha
method `xpos=`*(d: Displayable, x: int) {.base.} = d.destRect.x = x.cint
method `ypos=`*(d: Displayable, y: int) {.base.} = d.destRect.y = y.cint
method `xoffset=`*(d: Displayable, x: int) {.base.} = d.xoff = x.cint
method `yoffset=`*(d: Displayable, y: int) {.base.} = d.yoff = y.cint
method `enabled=`*(d: Displayable, y: bool) {.base.} =
  if y and not d.kenabled:
    d.tenabled = gfxTime()
  d.kenabled = y
method `visible=`*(d: Displayable, y: bool) {.base.} = d.kvisible = y
method `doUpdate=`*(d: Displayable | ContainerDisplayable, y: bool) {.base.} =
  d.kDoUpdate = y
  if y:
    d.printDebug "doUpdate=true for ", d.debugName
  if d.parent != nil and y:
    d.parent.doUpdate = y
method `width`*(d: Displayable): int {.base.} = d.destRect.w
method `height`*(d: Displayable): int {.base.} = d.destRect.h
method `alpha`*(d: Displayable): uint8 {.base.} = d.kalpha
method `xoffset`*(d: Displayable): int {.base.} = d.xoff
method `yoffset`*(d: Displayable): int {.base.} = d.yoff
method `xpos`*(d: Displayable): int {.base.} = d.destRect.x
method `ypos`*(d: Displayable): int {.base.} = d.destRect.y
method `enabled`*(d: Displayable): bool {.base.} = d.kenabled
method `visible`*(d: Displayable): bool {.base.} = d.kvisible
method `doUpdate`*(d: Displayable): bool {.base.} =
  result = d.kDoUpdate
#   if d.parent != nil:
#     result = result or d.parent.doUpdate
method `tEnabled`*(d: Displayable): uint32 {.base.} = d.tenabled

method getRenderer*(d: Displayable): RendererPtr {.base.} = d.renderer

# method setRenderer*(d: Displayable, renderer): RendererPtr {.base.}

method event*(d: Displayable, event: ChromaEvent): bool {.base.} =
  result = false
  if d.onEvent != nil and d.enabled and (d.tenabled <= event.timestamp):
    result = d.onEvent(d, event)

method event*(d: ContainerDisplayable, event: ChromaEvent): bool =
  result = false
  if d.enabled and (d.tenabled <= event.timestamp):
    d.printDebug d.debugName, " Received event ", event
    var i = d.children.len
    while i > 0:
      result = result or d.children[i - 1].event(event)
      if result:
        break
      else:
        i -= 1
    if d.onEvent != nil and not result:
      result = result or d.onEvent(d, event)

# method onEvent*(d: Displayable, doExpr: proc(self: Displayable, event: ChromaEvent): bool) =
#   d.onEvent = doExpr

proc onEventDoNothing*(self: Displayable, event: ChromaEvent): bool =
  false

proc onEventContainerDefault*(self: Displayable, event: ChromaEvent): bool =
  false

method render*(d: Displayable) {.base.} =
  echo "a displayable didn't inherit render:", d.type.name
  assert false #inherit me!

method render*(d: Displayable, masterSrc, masterDest: Rect) {.base.} =
  render(d)

method detachChild*(d: ContainerDisplayable, c: Displayable) {.base.} =
  let i = d.children.find(c)
  if i >= 0:
    d.children.delete i

method addChild*(d: ContainerDisplayable, c: Displayable) {.base.} =
  assert c.parent == nil
  d.children.insert(c, d.children.len)
  c.parent = d

method findChild*(d: ContainerDisplayable, c: Displayable): int {.base.} =
  result = d.children.find(c)

method insertChild*(d: ContainerDisplayable, c: Displayable, index: int) {.base.} =
  d.children.insert(c, index)
  c.parent = d

method cull*(d: Displayable) {.base.} =
  discard

method cull*(d: ContainerDisplayable) =
  for c in d.children:
    c.cull()

method destroy*(d: Displayable) {.base.}
method destroy*(d: ContainerDisplayable) =
  if d.children.len > 0:
    for c in d.children:
      c.parent = nil
      c.destroy()
    d.children = @[]
  if d.surface != nil:
    d.surface.destroy()
    d.surface = nil
  if d.texture != nil:
    d.texture.destroy()
    d.texture = nil

method destroyAllChildren*(d: ContainerDisplayable) {.base.} =
  var cs: seq[Displayable] = @[]
  for c in d.children:
    c.destroy()
    cs.add c
  for c in cs:
    d.detachChild(c)
  

method destroy*(d: Displayable) {.base.} =
  if d.surface != nil and not d.doNotDestroySurface:
    d.surface.destroy()
    d.surface = nil
  if d.texture != nil and not d.doNotDestroyTexture:
    d.texture.destroy()
    d.texture = nil

proc overDisplayable*(event: ChromaEvent, d: Displayable): bool =
  case event.kind:
  of eventKeyDown, eventKeyUp:
    event.mousePos.x >= d.xpos + d.xoff and
    event.mousePos.x <= d.xpos + d.xoff + d.width and
    event.mousePos.y >= d.ypos + d.yoff and
    event.mousePos.y <= d.ypos + d.yoff + d.height
  of eventMouseDown, eventMouseUp, eventMouseMotion, eventMouseWheel:
    event.position.x >= d.xpos + d.xoff and
    event.position.x <= d.xpos + d.xoff + d.width and
    event.position.y >= d.ypos + d.yoff and
    event.position.y <= d.ypos + d.yoff + d.height
  of eventQuit, eventTextEditing, eventTextInput, eventTextFinished:
    true
    
proc `in`*(e: ChromaEvent, d: Displayable): bool = e.overDisplayable(d)
proc `in`*(d: Displayable, e: ChromaEvent): bool = e.overDisplayable(d)
proc `notin`*(e: ChromaEvent, d: Displayable): bool = not e.overDisplayable(d)
proc `notin`*(d: Displayable, e: ChromaEvent): bool = not e.overDisplayable(d)
