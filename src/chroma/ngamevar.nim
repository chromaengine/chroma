#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import macros, json
import chroma/private/asthelpers
export json

var gameVarAst {.compileTime.} = newEmptyNode()

proc writeVar*[T](jn: var JsonNode, name: string, value: T) =
  when T is int:
    jn{name} = %value
  when T is string:
    jn{name} = %value
  when T is seq:
    jn{name} = %value
  when T is bool:
    jn{name} = %value
  when T is tuple:
    jn{name} = %value


proc saveVars*(jn: JsonNode, fname: string) =
  writeFile(fname, jn.pretty())


proc loadBlock(identNode: NimNode): NimNode =
  result = nnkIfStmt.newTree(
    nnkElifBranch.newTree(
      nnkCall.newTree(
        newIdentNode("hasCustomPragma"),
        nnkDotExpr.newTree(
          ident("gamevar"),
          identNode),
        newIdentNode("persistent")),
      nnkStmtList.newTree(
        nnkCommand.newTree(
          newIdentNode("echo"),
          newLit(identNode.strVal),
          newLit(" is persistent")))),
    nnkElse.newTree(
      nnkStmtList.newTree(
        nnkCommand.newTree(
          newIdentNode("echo"),
          newLit(identNode.strVal),
          newLit(" is volatile")))
      ))


proc defaultGameVarsDecl(gamevarInits: seq[NimNode]): NimNode =
  var body = nnkStmtList.newTree()
  for a in gamevarInits:
    body.add nnkIfStmt.newTree(
      nnkElifBranch.newTree(
        nnkPrefix.newTree(
          newIdentNode("not"),
          nnkCall.newTree(
            nnkDotExpr.newTree(
              newIdentNode("jn"),
              newIdentNode("contains")),
            newLit(a[0].strVal))),
        nnkStmtList.newTree(
          nnkAsgn.newTree(
            nnkDotExpr.newTree(
              newIdentNode("gamevar"),
              newIdentNode(a[0].strVal)),
            a[2]))))
  result = nnkProcDef.newTree(
    nnkPostfix.newTree(
      newIdentNode("*"),
      newIdentNode("loadDefaultGamevars")),
    newEmptyNode(),
    newEmptyNode(),
    nnkFormalParams.newTree(
      newEmptyNode(),
      nnkIdentDefs.newTree(
        newIdentNode("gamevar"),
        nnkVarTy.newTree(
          ident("GameVar")),
#         nnkVarTy.newTree(
#           nnkBracketExpr.newTree(
#             newIdentNode("ChromaInternal"),
#             newIdentNode("GameVar")),
#             ),
        newEmptyNode()),
      nnkIdentDefs.newTree(
        newIdentNode("jn"),
        newIdentNode("JsonNode"),
        newEmptyNode())),
    newEmptyNode(),
    newEmptyNode(),
    body)


proc defaultPersistentsDecl(gamevarInits: seq[NimNode]): NimNode =
  var body = nnkStmtList.newTree()
  for a in gamevarInits:
    body.add nnkIfStmt.newTree(
      nnkElifBranch.newTree(
        nnkPrefix.newTree(
          newIdentNode("not"),
          nnkCall.newTree(
            nnkDotExpr.newTree(
              newIdentNode("jn"),
              newIdentNode("contains")),
            newLit(a[0].strVal))),
        nnkStmtList.newTree(
          nnkAsgn.newTree(
            nnkDotExpr.newTree(
              ident("gamevar"),
              newIdentNode(a[0].strVal)),
            a[2]))))
  result = nnkProcDef.newTree(
    nnkPostfix.newTree(
      newIdentNode("*"),
      newIdentNode("loadDefaultPersistents")),
    newEmptyNode(),
    newEmptyNode(),
    nnkFormalParams.newTree(
      newEmptyNode(),
      nnkIdentDefs.newTree(
        newIdentNode("internal"),
#         nnkVarTy.newTree(
          nnkBracketExpr.newTree(
            newIdentNode("ChromaInternal"),
            newIdentNode("GameVar")),
#             ),
        newEmptyNode()),
      nnkIdentDefs.newTree(
        newIdentNode("jn"),
        newIdentNode("JsonNode"),
        newEmptyNode())),
    newEmptyNode(),
    newEmptyNode(),
    body)

proc checkDuplicateNames(ast, errnode: NimNode) =
  assert ast.kind == nnkIdent
  if ast.strVal in fontNames:
    echo errnode.toStrLit
    error "a gamevar cannot have the same name as a font: " & ast.strVal, errnode
  elif ast.strVal in characterNames:
    echo errnode.toStrLit
    error "a gamevar cannot have the same name as a character: " & ast.strVal, errnode
  elif ast.strVal in gamevarNames:
    echo errnode.toStrLit
    error "duplicate gamevar names: " & ast.strVal, errnode
  else:
    gamevarNames.add ast.strVal

proc checkDuplicatePersistentNames(ast, errnode: NimNode) =
  assert ast.kind == nnkIdent
  if ast.strVal in fontNames:
    echo errnode.toStrLit
    error "a persistent cannot have the same name as a font: " & ast.strVal, errnode
  elif ast.strVal in characterNames:
    echo errnode.toStrLit
    error "a persistent cannot have the same name as a character: " & ast.strVal, errnode
  elif ast.strVal in persistentNames:
    echo errnode.toStrLit
    error "duplicate persistent names: " & ast.strVal, errnode
  else:
    persistentNames.add ast.strVal

macro gamevars*(ast: untyped): untyped =
  assert ast.kind == nnkStmtList
  var
    reclist = nnkRecList.newTree()
    persists = nnkRecList.newTree()
    gamevarInits: seq[NimNode] = @[]
    gamevarNoInits: seq[NimNode] = @[]
    persistInits: seq[NimNode] = @[]
    persistNoInits: seq[NimNode] = @[]
  
  for a in ast:
    if a.kind == nnkVarSection: # gamevar def
      checkDuplicateNames(a[0][0], a)
      # identdef: nameIdent, typeIdent
      reclist.add nnkIdentDefs.newTree(nnkPostfix.newTree(newIdentNode("*"), a[0][0]), a[0][1], newEmptyNode())
      if a[0][2].kind != nnkEmpty:
        echo "found gamevar ", a[0][0].strVal, " with initializer ", a[0][2].treeRepr
        gamevarInits.add a[0]
      else:
        echo "found gamevar ", a[0][0].strVal, " no initializer"
        gamevarNoInits.add a[0]
    elif a.kind == nnkCommand and   # persistent def with no initializer
         a[0].kind == nnkIdent and
         a[0].strVal == "persistent" and
         a[1].kind == nnkIdent and
         a[2].kind == nnkStmtList and
         a[2][0].kind in [nnkIdent, nnkBracketExpr]:
      checkDuplicatePersistentNames(a[1], a)
      echo "found persistent ", a[1].strVal, " no initializer"
      var identDef = nnkIdentDefs.newTree(a[1], a[2][0], newEmptyNode())
      persists.add nnkIdentDefs.newTree(nnkPostfix.newTree(newIdentNode("*"), a[1]), a[2][0], newEmptyNode())
      persistNoInits.add identDef
    elif a.kind == nnkCommand and   # persistent def with initializer
         a[0].kind == nnkIdent and
         a[0].strVal == "persistent" and
         a[1].kind == nnkIdent and
         a[2].kind == nnkStmtList and
         a[2][0].kind == nnkAsgn:
      checkDuplicatePersistentNames(a[1], a)
      echo "found persistent ", a[1].strVal, " with initializer"
      persists.add nnkIdentDefs.newTree(nnkPostfix.newTree(newIdentNode("*"), a[1]), a[2][0][0], newEmptyNode())
      persistInits.add nnkIdentDefs.newTree(a[1], a[2][0][0], a[2][0][1])
    else:
      error("illegal gamevar definition: " & a.treeRepr)
  
  reclist.add nnkIdentDefs.newTree(
    nnkPostfix.newTree(
      newIdentNode("*"),
      newIdentNode("persistent")),
    newIdentNode("Persistent"),
    newEmptyNode())
  
  result = nnkStmtList.newTree(
    nnkTypeSection.newTree(
      nnkTypeDef.newTree(
        nnkPostfix.newTree(
          newIdentNode("*"),
          newIdentNode("Persistent")),
        newEmptyNode(),
        nnkObjectTy.newTree(
          newEmptyNode(),
          newEmptyNode(),
          persists))),
    nnkTypeSection.newTree(
      nnkTypeDef.newTree(
        nnkPostfix.newTree(
          newIdentNode("*"),
          newIdentNode("GameVar")),
        newEmptyNode(),
        nnkObjectTy.newTree(
          newEmptyNode(),
          newEmptyNode(),
          reclist))),
    defaultGameVarsDecl gamevarInits,
    defaultPersistentsDecl persistInits)
#   error result.toStrLit.strVal
  
macro genGamevars*(): untyped = gameVarAst
