#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import nimAES, base64, strutils

# a janky encryption and decryption tool. primarily here
# as a placeholder so a better encryption can be used in the future.

proc padStringTo16(s: string): string =
  result = "00"
  result.add s
  var n = 0
  while result.len mod 16 != 0:
    result.add " "
    n += 1
  result[0..1] = intToStr(n, 2)

proc jankEncrypt*(key: string, plaintext: string): string =
  assert key.len == 16
  result = ""
  var padded = padStringTo16(plaintext)
  var i = 0
  var aes = initAes()
  if aes.setEncodeKey(key):
    while i < padded.len:
      result.add aes.encryptECB(padded[i..i+15])
      i += 16
  result = encode(result)

proc jankDecrypt*(key: string, ciphertext: string): string =
  assert key.len == 16
  result = ""
  var decoded = decode(ciphertext)
  var i = 0
  var aes = initAes()
  if aes.setDecodeKey(key):
    while i < decoded.len:
      result.add aes.decryptECB(decoded[i..i+15])
      i += 16
  let padlen = parseInt(result[0..1])  
  result = result[2..^padlen+1]

proc jankDecryptFile*(key: string, fname: string): string =
  result = jankDecrypt(key, readFile(fname))
