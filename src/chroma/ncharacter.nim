#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import chroma
import macros, os, times, experimental/diff, strutils, tables

import chroma/private/asthelpers

var characterProcSyms {.compileTime.}: seq[NimNode] = @[]
var characterTrees {.compileTime.}: seq[NimNode] = @[]

proc makeLayerVariant(ast: NimNode): NimNode =
  # proc(internal: var ChromaInternal[T]): string = <ast>
  var res = convTree(ident("internal"), ast)
  result = nnkLambda.newTree(
    newEmptyNode(), newEmptyNode(), newEmptyNode(),
    nnkFormalParams.newTree(
      newIdentNode("string"),
      nnkIdentDefs.newTree(
        newIdentNode("internal"),
#         nnkVarTy.newTree(
          nnkBracketExpr.newTree(
            newIdentNode("ChromaInternal"),
            newIdentNode("GameVar")),
            #),
        newEmptyNode())),
    newEmptyNode(),
    newEmptyNode(),
    nnkStmtList.newTree(
      nnkAsgn.newTree(
        newIdentNode("result"),
        res)))


proc processVariant(ast: NimNode): tuple[id, procDef: NimNode] =
  if ast.kind != nnkAsgn:
    errorGotExpected(ast, nnkAsgn.newTree())
  if ast[0].kind != nnkIdent:
    errorGotExpected(ast[0], ident(""))
  if ast[1].kind notin {nnkStrLit, nnkIfExpr}:
    error "variant should either be a string or an expression returning strings", ast[1]
  result.id = newLit(ast[0].strVal)
  result.procDef = makeLayerVariant(ast[1])


proc characterLayerInit(characterName: string, prefix, ast: NimNode): NimNode =
  assert ast.kind == nnkCommand
  assert ast[0].kind == nnkIdent
  assert ast[0].strVal == "layer"
  if ast.len == 2:
    if ast[1].kind == nnkStmtList:
      error "expected a layer name before layer properties", ast
    elif ast[1].kind == nnkIdent:
      error "layer definition requires a list of properties", ast
    else:
      error "incorrect syntax in layer definition, expected a layer name and a list of layer properties", ast
  if ast.len > 3:
    error "too many arguments given for layer definition", ast
  if ast[1].kind != nnkIdent:
    errorGotExpected(ast[1], ident(""))
  if ast[2].kind != nnkStmtList:
    errorGotExpected(ast[1], nnkStmtList.newTree())
  
  let
    layerName = ast[1].strVal
    layerNameStr = newLit(ast[1].strVal)

  result = nnkStmtList.newTree()
  
  result.add quote do:
    `prefix`.layers[`layerNameStr`] = newOrderedTable[string, proc(internal: ChromaInternal[T]): string]()
  
  let
    variantStmts = ast[2]
    firstVariant = processVariant(variantStmts[0])
    fvId = firstVariant.id
    fvProc = firstVariant.procDef
  
  if characterAttrs[characterName].hasKey fvId.strVal:
    error "character '" & characterName & "' already has a variant named '" & fvId.strVal & "'", variantStmts[0]
  characterAttrs[characterName][fvId.strVal] = layerName
  
  result.add quote do:
    `prefix`.layerOpts[`layerNameStr`] = `fvId`
    `prefix`.layers[`layerNameStr`][`fvId`] = `fvProc`
  
  if variantStmts.len > 1:
    for va in variantStmts[1..^1]:
      let
        variant = processVariant(va)
        vId = variant.id
        vProc = variant.procDef
      if characterAttrs[characterName].hasKey vId.strVal:
        error "character '" & characterName & "' already has a variant named '" & vId.strVal & "'", va
      characterAttrs[characterName][vId.strVal] = layerName
      result.add quote do:
        `prefix`.layers[`layerNameStr`][`vId`] = `vProc`
  

proc createCharacterProc(id, ast: NimNode): NimNode =
  assert id.kind == nnkIdent
  assert ast.kind == nnkStmtList
  var layerNames: seq[string] = @[]
  let
    procSym = genSym(nskProc, id.strVal & "CharacterInit")
    internalSym = ident("internal")
    chrSym = genSym(nskVar, id.strVal)
    characterIdStr = newLit(id.strVal)
    characterId = id.strVal
  
  characterProcSyms.add procSym
  
  result = nnkStmtList.newTree()
  
  for a in ast:
    if a.kind == nnkCommand and a[0].kind == nnkIdent and a[0].strVal == "layer":
      if a[1].kind == nnkIdent and a[1].strVal in layerNames:
        error "character " & characterId & " already has a layer with name " & a[1].strVal, a[1]
      layerNames.add a[1].strVal
      let layerStmts = characterLayerInit(characterId, chrSym, a)
      for ls in layerStmts:
        result.add ls
    else:
      collapseTree(result, chrSym, a)
  
  result = quote do:
    proc `procSym`*[T](`internalSym`: ChromaInternal[T]) =
      var `chrSym` = newCharacter[T](`characterIdStr`)
      `result`
      `internalSym`.character[`characterIdStr`] = `chrSym`


macro character*(id, ast: untyped) =
  if id.kind != nnkIdent:
    error "not a valid name for a character: " & id.toStrLit.strVal, id
  if ast.kind != nnkStmtList:
    error "invalid syntax for a dialogue definition", ast
  if id.strVal in characterNames:
    error "a character with with name '" & id.strVal & "' already exists!", id
  if (id.strVal in reservedKeywords) or (id.strVal in reservedCommands):
    error "cannot name a dialogue '" & id.strVal & "' because " & id.strVal & " is a reserved keyword.", id
  if id.strVal in dialogueDispNames:
    error "a character and a dialogue cannot have the same name!", id
  
  characterNames.add id.strVal
  characterAttrs[id.strVal] = newTable[string, string]()
  
  characterTrees.add createCharacterProc(id, ast)

  
macro genCharacterInitializer*(): untyped =
  let procSym = ident("generatedCharacters")
  let internalSym = genSym(nskParam, "internal")
  result = nnkStmtList.newTree()
  for a in characterTrees:
    result.add a
  
  var procBody = nnkStmtList.newTree()
  for s in characterProcSyms:
    procBody.add quote do:
      `s`(`internalSym`)

  result.add quote do:
    proc `procSym`[T](`internalSym`: ChromaInternal[T]) =
      echo "loading characters"
      `procBody`

