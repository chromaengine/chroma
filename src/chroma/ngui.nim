#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import sdl2
import sequtils, macros, strutils, unicode, sugar
import chroma/displayable
import chroma/displayables/[simpletext, simpleimage, imagebutton, vbox, hbox, rectangle, textentry, slider, vscroll]
import chroma/private/[imagecache, fontcache, asthelpers]
export fontcache

var guiInitProcIdents {.compileTime.}: seq[NimNode] = @[]
var guiInitPreambleAst {.compileTime.} = nnkStmtList.newTree()
var guiInitAst {.compileTime.} = nnkStmtList.newTree()


proc identDefsToVarTuple(asts: seq[NimNode], assigner: NimNode): NimNode =
  result = nnkVarTuple.newTree()
  for a in asts:
    assert a.kind == nnkIdentDefs
    result.add a[0]
  result.add newEmptyNode()
  result.add assigner
  result = nnkLetSection.newTree(result)

  
proc initializerFromCallIdent(id, rootIdent, internalIdent: NimNode): NimNode =
  assert id.kind == nnkIdent
  case id.strVal:
  of "text":
    result = quote do:
      newSimpleText(`rootIdent`.getRenderer(), `internalIdent`.fontCache)
  of "textentry":
    result = quote do:
      newTextEntry(`rootIdent`.getRenderer(), `internalIdent`.fontCache)
  of "image":
    result = quote do:
      newSimpleImage(`rootIdent`.getRenderer(), `internalIdent`.imageCache)
  of "imagebutton":
    result = quote do:
      newImageButton(`rootIdent`.getRenderer(),
        `internalIdent`.imageCache,
        `internalIdent`.fontCache)
  of "vslide":
    result = quote do:
      newVSlider(`rootIdent`.getRenderer())
  of "hslide":
    result = quote do:
      newHSlider(`rootIdent`.getRenderer())
  of "vscroll":
    result = quote do:
      newVscroll(`rootIdent`.getRenderer())
  of "rectangle":
    result = quote do:
      newRectangle(`rootIdent`.getRenderer())
  of "vbox":
    result = quote do:
      newVbox()
  of "hbox":
    result = quote do:
      newHbox()
  of "frame":
    result = quote do:
      newFrame(`rootIdent`.getRenderer())
  else:
    error "unrecognized displayable: " & id.strVal, id

proc typeFromCallIdent(id: NimNode): NimNode =
  assert id.kind == nnkIdent
  case id.strVal:
  of "text":
    result = ident("SimpleText")
  of "textentry":
    result = ident("TextEntry")
  of "image":
    result = ident("SimpleImage")
  of "imagebutton":
    result = ident("ImageButton")
  of "vslide":
    result = ident("VSlider")
  of "hslide":
    result = ident("HSlider")
  of "vscroll":
    result = ident("VScroll")
  of "rectangle":
    result = ident("Rectangle")
  of "vbox":
    result = ident("Vbox")
  of "hbox":
    result = ident("Hbox")
  of "frame":
    result = ident("Frame")
  else:
    error "unrecognized displayable: " & id.strVal, id

proc processAst*(
    preamble: var NimNode,
    internalPrefix, dispPrefix, rootIdent: NimNode,
    ast: NimNode): NimNode =
  # processAst handles the "UI building" side of the ui AST. e.g. in...
  #   ui foo[100, 200](someArg: bool):
  #     xpos = 123
  #     ypos = 456
  #     if someArg:
  #       text:
  #         value = "foo"
  # ...processAst will:
  #    - manipulate the idents `xpos`, `ypos`, `text`, and `value`. 
  #    - pass `123`, `456`, `someArg`, and `"foo"` into convTree
  case ast.kind:
  of nnkAsgn:
    let asgnId = nnkDotExpr.newTree(
      dispPrefix,
      ast[0])
    let asgnVal = convTree(internalPrefix, ast[1])
    result = nnkAsgn.newTree(asgnId, asgnVal)
  of nnkCommand:
    if ast.len == 3 and ast[0].kind == nnkIdent and ast[0].strVal == "add":
      expect ast[1], nnkCall
      expect ast[2], nnkStmtList
      result = nnkStmtList.newTree()
      let childSym = genSym(nskVar)
      let childInit = ast[1]
      result.add quote do:
        var `childSym` = `childInit`
      for a in ast[2]:
        result.add processAst(preamble, internalPrefix, childSym, rootIdent, a)
      result.add quote do:
        `dispPrefix`.addChild(`childSym`)
    else:
      result = convTree(internalPrefix, ast)
  of nnkCall:
    if ast[0].kind == nnkIdent and ast[0].strVal == "raw":
      result = ast[1]
    else:
      expect ast[0], nnkIdent
      if ast.len == 2:
        expect ast[1], nnkStmtList
      elif ast.len == 3:
        expect ast[1], nnkIdent
        expect ast[2], nnkStmtList
      else:
        error "too many arguments for block declaration", ast[0]
      result = nnkStmtList.newTree()
      let (childSym, childBody) =
        if ast.len == 2:
          (genSym(nskVar, ast[0].strVal), ast[1])
        else:
          (ast[1], ast[2])
      let childInit = initializerFromCallIdent(ast[0], rootIdent, internalPrefix)
      let typeIdent = typeFromCallIdent(ast[0])
      preamble.add quote do:
        var `childSym` = `childInit`
#       result.add quote do:
#         `childSym` = `childInit`
      for a in childBody:
        result.add processAst(preamble, internalPrefix, childSym, rootIdent, a)
      if ast[0].kind == nnkIdent and ast[0].strVal == "textentry":
        # default onEvent for textentries
        result.add quote do:
          `childSym`.onEvent = proc(disp: Displayable, event: ChromaEvent): bool =
            let txt = TextEntry(disp)
            case event.kind:
            of eventMouseDown:
              if event in txt and not txt.active:
                txt.active = true
                internal.startTextEntry(txt)
                result = true
              elif not (event in txt):
                internal.stopTextEntry()
              txt.doUpdate = event in txt
            of eventTextFinished:
              txt.active = false
              txt.onTextChanged(txt, txt.runes)
              result = false
            else:
              if txt.active:
                result = true
      result.add quote do:
        `dispPrefix`.addChild(`childSym`)
  of nnkStmtList, nnkStmtListExpr:
    # process all members as ast
    var newPreamble = nnkStmtList.newTree()
    var stmtList = nnkStmtList.newTree()
    result = ast.kind.newTree()
    for a in ast:
      stmtList.add processAst(newPreamble, internalPrefix, dispPrefix, rootIdent, a)
    for a in newPreamble:
      result.add a
    for a in stmtList:
      result.add a
  of nnkInfix, nnkBracketExpr, nnkPar, nnkIfExpr, nnkIfStmt,
     nnkElseExpr, nnkElse, nnkPrefix, nnkTryStmt, nnkFinally, 
     nnkIdentDefs, nnkMixinStmt, nnkBindStmt, 
     nnkPostfix, nnkFormalParams, nnkProcDef, nnkIteratorDef, 
     nnkExprColonExpr, nnkExprEqExpr, nnkDo,
     nnkTableConstr, nnkLambda, nnkRange, nnkDiscardStmt, nnkBlockStmt, nnkVarTuple:
    # process all members as ast
    result = ast.kind.newTree()
    for a in ast:
      result.add processAst(preamble, internalPrefix, dispPrefix, rootIdent, a)
  of nnkElifBranch, nnkElifExpr, nnkWhileStmt, nnkExceptBranch,
     nnkOfBranch:
    # process the first n-1 members as arg, then the last member as ast
    result = ast.kind.newTree()
    for a in ast[0..^2]:
      result.add convTree(internalPrefix, a)
    result.add processAst(preamble, internalPrefix, dispPrefix, rootIdent, ast[^1])
  of nnkForStmt:
    result = ast.kind.newTree()
    var body = nnkStmtList.newTree()
    for a in ast[0..^3]:
      let sym = genSym(nskForVar, a.toStrLit.strVal)
      result.add sym
      body.add quote do:
        let `a` = `sym`
    result.add convTree(internalPrefix, ast[^2])
    body.add processAst(preamble, internalPrefix, dispPrefix, rootIdent, ast[^1])
    var cscope = nnkCall.newTree()
    cscope.add ident("closureScope")
    cscope.add body
    result.add cscope
#   of :
#     process the first n-1 members as arg, then the last member as ast
#     result = ast.kind.newTree()
#     for a in ast[0..^2]:
#       result.add convTree(internalPrefix, a)
#     let res = processAst(preamble, internalPrefix, dispPrefix, rootIdent, ast[^1])
#     var enclose = nnkCommand.newTree(ident("capture"))
#     for a in ast[0..^3]:
#       enclose.add a
#     enclose.add res
#     result.add enclose
  of nnkCaseStmt:
    # process first member as arg, then last n-1 members as ast
    result = ast.kind.newTree()
    result.add convTree(internalPrefix, ast[0])
    for a in ast[1..^1]:
      result.add processAst(preamble, internalPrefix, dispPrefix, rootIdent, a)
  else:
    result = convTree(internalPrefix, ast)

proc createInitProc(
    result: var NimNode,
    uiIdent, argPackTy, gameUiRootTy, initializerIdent, width, height, ast: NimNode,
    argPackMembers: seq[NimNode]) =
  assert uiIdent.kind == nnkIdent
  assert width.kind == nnkIntLit
  assert height.kind == nnkIntLit
  assert ast.kind == nnkStmtList
  let
    procIdent = genSym(nskProc, uiIdent.strVal & "GuiInit")
    iIdent = genSym(nskParam, "internal")
    aIdent = genSym(nskParam, "argpack")
    uiObfuscatedIdent = ident("uiElement")
#     uiObfuscatedIdent = genSym(nskLet, uiIdent.strVal) # mangled version of uiIdent
    iTrueIdent = ident("internal")
    uiNameStr = newLit(uiIdent.strVal)
  let
    deconstrTup = 
      if argPackMembers.len > 0:
        identDefsToVarTuple(argPackMembers, quote do: `argPackTy`(`aIdent`).args)
      else:
        newEmptyNode()
  guiInitProcIdents.add procIdent
  
  var preamble = nnkStmtList.newTree()
  
  let fullTree = processAst(preamble,
    iIdent, uiObfuscatedIdent, uiObfuscatedIdent, ast)
  
  result.add quote do:
    proc `procIdent`[GameVar](`iIdent`: ChromaInternal[GameVar]) =
      let `uiObfuscatedIdent` = `initializerIdent`(
        `iIdent`.renderer,
        `iIdent`.imageCache,
        `iIdent`.fontCache,
        `width`, `height`)
      `uiObfuscatedIdent`.onUpdate = proc(`iTrueIdent`: ChromaInternal[GameVar], `aIdent`: ArgPack): bool =
        `preamble`
        `deconstrTup`
        `fullTree`
      `iIdent`.guiElements[`uiNameStr`] = `uiObfuscatedIdent`

proc createArgPackSubclass(
    result, argPackTy: var NimNode,
    uiIdent: NimNode,
    argPackMembers: seq[NimNode]) =
  # create the subclass of ArgPack that will be passed into this
  # ui's update proc.
  var tupty = nnkTupleTy.newTree()
  for a in argPackMembers:
    tupty.add a
  let argsIdent = ident("args")
  
#   argPackTy = genSym(nskType, "ArgPack_" & uiIdent.strVal)
  argPackTy = ident("ArgPack_" & uiIdent.strVal)
  result.add quote do:
    type `argPackTy`* = ref object of ArgPack
      `argsIdent`*: `tupty`

proc createGameUiRootSubclass(
    result, gameUiRootTy, initializerIdent: var NimNode,
    uiIdent, argPackTy: NimNode) =
  # create the subclass of GameUiRoot that will represent this ui.
  # also create its initializer proc.
  gameUiRootTy = ident("GameUiRoot_" & uiIdent.strVal)
  initializerIdent = genSym(nskProc, "newGameUiRoot_" & uiIdent.strVal)
  let uilit = newLit(uiIdent.strVal)
  result.add quote do:
    type `gameUiRootTy`* = ref object of GameUiRoot

    proc `initializerIdent`*(
        renderer: RendererPtr,
        imageCache: ImageCache,
        fontCache: FontCache,
        width: range[1..3840],
        height: range[1..2160]): `gameUiRootTy` =
      new result
      echo "initializing ", `uilit`
      result.renderer = renderer
      result.destRect = rect(0.cint, 0.cint, width.cint, height.cint)
      result.srcRect = rect(0.cint, 0.cint, width.cint, height.cint)
      result.surface = createRgbSurface(0.cint,
        width.cint, height.cint, 32.cint,
        0xff.uint32, 0xff00.uint32, 0xff0000.uint32, 0xff000000.uint32)
      result.childRenderer = result.surface.createSoftwareRenderer()
      result.imageCache = imageCache
      result.fontCache = fontCache
      result.doUpdate = true
      result.visible = false
      result.enabled = false
      result.alpha = 255
      result.onEvent = proc(d: Displayable, e: ChromaEvent): bool =
        (e in d) # ui blocks should always capture any event that occurs within their bounds
      result.children = @[]
      result.onUpdateHook = proc(internal: ChromaInternal[GameVar], argpack: ArgPack): bool =
        false

proc createShow(
    result: var NimNode,
    gameUiRootTy, argPackTy: NimNode,
    argPackMembers: seq[NimNode]) =
  let
    gameRootArg = genSym(nskParam, "root")
    internalArg = genSym(nskParam, "internal")
    animationsArg = genSym(nskParam, "animations")
  # (root: GameUiRoot_id, internal: Internal, animations: seq[ShowArg], (each argpack member))
  var procArgs = nnkFormalParams.newTree(
    newEmptyNode(), # return type
    nnkIdentDefs.newTree(
      gameRootArg,
      gameUiRootTy, 
      newEmptyNode()),
    nnkIdentDefs.newTree(
      internalArg,
      ident("ChromaInternal"),
      newEmptyNode()),
    nnkIdentDefs.newTree(
      animationsArg,
      nnkBracketExpr.newTree(ident("seq"), ident("ShowArg")),
      newEmptyNode()))
  for a in argPackMembers:
    procArgs.add a
  
  var argPackConstr = nnkPar.newTree()
  for a in argPackMembers:
    argPackConstr.add nnkExprColonExpr.newTree(a[0], a[0])
  
  let stmtList = quote do:
    var anims = newSeq[Animation]()
    for a in `animationsArg`:
      case a.kind:
      of sarAnimation:
        anims.add a.animation
      of sarNothing:
        discard
      else:
        echo "WARNING: invalid showArg given to ui.show call"
    let ap = `argPackTy`(args: `argPackConstr`)
    `gameRootArg`.show(`internalArg`, anims, ap)
  
  result.add nnkProcDef.newTree(
    nnkPostfix.newTree(ident("*"), ident("show")),
    newEmptyNode(),
    newEmptyNode(),
    procArgs,
    newEmptyNode(),
    newEmptyNode(),
    stmtList)

proc createRefresh(
    result: var NimNode,
    gameUiRootTy, argPackTy: NimNode,
    argPackMembers: seq[NimNode]) =
  let
    gameRootArg = genSym(nskParam, "root")
    internalArg = genSym(nskParam, "internal")
    animationsArg = genSym(nskParam, "animations")
  var procArgs = nnkFormalParams.newTree(
    newEmptyNode(), # return type
    nnkIdentDefs.newTree(
      gameRootArg,
      gameUiRootTy, 
      newEmptyNode()),
    nnkIdentDefs.newTree(
      internalArg,
      ident("ChromaInternal"),
      newEmptyNode()),
    nnkIdentDefs.newTree(
      animationsArg,
      nnkBracketExpr.newTree(ident("seq"), ident("Animation")),
      newEmptyNode()))
  for a in argPackMembers:
    procArgs.add a
  
  var argPackConstr = nnkPar.newTree()
  for a in argPackMembers:
    argPackConstr.add nnkExprColonExpr.newTree(a[0], a[0])
  
  let stmtList = quote do:
    let ap = `argPackTy`(args: `argPackConstr`)
    `gameRootArg`.refresh(`internalArg`, `animationsArg`, ap)
  
  result.add nnkProcDef.newTree(
    nnkPostfix.newTree(ident("*"), ident("refresh")),
    newEmptyNode(),
    newEmptyNode(),
    procArgs,
    newEmptyNode(),
    newEmptyNode(),
    stmtList)


proc createInit(
    result: var NimNode,
    gameUiRootTy, argPackTy: NimNode) =
  let uiIdent = newLit(gameUiRootTy.strVal)
  result.add quote do:
    method init*(d: `gameUiRootTy`, internal: ChromaInternal[GameVar]) =
      echo "init: ", `uiIdent`
      let ap = `argPackTy`()
      discard d.update(internal, ap)

macro ui*(id, ast: untyped) =
  var
    argPackMembers: seq[NimNode] = @[]
    uiIdent: NimNode = newEmptyNode()
    widthLit: NimNode = newEmptyNode()
    heightLit: NimNode = newEmptyNode()

  case id.kind:
  of nnkBracketExpr:
    expect id[0], nnkIdent
    expect id[1], nnkIntLit
    expect id[2], nnkIntLit
    if id.len != 3:
      error "incorrect number of parameters for ui definition", id
    uiIdent = id[0]
    widthLit = id[1]
    heightLit = id[2]
  of nnkObjConstr, nnkCall:
    expect id[0][0], nnkIdent
    expect id[0][1], nnkIntLit
    expect id[0][2], nnkIntLit
    if id[0].len != 3:
      error "incorrect number of parameters for ui definition", id[0]
    uiIdent = id[0][0]
    widthLit = id[0][1]
    heightLit = id[0][2]
    for a in id[1..^1]:
      expect a, nnkExprColonExpr
      expect a[0], nnkIdent
      argPackMembers.add exprColonExprToIdentDef(a)
  else:
    echo id.toStrLit
    error "invalid identifier for ui element", id
#   argPackMembers.add nnkIdentDefs.newTree(
#     ident("sdfsdfsdfsdfsdf"), ident("int"), newEmptyNode())
  
  expect ast, nnkStmtList
  
  guiElementNames.add uiIdent.strVal
  
  var
    argPackTy = newEmptyNode()
    gameUiRootTy = newEmptyNode()
    initializerIdent = newEmptyNode()
  
  guiInitPreambleAst.createArgPackSubclass(argPackTy, uiIdent, argPackMembers)
  assert argPackTy.kind != nnkEmpty
  
  guiInitPreambleAst.createGameUiRootSubclass(gameUiRootTy, initializerIdent, uiIdent, argPackTy)
  assert gameUiRootTy.kind != nnkEmpty
  assert initializerIdent.kind != nnkEmpty
  
  guiInitAst.createShow(gameUiRootTy, argPackTy, argPackMembers)
  
  guiInitAst.createRefresh(gameUiRootTy, argPackTy, argPackMembers)
  
  guiInitAst.createInit(gameUiRootTy, argPackTy)
  
  guiInitAst.createInitProc(uiIdent, argPackTy, gameUiRootTy, initializerIdent, widthLit, heightLit, ast, argPackMembers)
  
  guiTypeData.add (
    name: uiIdent.strVal,
    argTyIdent: argPackTy,
    objTyIdent: gameUiRootTy)


macro genUiInitializers*(): untyped =
  result = guiInitPreambleAst
  for g in guiInitAst:
    result.add g
  let
    initIdent = ident("generatedUiElements")
    internalIdent = genSym(nskParam, "internal")
  var calls = nnkStmtList.newTree()
  for p in guiInitProcIdents:
    calls.add quote do:
      `p`(`internalIdent`)
  result.add quote do:
    proc `initIdent`[GameVar](`internalIdent`: ChromaInternal[GameVar]) =
      `calls`
#   echo result.toStrLit

#   foo do (rect: Displayable, event: ChromaEvent): bool =
#     false
