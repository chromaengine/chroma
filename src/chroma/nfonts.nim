#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import macros
import chroma
import chroma/private/[asthelpers, fontcache]
export fontcache

var fontProcSyms {.compileTime.}: seq[NimNode] = @[]

proc alterFontVariantAsgn(ast: NimNode): NimNode =
  if ast.kind != nnkAsgn or ast.len != 2:
    echo ast.toStrLit
    error "invalid syntax, was expecting a font variant", ast
  let
    variantProc = ident(ast[0].strVal & "FontVariant")
  result = nnkCall.newTree(variantProc, ast[1])

proc createFontProc(id, ast: NimNode): NimNode =
  assert id.kind == nnkIdent
  assert ast.kind == nnkStmtList
  let procSym = genSym(nskProc, id.strVal & "FontInit")
  let internalSym = genSym(nskParam, "internal")
  let fontNameStr = newLit(id.strVal)
  fontProcSyms.add procSym
  
  result = nnkCall.newTree()
  result.add quote do:
    `internalSym`.fontCache.add
  result.add newLit(id.strVal)
  result.add alterFontVariantAsgn(ast[0])
  for a in ast:
    result.add alterFontVariantAsgn(a)
  
  result = quote do:
    proc `procSym`*[T](`internalSym`: ChromaInternal[T]) =
      echo "loading font '", `fontNameStr`, "'"
      `result`
#   echo result.toStrLit

macro font*(id, ast: untyped): untyped =
  if id.kind != nnkIdent:
    error "not a valid name for a font: " & id.toStrLit.strVal, id
  if ast.kind != nnkStmtList:
    error "invalid syntax for a font definition", ast
  if id.strVal in fontNames:
    error "a font with name '" & id.strVal & "' already exists!", id
  fontNames.add id.strVal
  createFontProc(id, ast)

macro font*(ast: untyped): untyped =
  if ast.kind != nnkStmtList:
    error "invalid syntax for a font definition", ast
  if "default" in fontNames:
    error "a default font already exists!", ast
  fontNames.add "default"
  createFontProc(ident("default"), ast)

macro genFontInitializer*(): untyped =
  let iprocsym = ident("generatedFonts")
  let internalSym = genSym(nskParam, "internal")
  result = nnkStmtList.newTree()
  for s in fontProcSyms:
    result.add quote do:
      `s`(`internalSym`)
  
  result = quote do:
    proc `iprocsym`[T](`internalSym`: ChromaInternal[T]) =
      `internalSym`.fontCache = initFontCache()
      `result`
#   echo result.toStrLit
