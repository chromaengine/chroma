#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import macros
import chroma
import chroma/private/[asthelpers, audiomanager]

var audioDefSyms {.compileTime.}: seq[NimNode] = @[]
