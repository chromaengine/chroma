#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import sdl2
import chroma/displayable, chroma/private/[fontcache, imagecache]
import chroma/displayables/[rectangle, simpletext, simpleimage]

type
  MenuButton* = ref object of Displayable
    txt: SimpleText
    img, hoverImg: SimpleImage
    imc: ImageCache

proc newMenuButton*(renderer: RendererPtr, imageCache: ImageCache, fontCache: FontCache): MenuButton =
  new result
  result.destRect = rect(0.cint, 0.cint, 0.cint, 0.cint)
  result.imc = imageCache
  result.renderer = renderer
  result.txt = newSimpleText(renderer, fontCache)
  result.txt.font = "default"
  result.txt.size = 80
  result.txt.align = Center
  result.visible = true
  result.enabled = true
  result.onEvent = proc(d: Displayable, e: ChromaEvent): bool =
    let mb = cast[MenuButton](d)
    case e.kind:
    of eventMouseDown:
      if e.overDisplayable mb:
        mb.hoverImg.visible = false
        mb.img.visible = true
        result = true
    of eventMouseMotion:
      if e.overDisplayable mb:
        mb.hoverImg.visible = true
        mb.img.visible = false
      else:
        mb.hoverImg.visible = false
        mb.img.visible = true
    else: discard
  result.width = 1800
  result.height = 140

method `width=`*(mb: MenuButton, w: int) =
  mb.destRect.w = w.cint
  mb.txt.width = w
  if mb.img != nil:
    mb.img.width = w
  if mb.hoverImg != nil:
    mb.hoverImg.width = w

method `height=`*(mb: MenuButton, h: int) =
  mb.destRect.h = h.cint
  mb.txt.height = h
  if mb.img != nil:
    mb.img.height = h
  if mb.hoverImg != nil:
    mb.hoverImg.height = h

method `xpos=`*(mb: MenuButton, x: int) =
  mb.destRect.x = x.cint
  mb.txt.xpos = x
  if mb.img != nil:
    mb.img.xpos = x
  if mb.hoverImg != nil:
    mb.hoverImg.xpos = x

method `ypos=`*(mb: MenuButton, y: int) =
  mb.destRect.y = y.cint
  mb.txt.ypos = y
  if mb.img != nil:
    mb.img.ypos = y
  if mb.hoverImg != nil:
    mb.hoverImg.ypos = y

method `xoffset=`*(mb: MenuButton, x: int) =
  mb.txt.xoffset = x

method `yoffset=`*(mb: MenuButton, y: int) =
  mb.txt.yoffset = y

method `image=`*(mb: MenuButton, path: string) =
  if mb.img == nil:
    mb.img = newSimpleImage(mb.renderer, mb.imc, path)
    mb.img.xpos = mb.xpos
    mb.img.ypos = mb.ypos
    mb.img.width = mb.width

method `hoverImage=`*(mb: MenuButton, path: string) =
  if mb.hoverImg == nil:
    mb.hoverImg = newSimpleImage(mb.renderer, mb.imc, path)
    mb.hoverImg.xpos = mb.xpos
    mb.hoverImg.ypos = mb.ypos
    mb.hoverImg.width = mb.width
    mb.hoverImg.visible = false

method `textArea`*(mb: MenuButton): SimpleText = mb.txt

method render*(mb: MenuButton) =
  if mb.hoverImg != nil and mb.hoverImg.visible:
    mb.hoverImg.render()
  if mb.img != nil and mb.img.visible:
    mb.img.render()
  mb.txt.render()

method destroy*(mb: MenuButton) =
  if mb.hoverImg != nil: mb.hoverImg.destroy()
  if mb.img != nil: mb.img.destroy()
  mb.txt.destroy()
