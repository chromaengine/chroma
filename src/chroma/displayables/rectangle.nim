#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import sdl2, sdl2/[ttf, image]
import chroma/jankcrypt, chroma/displayable
import chroma/private/sdlcircle

type
  Rectangle* = ref object of Displayable
    color: Color
    crad: int
    surfaceRenderer: RendererPtr
    topLeft, bottomLeft, topRight, bottomRight: bool
    lastRender: uint32

proc newRectangle*(renderer: RendererPtr): Rectangle =
  new result
  result.renderer = renderer
  result.doUpdate = true
  result.color = color(0, 0, 0, 255)
  result.kalpha = 255
  result.xoff = 0
  result.yoff = 0
  result.topLeft = on
  result.bottomLeft = on
  result.topRight = on
  result.bottomRight = on
  result.enabled = true
  result.visible = true

method `width=`*(d: Rectangle, width: int) =
  if d.destRect.w != width.cint:
    d.doUpdate = true
  d.destRect.w = width.cint

method `height=`*(d: Rectangle, height: int) =
  if d.destRect.h != height.cint:
    d.doUpdate = true
  d.destRect.h = height.cint

method `xpos=`*(d: Rectangle, x: int) =
  d.destRect.x = x.cint

method `ypos=`*(d: Rectangle, y: int) =
  d.destRect.y = y.cint

method `radius=`*(d: Rectangle, c: int) =
  if d.crad != c:
    d.doUpdate = true
  d.crad = c

method `fill=`*(d: Rectangle, c: (int, int, int)) =
  d.color.r = minmax(0, c[0], 255).uint8
  d.color.g = minmax(0, c[1], 255).uint8
  d.color.b = minmax(0, c[2], 255).uint8

method `r=`*(d: Rectangle, c: int) =
  d.color.r = minmax(0, c, 255).uint8

method `g=`*(d: Rectangle, c: int) =
  d.color.g = minmax(0, c, 255).uint8

method `b=`*(d: Rectangle, c: int) =
  d.color.b = minmax(0, c, 255).uint8

method `corners=`*(d: Rectangle, c: (bool, bool, bool, bool)) =
  if d.topLeft != c[0]:
    d.topLeft = c[0]
    d.doUpdate = true
  if d.topRight != c[1]:
    d.topRight = c[1]
    d.doUpdate = true
  if d.bottomRight != c[2]:
    d.bottomRight = c[2]
    d.doUpdate = true
  if d.bottomLeft != c[3]:
    d.bottomLeft = c[3]
    d.doUpdate = true

method `width`*(d: Rectangle): int = d.destRect.w.int
method `height`*(d: Rectangle): int = d.destRect.h.int
method `xpos`*(d: Rectangle): int = d.destRect.x.int
method `ypos`*(d: Rectangle): int = d.destRect.y.int
method `r`*(d: Rectangle): int = d.color.r.int
method `g`*(d: Rectangle): int = d.color.g.int
method `b`*(d: Rectangle): int = d.color.b.int

proc drawCircles(d: Rectangle) =
  let
    (x1, y1) = (d.crad, d.crad)
    (x2, y2) = (d.width - d.crad, d.crad)
    (x3, y3) = (d.width - d.crad, d.height - d.crad)
    (x4, y4) = (d.crad, d.height - d.crad)
  
  if d.topLeft:
    d.surfaceRenderer.drawFilledCircle(x1, y1, d.crad, color(255, 255, 255, 255))
  else:
    var r = rect(0.cint, 0.cint, d.crad.cint, d.crad.cint)
    d.surfaceRenderer.fillRect(r)
  
  if d.topRight:
    d.surfaceRenderer.drawFilledCircle(x2, y2, d.crad, color(255, 255, 255, 255))
  else:
    var r = rect(x2.cint, 0.cint, d.crad.cint, d.crad.cint)
    d.surfaceRenderer.fillRect(r)
  
  if d.bottomRight:
    d.surfaceRenderer.drawFilledCircle(x3, y3, d.crad, color(255, 255, 255, 255))
  else:
    var r = rect(x3.cint, y3.cint, d.crad.cint, d.crad.cint)
    d.surfaceRenderer.fillRect(r)
  
  if d.bottomLeft:
    d.surfaceRenderer.drawFilledCircle(x4, y4, d.crad, color(255, 255, 255, 255))
  else:
    var r = rect(0.cint, y4.cint, d.crad.cint, d.crad.cint)
    d.surfaceRenderer.fillRect(r)

proc drawEdgeRects(d: Rectangle) =
  var
    (x1, y1, w1, h1) = (d.crad, 0, (d.width - (2 * d.crad)), d.crad)
    (x2, y2, w2, h2) = (d.crad, d.height - d.crad, (d.width - (2 * d.crad)), d.crad)
    (x3, y3, w3, h3) = (0, d.crad, d.crad, (d.height - (2 * d.crad)))
    (x4, y4, w4, h4) = (d.width - d.crad, d.crad, d.crad, (d.height - (2 * d.crad)))
    r1 = rect(x1.cint, y1.cint, w1.cint, h1.cint)
    r2 = rect(x2.cint, y2.cint, w2.cint, h2.cint)
    r3 = rect(x3.cint, y3.cint, w3.cint, h3.cint)
    r4 = rect(x4.cint, y4.cint, w4.cint, h4.cint)
  
  d.surfaceRenderer.fillRect(r1)
  d.surfaceRenderer.fillRect(r2)
  d.surfaceRenderer.fillRect(r3)
  d.surfaceRenderer.fillRect(r4)

method render*(d: Rectangle) =
  if d.visible:
    d.lastRender = gfxTime()
    if d.doUpdate or d.texture == nil:
      if d.debugName.len > 0:
        echo "redrawing ", d.debugName
      d.surface = createRgbSurface(0.cint,
        d.width.cint, d.height.cint, 32.cint,
        0xff.uint32, 0xff00.uint32, 0xff0000.uint32, 0xff000000.uint32)
      defer:
        d.surface.freeSurface()
        d.surface = nil
      d.surfaceRenderer = d.surface.createSoftwareRenderer()
      defer:
        d.surfaceRenderer.destroy()
        d.surfaceRenderer = nil
      var midRect = rect(
        (0 + d.crad).cint,
        (0 + d.crad).cint,
        (d.destRect.w - (2 * d.crad)).cint,
        (d.destRect.h - (2 * d.crad)).cint)
      d.surfaceRenderer.setDrawColor(color(255, 255, 255, 255))
      if d.crad > 0:
        drawCircles(d)
        drawEdgeRects(d)
      d.surfaceRenderer.fillRect(midRect)
      if d.texture != nil:
        d.texture.destroy()
      d.texture = d.renderer.createTextureFromSurface(d.surface)
      d.doUpdate = false
    d.printDebug "rendering ", d.debugName, " (", d.width, ", ", d.height, ")"
    d.texture.setTextureAlphaMod(d.kalpha)
    discard d.texture.setTextureColorMod(d.color.r, d.color.g, d.color.b)
    var shadow = rect(
      d.destRect.x + d.xoff.cint,
      d.destRect.y + d.yoff.cint,
      d.width.cint, d.height.cint)
    d.renderer.copy(d.texture, nil, addr shadow)

method render*(d: Rectangle, masterSrc, masterDest: Rect) =
  if d.visible:
    d.lastRender = gfxTime()
    if d.doUpdate or d.texture == nil:
      if d.debugName.len > 0:
        echo "redrawing ", d.debugName
      d.surface = createRgbSurface(0.cint,
        d.width.cint, d.height.cint, 32.cint,
        0xff.uint32, 0xff00.uint32, 0xff0000.uint32, 0xff000000.uint32)
      defer:
        d.surface.freeSurface()
        d.surface = nil
      d.surfaceRenderer = d.surface.createSoftwareRenderer()
      defer:
        d.surfaceRenderer.destroy()
        d.surfaceRenderer = nil
      var midRect = rect(
        (0 + d.crad).cint,
        (0 + d.crad).cint,
        (d.destRect.w - (2 * d.crad)).cint,
        (d.destRect.h - (2 * d.crad)).cint)
      d.surfaceRenderer.setDrawColor(color(255, 255, 255, 255))
      if d.crad > 0:
        drawCircles(d)
        drawEdgeRects(d)
      d.surfaceRenderer.fillRect(midRect)
      if d.texture != nil:
        d.texture.destroy()
      d.texture = d.renderer.createTextureFromSurface(d.surface)
      d.doUpdate = false
    d.printDebug "rendering ", d.debugName, " (", d.width, ", ", d.height, ")"
    d.texture.setTextureAlphaMod(d.kalpha)
    discard d.texture.setTextureColorMod(d.color.r, d.color.g, d.color.b)
    let
      x = (d.destRect.x + d.xoff.cint) + masterDest.x
      y = (d.destRect.y + d.yoff.cint) + masterDest.y
      w = cint(d.width.float * (masterDest.w.float / masterSrc.w.float))
      h = cint(d.height.float * (masterDest.h.float / masterSrc.h.float))
    var shadow = rect(x, y, w, h)
    d.renderer.copy(d.texture, nil, addr shadow)


method cull*(d: Rectangle) =
  if d.lastRender + 1000'u32 < gfxTime():
    if d.texture != nil:
      d.texture.destroy()
    d.texture = nil

method destroy*(d: Rectangle) =
  if d.surfaceRenderer != nil:
    d.surfaceRenderer.destroy()
    d.surfaceRenderer = nil
  if d.texture != nil:
    d.texture.destroy()
    d.texture = nil
  if d.surface != nil:
    d.surface.freeSurface()
    d.surface = nil
