#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import sdl2, sdl2/ttf
import times, sequtils, unicode
import chroma/displayable
import chroma/displayables/simpletext
import chroma/private/[fontcache, dialogueaction]

type
  DialogueText* = ref object of Displayable
    simpleTexts: seq[SimpleText]
    activeAction: DialogueAction
    actionQueue: seq[DialogueAction]
    fontCache: FontCache
    tProgress: DateTime
    tWait, tTimer: uint32
    flags*: DialogueTextFlags
    defaults*: DialogueTextFlags
    wordacc: int
    ktimer: int
  
  DialogueTextFlags* = object
    bold*, italic*, underline*: bool
    font*: string
    color*: (int, int, int)
    size*: int
  
  DialogueTextStat* = object
    ## A DialogueText's status flags. returned by execute()
    isExecuting*: bool
    waitForClick*: bool
    waitUntilTime*: uint32
  
  DialogueTextState* = object
    ## has nothing to do with DialogueTextStat. A serializable
    ## object representing the state of a DialogueText.
    stStates*: seq[SimpleTextState]
    flags*, defaults*: DialogueTextFlags
    visible*, enabled*: bool
    ktimer*: int

proc `bold=`*(dt: DialogueText, b: bool) =
  dt.defaults.bold = b

proc `italic=`*(dt: DialogueText, b: bool) =
  dt.defaults.italic = b

proc `underline=`*(dt: DialogueText, b: bool) =
  dt.defaults.underline = b

proc `fill=`*(dt: DialogueText, c: (int, int, int)) =
  dt.defaults.color = c

proc `font=`*(dt: DialogueText, f: string) =
  dt.defaults.font = f

proc `size=`*(dt: DialogueText, s: int) =
  dt.defaults.size = s

proc `alpha=`*(dt: DialogueText, i: uint8) =
  dt.kalpha = i
  for st in dt.simpleTexts:
    st.alpha = i

proc queue*(dt: DialogueText, da: DialogueAction) =
  if da.kind == dakText:
    dt.wordacc += da.tText.len
  dt.actionQueue.add da


proc isActive*(dt: DialogueText): bool = dt.activeAction != nil

proc setTimer*(dt: DialogueText) =
  dt.tTimer = gfxTime() + (dt.wordacc.uint32 * dt.ktimer.uint32)

proc getTimer*(dt: DialogueText): uint32 = dt.tTimer

proc flush*(dt: DialogueText) =
  for st in dt.simpleTexts:
    st.destroy()
  dt.simpleTexts = @[]
  dt.actionQueue = @[]
  dt.flags = dt.defaults
  dt.activeAction = nil
  dt.wordacc = 0
  echo "resetting timer"

proc newActiveAction(dt: DialogueText) =
  let act = dt.activeAction
  case act.kind
  of dakText:
    let t = newSimpleText(dt.renderer, dt.fontCache)
    t.font = dt.flags.font
    t.bold = dt.flags.bold
    t.italic = dt.flags.italic
    t.underline = dt.flags.underline
    t.fill = dt.flags.color
    t.size = dt.flags.size
    t.text = if act.tText.len > 0: $(act.tText[0..0]) else: ""
    t.width = dt.width
    t.alpha = dt.alpha
    (t.xpos, t.ypos) =
      if dt.simpleTexts.len > 0:
        dt.simpleTexts[^1].appendpos
      else:
        (0, 0)
    t.height = dt.height
    t.indent =
      if dt.simpleTexts.len > 0:
        dt.simpleTexts[^1].endx
      else:
        0
    dt.simpleTexts.add t
  of dakBold:
    dt.flags.bold = act.bBold
    dt.activeAction = nil
  of dakItalic:
    dt.flags.italic = act.iItalic
    dt.activeAction = nil
  of dakUnderline:
    dt.flags.underline = act.uUnderline
    dt.activeAction = nil
  of dakSize:
    dt.flags.size = act.sSize
    dt.activeAction = nil
  of dakColor:
    dt.flags.color = act.cRgb
    dt.activeAction = nil
  of dakFont:
    dt.flags.font = act.fFont
    dt.activeAction = nil
  of dakWait:
    dt.tProgress = now()
  of dakPause:
    dt.tWait = (cpuTime() * 1000).uint32
  else:
    discard

# waitUntilTime specifies the time that the DialogueText started
# waiting for a click. it is only a meaningful value when waitForClick is true.
proc execute*(dt: DialogueText, fast = false, endNow = false): DialogueTextStat =
  if dt.activeAction == nil:
    if dt.actionQueue.len > 0:
      dt.activeAction = dt.actionQueue[0]
      dt.actionQueue.delete 0
      dt.newActiveAction()
  else:
    let act = dt.activeAction
    case act.kind:
    of dakText:
      let st = dt.simpleTexts[^1]
      let interval = if fast: 4 else: 1
      let stRunes = st.text.toRunes()
      let endl =
        if endNow or stRunes.high + interval >= act.tText.len:
          act.tText.high
        else:
          stRunes.high + interval
      if stRunes.len == act.tText.len:
        dt.activeAction = nil
      else:
        st.text = $(act.tText[0..endl])
    of dakWait:
      let ntq = now()
      let nMillis = (act.wTime * 1000).int
      let tdiff = (ntq - dt.tProgress).inMilliseconds().int
      if tdiff >= nMillis or fast or endNow:
        dt.activeAction = nil
    else:
      discard
  result = DialogueTextStat(
    isExecuting: dt.activeAction != nil,
    waitForClick: dt.activeAction != nil and dt.activeAction.kind == dakPause,
    waitUntilTime: dt.tWait)


proc cancelActiveAction*(dt: DialogueText) =
  dt.activeAction = nil

proc shadow(p, c: Displayable): tuple[xpos, ypos, xoffset, yoffset, width, height: int, alpha: uint8] =
  result.xpos = c.xpos
  result.ypos = c.ypos
  result.xoffset = c.xoffset
  result.yoffset = c.yoffset
  result.width = c.width
  result.height = c.height
  result.alpha = c.alpha
  c.xpos = c.xpos + p.xpos
  c.ypos = c.ypos + p.ypos
  c.xoffset = c.xoffset + p.xoffset
  c.yoffset = c.yoffset + p.yoffset
  c.alpha = (c.alpha.uint32 * p.alpha.uint32 div 255).uint8

proc unshadow(c: Displayable, prop: tuple[xpos, ypos, xoffset, yoffset, width, height: int, alpha: uint8]) =
  c.xpos = prop.xpos
  c.ypos = prop.ypos
  c.xoffset = prop.xoffset
  c.yoffset = prop.yoffset
  c.width = prop.width
  c.height = prop.height
  c.alpha = prop.alpha

method render*(dt: DialogueText) =
  for st in dt.simpleTexts:
    let s = shadow(dt, st)
    st.render()
    unshadow(st, s)


proc newDialogueText*(renderer: RendererPtr, fontCache: FontCache, defaults: DialogueTextFlags): DialogueText =
  new result
  result.renderer = renderer
  result.fontCache = fontCache
  result.simpleTexts = @[]
  result.actionQueue = @[]
  result.activeAction = nil
  result.defaults = defaults
  result.flags = defaults
  result.enabled = true
  result.visible = true
  result.alpha = 255
  result.ktimer = 55

proc getState*(dt: DialogueText): DialogueTextState = 
  result.stStates = dt.simpleTexts.mapIt(it.getState())
  result.flags = dt.flags
  result.defaults = dt.defaults
  result.enabled = dt.enabled
  result.visible = dt.visible
  result.ktimer = dt.ktimer

proc loadState*(dt: DialogueText, state: DialogueTextState) =
  dt.cancelActiveAction()
  dt.flush()
  for s in state.stStates:
    var st = newSimpleText(dt.renderer, dt.fontCache)
    st.loadState(s)
    dt.simpleTexts.add st
  dt.flags = state.flags
  dt.defaults = state.defaults
  dt.enabled = state.enabled
  dt.visible = state.visible
  dt.ktimer = state.ktimer
