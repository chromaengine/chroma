#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import sdl2, sdl2/[ttf, image]
import chroma/jankcrypt, chroma/displayable

type
  Hbox* = ref object of ContainerDisplayable
    width, height: int
    renderedWidth: int
    renderedHeight: int
    hspace: int

method `xpos=`*(d: Hbox, x: int) = d.destRect.x = x.cint

method `ypos=`*(d: Hbox, y: int) = d.destRect.y = y.cint


proc `spacing=`*(d: Hbox, s: int) = d.hspace = s
proc `spacing`*(d: Hbox): int = d.hspace

method `renderedWidth`*(d: Hbox): int = d.renderedWidth

proc newHbox*(win: WindowPtr): Hbox =
  new result
  result.parent = nil
  result.doUpdate = true
  result.children = @[]
  result.srcRect = rect(0.cint, 0.cint, 3840.cint, 2160.cint)
  result.destRect = rect(0.cint, 0.cint, 3840.cint, 2160.cint)
  result.width = 0
  result.height = 0
  result.xoff = 0.cint
  result.yoff = 0.cint
  result.enabled = true
  result.visible = true
  result.onEvent = onEventContainerDefault

proc newHbox*(width: int = -1, height: int = -1): Hbox =
  new result
  result.parent = nil
  result.doUpdate = true
  result.children = @[]
  result.srcRect = rect(0.cint, 0.cint,
    if width >= 0: width.cint else: 3840.cint,
    if height >= 0: height.cint else: 2160.cint
  )
  result.destRect = result.srcRect
  result.width = width
  result.height = height
  result.xoff = 0.cint
  result.yoff = 0.cint
  result.enabled = true
  result.visible = true
  result.onEvent = onEventContainerDefault

method addChild*(d: Hbox, c: Displayable) =
  d.children.add c
  c.parent = d

method render*(d: Hbox) =
  var x = d.xpos
  d.renderedHeight = 0
  for c in d.children:
    if c.height > d.height and d.height > 0:
      c.height = d.height
    c.xpos = x
    c.ypos = d.ypos
    if c.visible:
      c.render()
    x += c.width
    x += d.hspace
    if d.renderedHeight < c.height:
      d.renderedHeight = c.height
  d.renderedWidth = x

# method render(d: Hbox) =
#   var x = 0
#   d.renderedHeight = 0
#   for c in d.children:
#     c.xpos = x
#     c.render(d.surface)
#     x += c.width
#     if d.renderedHeight < c.height:
#       d.renderedHeight = c.height
#   d.renderedWidth = x
#   var shadow = d.destRect
#   d.surface.blitSurface(addr d.srcRect, surface, addr shadow)

# method render*(d: Displayable, renderer: var RendererPtr) {.base.} =
#   if d.doUpdate or d.texture == nil:
#     d.doUpdate = false
#     if d.texture != nil:
#       d.texture.destroy()
#     discard d.surface.setSurfaceAlphaMod(d.alpha)
#     d.texture = renderer.createTextureFromSurface(d.surface)
#   renderer.copyEx(d.texture, d.srcRect, d.destRect,
#     angle = 0.0,
#     center = nil,
#     flip = SDL_FLIP_NONE)
