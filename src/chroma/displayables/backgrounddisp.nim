#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import sdl2
import chroma/displayable
import chroma/private/imagecache
import chroma/displayables/simpleimage

type
  Background* = ref object of Displayable
    img: SimpleImage
    imageCache: ImageCache
  
  BackgroundState* = object
    fname*: string
    alpha*: uint8
    visible*, enabled*, childVisible*, childEnabled*: bool

proc newBackground*(renderer: RendererPtr, imageCache: ImageCache): Background =
  new result
  result.renderer = renderer
  result.destRect = rect(0.cint, 0.cint, 3840.cint, 2160.cint)
  result.visible = true
  result.enabled = true
  result.alpha = 255
  result.imageCache = imageCache
  result.img = newSimpleImage(renderer, imageCache)

method `alpha=`*(bg: Background, a: uint8) =
  bg.kalpha = a
  if bg.img != nil:
    bg.img.alpha = a

proc update*(bg: Background, fpath: string): bool =
  if bg.img == nil:
    bg.img = newSimpleImage(bg.renderer, bg.imageCache, fpath)
    result = true
  elif bg.img.filename != fpath:
    bg.img.path = fpath
    result = true
  bg.img.visible = true

proc flush*(bg: Background) =
  # secretly, don't actually delete the image. keep it in case it
  # shows up again later.
  if bg.img != nil:
    bg.img.visible = false

proc `fname`*(bg: Background): string =
  if bg.img != nil:
    bg.img.filename
  else:
    ""

method render*(bg: Background) =
  bg.printDebug "rendering ", bg.debugName
  if bg.visible and bg.img != nil and bg.img.visible:
    bg.img.alpha = bg.kalpha
    bg.img.render()

method destroy*(bg: Background) =
  if bg.img != nil:
    bg.img.destroy()
    bg.img = nil

proc getState*(bg: Background): BackgroundState =
  result = BackgroundState(
    fname: bg.fname,
    alpha: bg.kalpha,
    visible: bg.visible,
    enabled: bg.enabled,
    childVisible: if bg.img != nil: bg.img.visible else: false,
    childEnabled: if bg.img != nil: bg.img.enabled else: false)

proc defaultBackgroundState*: BackgroundState =
  result = BackgroundState(
    fname: "",
    alpha: 255,
    visible: true,
    enabled: true,
    childVisible: false,
    childEnabled: false)

proc loadState*(bg: Background, state: BackgroundState) =
  if state.fname.len > 0:
    discard bg.update(state.fname)
  else:
    bg.flush()
  bg.alpha = state.alpha
  bg.visible = state.visible
  bg.enabled = state.enabled
  if bg.img != nil:
    bg.img.visible = state.childVisible
    bg.img.enabled = state.childEnabled
