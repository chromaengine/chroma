#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import sdl2, sdl2/[ttf, image]
import chroma/jankcrypt, chroma/displayable
import chroma/private/imagecache

type
  SimpleImage* = ref object of Displayable
    filename*: string
    preserveAspectRatio: bool
    imageCache: ImageCache
    cachedImage: CachedImage
    hyper: bool
    hflip*, vflip*: bool


method `width=`*(d: SimpleImage, width: int) =
  d.destRect.w = width.cint
  if d.preserveAspectRatio and d.srcRect.w > 0:
    d.destRect.h = (width.toFloat * (d.srcRect.h.toFloat / d.srcRect.w.toFloat)).toInt.cint


method `height=`*(d: SimpleImage, height: int) =
  d.destRect.h = height.cint
  if d.preserveAspectRatio and d.srcRect.h > 0:
    d.destRect.w = (height.toFloat * (d.srcRect.w.toFloat / d.srcRect.h.toFloat)).toInt.cint


method `alpha=`*(d: SimpleImage, alpha: uint8) =
  d.kalpha = alpha

method `xpos=`*(d: SimpleImage, x: int) =
  d.destRect.x = x.cint


method `ypos=`*(d: SimpleImage, y: int) =
  d.destRect.y = y.cint


method `width`*(d: SimpleImage): int = d.destRect.w.int
method `height`*(d: SimpleImage): int = d.destRect.h.int
method `xpos`*(d: SimpleImage): int = d.destRect.x.int
method `ypos`*(d: SimpleImage): int = d.destRect.y.int

method `path=`*(d: SimpleImage, fname: string) {.base.}
proc newSimpleImage*(
    # every simpleimage is bound to a particular image cache.
    # That way, when a simpleimage is loaded from the same file as a previous one,
    # the same surface and texture can be re-used.
    renderer: RendererPtr,
    imageCache: ImageCache,
    fname: string,
    preserveAspectRatio: bool = true,
    hyper: bool = false
  ): SimpleImage =
  new result
  result.renderer = renderer
  result.imageCache = imageCache
  result.kalpha = 255
  result.onEvent = onEventDoNothing
  result.preserveAspectRatio = preserveAspectRatio
  result.xoff = 0.cint
  result.yoff = 0.cint
  result.enabled = true
  result.visible = true
  result.path = fname
  result.hyper = hyper

proc newSimpleImage*(
    # every simpleimage is bound to a particular image cache.
    # That way, when a simpleimage is loaded from the same file as a previous one,
    # the same surface and texture can be re-used.
    renderer: RendererPtr,
    imageCache: ImageCache,
    preserveAspectRatio: bool = true,
    hyper: bool = false
  ): SimpleImage =
  new result
  result.renderer = renderer
  result.imageCache = imageCache
  result.kalpha = 255
  result.onEvent = onEventDoNothing
  result.preserveAspectRatio = preserveAspectRatio
  result.xoff = 0.cint
  result.yoff = 0.cint
  result.enabled = true
  result.visible = true
  result.hyper = hyper


method `path=`*(d: SimpleImage, fname: string) {.base.} =
  if d.filename != fname:
    if d.cachedImage != nil:
      d.imageCache.release(d.cachedImage)
      d.cachedImage = nil
    d.cachedImage = d.imageCache.load(fname)
    if d.cachedImage == nil:
      raise Exception.newException("file does not exist: " & fname)
    d.srcRect = rect(0.cint, 0.cint, d.cachedImage.w.cint, d.cachedImage.h.cint)
    d.destRect = rect(0.cint, 0.cint, d.cachedImage.w.cint, d.cachedImage.h.cint)
    echo "loading image ", fname, " ", d.cachedImage.w.cint, " ", d.cachedImage.h.cint
    d.filename = fname
    d.doUpdate = true

method `path`*(d: SimpleImage): string {.base.} = d.filename

method cull*(d: SimpleImage) =
  if d.cachedImage != nil:
    d.imageCache.release(d.cachedImage)

proc `preserveAspectRatio=`*(d: SimpleImage, b: bool) =
  d.preserveAspectRatio = b

proc getFlip(d: SimpleImage): RendererFlip =
  result = SDL_FLIP_NONE
  if d.hflip:
    result = result or SDL_FLIP_HORIZONTAL
  if d.vflip:
    result = result or SDL_FLIP_VERTICAL

method render*(d: SimpleImage) =
  d.printDebug "starting render for ", d.debugName
  if d.doUpdate and d.cachedImage != nil:
    d.printDebug "rebuilding texture for image"
    if d.texture != nil:
      d.texture.destroy()
    if not d.cachedImage.valid:
      d.cachedImage = d.imageCache.load(d.filename)
    d.texture = d.cachedImage.texture(d.renderer)
    d.doUpdate = false
  if d.visible and d.texture != nil:
    d.printDebug "rendering ", d.debugName
    var shadow = rect(
      d.destRect.x.cint + d.xoff.cint,
      d.destRect.y.cint + d.yoff.cint,
      d.destRect.w.cint,
      d.destRect.h.cint)
    d.texture.setTextureAlphaMod(d.kalpha)
    assert d.texture != nil
    d.renderer.copyEx(d.texture, nil, addr shadow, angle = 0.0, center = nil, flip = d.getFlip())
  d.printDebug "render finished for ", d.debugName

method destroy*(d: SimpleImage) =
  if d.surface != nil and not d.doNotDestroySurface:
    d.surface.destroy()
    d.surface = nil
  if d.texture != nil and not d.doNotDestroyTexture:
    d.texture.destroy()
    d.texture = nil
  if d.cachedImage != nil:
    d.imageCache.release(d.cachedImage)
