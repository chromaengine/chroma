#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import sdl2, sdl2/[ttf, image]
import chroma, chroma/jankcrypt, chroma/displayable, chroma/displayables/simpleimage, chroma/private/imagecache
import tables

type
  Characterd* = ref object of Displayable
    currentImgs*: OrderedTable[string, SimpleImage]
    baseWidth*, baseHeight*: cint
    name*: string
    childRenderer*: RendererPtr
    imageCache*: ImageCache
    hflip*, vflip*: bool

method `width=`*(d: Characterd, width: int) =
  d.destRect.w = width.cint
  d.destRect.h = (d.baseHeight.toFloat * (width.toFloat / d.baseWidth.toFloat)).toInt.cint


method `height=`*(d: Characterd, height: int) =
  d.destRect.h = height.cint
  d.destRect.w = (d.baseWidth.toFloat * (height.toFloat / d.baseHeight.toFloat)).toInt.cint

method render*(d: Characterd) =
  d.printDebug "rendering characterd ", d.debugName
  if d.doUpdate or d.texture == nil:
    d.printDebug "re-rendering ", d.debugName
    assert d.childRenderer != nil
    d.childRenderer.clear()
    for k, v in d.currentImgs.pairs:
      v.hflip = d.hflip
      v.vflip = d.vflip
      v.render()
    if d.texture != nil:
      d.texture.destroy()
    d.texture = d.renderer.createTextureFromSurface(d.surface)
    assert d.texture != nil
    d.doUpdate = false
  d.texture.setTextureAlphaMod(d.kalpha)
  var shadow = rect(
    d.destRect.x + d.xoff,
    d.destRect.y + d.yoff,
    d.destRect.w,
    d.destRect.h)
  d.renderer.copyEx(d.texture, nil, addr shadow, 0.0, nil)


method `[]=`*(d: Characterd, layer: string, fpath: string) =
  if layer notin d.currentImgs:
    d.printDebug "load ", d.currentImgs[layer].filename, " ", fpath
    d.currentImgs[layer] = newSimpleImage(d.childRenderer, d.imageCache)
    d.currentImgs[layer].path = fpath
    d.doUpdate = true
  if d.currentImgs[layer].filename != fpath:
    d.printDebug "reload ", d.currentImgs[layer].filename, " ", fpath
    d.currentImgs[layer].path = fpath
    d.doUpdate = true
    
    
#     let
#       img = d.currentImgs[layer]
#       xpos = img.xpos
#       ypos = img.ypos
#       width = img.width
#       newimg = newSimpleImage(d.childRenderer, d.imageCache, fpath)
#     img.destroy()
#     newimg.xpos = xpos
#     newimg.ypos = ypos
#     newimg.width = width
#     d.currentImgs[layer] = newimg
#     d.doUpdate = true

method destroy*(d: Characterd) =
  if d.parent != nil:
    d.parent.detachChild(d)
  if d.texture != nil:
    d.texture.destroy()
  if d.surface != nil:
    d.surface.freeSurface()
  d.imageCache.destroy()
  d.childRenderer.destroy()
