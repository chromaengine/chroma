#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import chroma/displayable
import chroma/displayables/animation
import chroma/private/audiomanager
import math, tables

type
  AudioAnimation* = ref object of Animation
  
  AudioFadeAnimation* = ref object of AudioAnimation
    t: int
    to, fro: range[0..100]
    vec: float

method run*(a: AudioFadeAnimation) =
  if not a.done:
    a.nruns += 1
    let level = a.fro + round(a.nruns.float * a.vec).cint
    WavData(a.displayable).volume = level
    if a.nruns == a.t:
      WavData(a.displayable).volume = a.to
      a.done = true

proc newAudioFadeAnimation*(d: WavData, t: int, to: range[0..100]): AudioFadeAnimation =
  new result
  result.tCreated = gfxTime()
  result.displayable = d
  result.blocking = false
  result.t = t
  result.to = to
  result.nruns = 0
  result.fro = d.volume
  result.vec = (to - d.volume).float / t.float
  result.sems = AnimationSemaphore(alpha: true)
