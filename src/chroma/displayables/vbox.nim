#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import sdl2, sdl2/[ttf, image]
import chroma/jankcrypt, chroma/displayable

type
  Vbox* = ref object of ContainerDisplayable
    renderedWidth: int
    renderedHeight: int
    vspace: int
    center: bool

method `vcenter=`*(d: Vbox, b: bool) {.base.} =
  ## turn this on to make the vbox vertically center its contents.
  d.center = b

method `spacing=`*(d: Vbox, s: int) {.base.} = d.vspace = s
method `spacing`*(d: Vbox): int {.base.} = d.vspace

method `renderedHeight`*(d: Vbox): int = d.renderedHeight

# method `height`*(d: Vbox): int = d.renderedHeight

proc newVbox*(win: WindowPtr): Vbox =
  new result
  result.parent = nil
  result.doUpdate = true
  result.children = @[]
  result.destRect = rect(0.cint, 0.cint, 0.cint, 0.cint)
  result.width = 0
  result.height = 0
  result.xoff = 0.cint
  result.yoff = 0.cint
  result.visible = true
  result.enabled = true
  result.onEvent = onEventContainerDefault

proc newVbox*(width: int = -1, height: int = -1): Vbox =
  new result
  result.parent = nil
  result.doUpdate = true
  result.children = @[]
  result.destRect = rect(0.cint, 0.cint, 0.cint, 0.cint)
  result.width = width
  result.height = height
  result.xoff = 0.cint
  result.yoff = 0.cint
  result.visible = true
  result.enabled = true
  result.onEvent = onEventContainerDefault

method addChild*(d: Vbox, c: Displayable) =
  d.children.add c
  c.parent = d

method render*(d: Vbox) =
  var y = d.ypos
  d.renderedHeight = 0
  for c in d.children:
    d.renderedHeight += c.height
    d.renderedHeight += d.spacing
  
  if d.center and d.renderedHeight < d.height:
    y += (d.height - d.renderedHeight) div 2
  
  d.renderedWidth = 0
  for c in d.children:
    if c.visible:
      if c.width > d.width and d.width > 0:
        c.width = d.width
      c.ypos = y
      c.xpos = d.xpos
      c.render()
      y += c.height 
      y += d.vspace
      if d.renderedWidth < c.width:
        d.renderedWidth = c.width
