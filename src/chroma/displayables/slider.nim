#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import sdl2
import chroma/displayable
import chroma/displayables/rectangle

type
  VSlider* = ref object of Displayable
    span: float
    minspan: int
    small*, large*: Rectangle
    active: bool
    onPositionChanged*: proc(slider: VSlider, value: range[0.0..1.0])
    onPositionChosen*: proc(slider: VSlider, value: range[0.0..1.0])
  
  HSlider* = ref object of Displayable
    span: float
    minspan: int
    small*, large*: Rectangle
    active: bool
    onPositionChanged*: proc(slider: HSlider, value: range[0.0..1.0])
    onPositionChosen*: proc(slider: HSlider, value: range[0.0..1.0])
    

proc `position`*(d: VSlider): range[0.0..1.0] =
  let rnge = d.height - d.small.height
  let rel = d.small.ypos - d.ypos
  result = min(1.0, max(0.0, 1.0 - (rel.float / rnge.float)))

proc `position=`*(d: VSlider, pos: float) =
  let rnge = d.height - d.small.height
  d.small.ypos = d.ypos + int((1.0 - minmax(0.0, pos, 1.0)) * rnge.float)

proc `position`*(d: HSlider): range[0.0..1.0] =
  let rnge = d.width - d.small.width
  let rel = d.small.xpos - d.xpos
  result = min(1.0, max(0.0, rel.float / rnge.float))

proc `position=`*(d: HSlider, pos: float) =
  let rnge = d.width - d.small.width
  d.small.xpos = d.xpos + int(minmax(0.0, pos, 1.0) * rnge.float)


method event*(d: VSlider, event: ChromaEvent): bool =
  case event.kind:
  of eventMouseDown:
    if event in d:
      result = true
      if event in d.small:
        d.active = true
  of eventMouseUp:
    result = d.active
    if d.active:
      d.onPositionChosen(d, d.position)
    d.active = false
  of eventMouseMotion:
    if d.active:
      result = true
      let newpos = d.small.ypos + event.motion.y
      if event.motion.y != 0:
        d.small.ypos = minmax(d.large.ypos, newpos,
          d.large.ypos + d.large.height - d.small.height)
        d.doUpdate = true
        d.onPositionChanged(d, d.position)
  else:
    discard

method event*(d: HSlider, event: ChromaEvent): bool =
  case event.kind:
  of eventMouseDown:
    if event in d:
      result = true
      if event in d.small:
        d.active = true
  of eventMouseUp:
    result = d.active
    if d.active:
      d.onPositionChosen(d, d.position)
    d.active = false
  of eventMouseMotion:
    if d.active:
      result = true
      let newpos = d.small.xpos + event.motion.x
      if event.motion.x != 0:
        d.small.xpos = minmax(d.large.xpos, newpos,
          d.large.xpos + d.large.width - d.small.width)
        d.doUpdate = true
        d.onPositionChanged(d, d.position)
  else:
    discard

method `width=`*(d: VSlider, x: int) =
  d.small.width = x
  d.destRect.w = max(d.large.width, d.small.width).cint
  if d.large.width > d.small.width:
    d.small.xoff = ((d.large.width - d.small.width) div 2).cint
    d.large.xoff = 0.cint
  else:
    d.large.xoff = ((d.small.width - d.large.width) div 2).cint
    d.small.xoff = 0.cint

proc `barWidth=`*(d: VSlider, x: int) =
  d.large.width = x
  d.destRect.w = max(d.large.width, d.small.width).cint
  if d.large.width > d.small.width:
    d.small.xoff = ((d.large.width - d.small.width) div 2).cint
    d.large.xoff = 0.cint
  else:
    d.large.xoff = ((d.small.width - d.large.width) div 2).cint
    d.small.xoff = 0.cint

proc `radius=`*(d: VSlider, x: int) =
  d.small.radius = x

proc `barRadius=`*(d: VSlider, x: int) =
  d.large.radius = x

method `alpha=`*(d: VSlider, x: uint8) =
  d.kalpha = x

method `barAlpha=`(d: VSlider, x: uint8) =
  d.large.alpha = x

proc `fill=`*(d: VSlider, c: (int, int, int)) =
  d.small.fill = c

proc `barFill=`*(d: VSlider, c: (int, int, int)) =
  d.large.fill = c

method `height=`*(d: VSlider, x: int) =
  d.destRect.h = x.cint
  d.large.height = x
  d.small.height = int(d.span * x.float)

method `xpos=`*(d: VSlider, x: int) =
  d.destRect.x = x.cint
  d.small.xpos = x
  d.large.xpos = x

method `ypos=`*(d: VSlider, x: int) =
  d.destRect.y = x.cint
  let diff = x - d.large.ypos
  d.large.ypos = d.large.ypos + diff
  d.small.ypos = d.small.ypos + diff



method `height=`*(d: HSlider, x: int) =
  d.small.height = x
  d.destRect.h = max(d.large.height, d.small.height).cint
  if d.large.height > d.small.height:
    d.small.yoff = ((d.large.height - d.small.height) div 2).cint
    d.large.yoff = 0.cint
  else:
    d.large.yoff = ((d.small.height - d.large.height) div 2).cint
    d.small.yoff = 0.cint

proc `barHeight=`*(d: HSlider, x: int) =
  d.large.height = x
  d.destRect.h = max(d.large.height, d.small.height).cint
  if d.large.height > d.small.height:
    d.small.yoff = ((d.large.height - d.small.height) div 2).cint
    d.large.yoff = 0.cint
  else:
    d.large.yoff = ((d.small.height - d.large.height) div 2).cint
    d.small.yoff = 0.cint

proc `radius=`*(d: HSlider, x: int) =
  d.small.radius = x

proc `barRadius=`*(d: HSlider, x: int) =
  d.large.radius = x

method `alpha=`*(d: HSlider, x: uint8) =
  d.kalpha = x

method `barAlpha=`(d: HSlider, x: uint8) =
  d.large.alpha = x

proc `fill=`*(d: HSlider, c: (int, int, int)) =
  d.small.fill = c

proc `barFill=`*(d: HSlider, c: (int, int, int)) =
  d.large.fill = c

method `width=`*(d: HSlider, x: int) =
  d.destRect.w = x.cint
  d.large.width = x
  d.small.width = int(d.span * x.float)

method `ypos=`*(d: HSlider, x: int) =
  d.destRect.y = x.cint
  d.small.ypos = x
  d.large.ypos = x

method `xpos=`*(d: HSlider, x: int) =
  d.destRect.x = x.cint
  let diff = x - d.large.xpos
  d.large.xpos = d.large.xpos + diff
  d.small.xpos = d.small.xpos + diff


method `xoffset=`*(d: VSlider | HSlider, x: int) =
  d.xoff = x.cint

method `yoffset=`*(d: VSlider | HSlider, x: int) =
  d.yoff = x.cint

proc `span=`*(d: VSlider, x: range[0.0..1.0]) =
  d.span = x
  d.small.height = minmax(d.minspan, int(d.large.height.float * x), d.large.height)
  let fix = d.small.height + d.small.ypos - (d.large.height + d.large.ypos)
  if fix > 0:
    d.small.ypos = d.small.ypos - fix

proc `span=`*(d: HSlider, x: range[0.0..1.0]) =
  d.span = x
  d.small.width = minmax(d.minspan, int(d.large.width.float * x), d.large.width)
  let fix = d.small.width + d.small.xpos - (d.large.width + d.large.xpos)
  if fix > 0:
    d.small.xpos = d.small.xpos - fix



method render*(d: VSlider) =
  if d.visible:
    d.large.xpos = d.large.xpos + d.xoffset
    d.large.ypos = d.large.ypos + d.yoffset
    d.small.xpos = d.small.xpos + d.xoffset
    d.small.ypos = d.small.ypos + d.yoffset
    defer:
      d.large.xpos = d.large.xpos - d.xoffset
      d.large.ypos = d.large.ypos - d.yoffset
      d.small.xpos = d.small.xpos - d.xoffset
      d.small.ypos = d.small.ypos - d.yoffset
    let
      la = d.large.alpha
      sa = d.small.alpha
    d.large.alpha = (d.large.alpha.uint32 * d.kalpha.uint32 div 255).uint8
    d.small.alpha = (d.small.alpha.uint32 * d.kalpha.uint32 div 255).uint8
    d.large.render()
    d.small.render()
    d.large.alpha = la
    d.small.alpha = sa

method render*(d: HSlider) =
  if d.visible:
    d.large.xpos = d.large.xpos + d.xoffset
    d.large.ypos = d.large.ypos + d.yoffset
    d.small.xpos = d.small.xpos + d.xoffset
    d.small.ypos = d.small.ypos + d.yoffset
    defer:
      d.large.xpos = d.large.xpos - d.xoffset
      d.large.ypos = d.large.ypos - d.yoffset
      d.small.xpos = d.small.xpos - d.xoffset
      d.small.ypos = d.small.ypos - d.yoffset
    let
      la = d.large.alpha
      sa = d.small.alpha
    d.large.alpha = (d.large.alpha.uint32 * d.kalpha.uint32 div 255).uint8
    d.small.alpha = (d.small.alpha.uint32 * d.kalpha.uint32 div 255).uint8
    d.large.render()
    d.small.render()
    d.large.alpha = la
    d.small.alpha = sa


method destroy*(d: VSlider) =
  d.small.destroy()
  d.large.destroy()

method destroy*(d: HSlider) =
  d.small.destroy()
  d.large.destroy()

proc newVSlider*(renderer: RendererPtr): VSlider =
  new result
  result.renderer = renderer
  result.small = newRectangle(renderer)
  result.small.fill = (0, 0, 180)
  result.large = newRectangle(renderer)
  result.visible = true
  result.enabled = true
  result.doUpdate = true
  result.kalpha = 255
  result.minspan = 50
  result.width = 60
  result.barWidth = 30
  result.height = 500
  result.radius = 30
  result.barRadius = 15
  result.onPositionChanged = proc(slider: VSlider, value: range[0.0..1.0]) =
    discard
  result.onPositionChosen = proc(slider: VSlider, value: range[0.0..1.0]) =
    discard

proc newHSlider*(renderer: RendererPtr): HSlider =
  new result
  result.renderer = renderer
  result.small = newRectangle(renderer)
  result.small.fill = (0, 0, 180)
  result.large = newRectangle(renderer)
  result.visible = true
  result.enabled = true
  result.doUpdate = true
  result.kalpha = 255
  result.minspan = 50
  result.height = 60
  result.barHeight = 30
  result.width = 500
  result.radius = 30
  result.barRadius = 15
  result.onPositionChanged = proc(slider: HSlider, value: range[0.0..1.0]) =
    discard
  result.onPositionChosen = proc(slider: HSlider, value: range[0.0..1.0]) =
    discard
