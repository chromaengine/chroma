#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import sdl2
import chroma/displayable
import chroma/displayables/vbox

type
  VScroll* = ref object of ContainerDisplayable
    vspace: int
    pos: range[0.0..1.0]
    renderedHeight, renderedWidth: int
    onPositionChangedByUser*: proc(scroll: VScroll, value: range[0.0..1.0])

proc newVScroll*(renderer: RendererPtr): VScroll =
  new result
  result.renderer = renderer
  result.parent = nil
  result.doUpdate = true
  result.children = @[]
  result.destRect = rect(0.cint, 0.cint, 0.cint, 0.cint)
  result.width = 0
  result.height = 0
  result.pos = 0.0
  result.xoff = 0.cint
  result.yoff = 0.cint
  result.vspace = 0.cint
  result.visible = true
  result.enabled = true
  result.renderedHeight = 0
  result.onEvent = onEventContainerDefault
  result.onPositionChangedByUser = proc(scroll: VScroll, value: range[0.0..1.0]) = discard

method `spacing=`*(d: VScroll, s: int) {.base.} = d.vspace = s
method `spacing`*(d: VScroll): int {.base.} = d.vspace

method addChild*(d: VScroll, c: Displayable) =
  d.children.add c
  c.parent = d

proc `position=`*(d: VScroll, pos: float) =
  d.pos = minmax(0.0, pos, 1.0)

proc `position`*(d: VScroll): range[0.0..1.0] = d.pos

proc `span`*(d: VScroll): range[0.0..1.0] =
  if d.renderedHeight == 0:
    1.0
  else:
    minmax(0.0, d.height.float / d.renderedHeight.float, 1.0)

method event*(d: VScroll, event: ChromaEvent): bool =
  result = false
  if d.debugName.len > 0:
    echo "container event (", d.debugName, ", ", $d.enabled, "):" , event
  if d.enabled and (d.tenabled <= event.timestamp):
    case event.kind:
    of eventMouseWheel:
      if event in d:
        let posadj = (50.0 * (0.0 - event.motion.y.float)) / (d.renderedHeight - d.height).float
        d.position = d.position + posadj
        d.doUpdate = true
        d.onPositionChangedByUser(d, d.position)
        result = true
    else:
      if event in d:
        for child in d.children:
          result = result or child.event(event)
          if result:
            break
    if d.onEvent != nil and not result:
      result = result or d.onEvent(d, event)

method render*(d: VScroll) =
  if d.visible:
    d.printDebug "rendering ", d.debugName
    d.renderedHeight = 0
    for c in d.children:
      try:
        d.renderedHeight += Vbox(c).renderedHeight
      except:
        d.renderedHeight += c.height
      d.renderedHeight += d.vspace
    var y = d.ypos - max(0.0, d.pos * (d.renderedHeight - d.height).float).int
    var
      clipEnabled = isClipEnabled(d.renderer)
      oldRect = rect(0.cint, 0.cint)
      destCpy = d.destRect.deepCopy()
    if clipEnabled != 0.cint:
      discard d.renderer.getClipRect(addr oldRect)
    assert 0 == d.renderer.setClipRect(addr destCpy)
    defer:
      if clipEnabled != 0.cint:
        assert 0 == d.renderer.setClipRect(addr oldRect)
      else:
        assert 0 == d.renderer.setClipRect(nil)
    
    d.renderedWidth = 0
    for c in d.children:
      if c.width > d.width and d.width > 0:
        c.width = d.width
      c.ypos = y
      c.xpos = d.xpos
      if c.visible:
        c.render()
      y += c.height 
      y += d.vspace
      if d.renderedWidth < c.width:
        d.renderedWidth = c.width
  

