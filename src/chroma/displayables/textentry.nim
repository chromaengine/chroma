#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import sdl2, sdl2/ttf
import chroma/displayable
import chroma/displayables/[simpletext, rectangle]
import chroma/private/[fontcache, keycodedefs]
import unicode, strutils, sequtils

type
  TextEntry* = ref object of Displayable
    fontCache: FontCache
    kactive: bool
    cursor: int
    cursorPx, cursorRel: int
    committed: bool
    editCursor: int
    runestr: seq[Rune]
    editstr: seq[Rune]
    color: Color
    textSize: int
    style: FontKind
    font: string
    cursorLine: Rectangle
    renderedHeight: int
    onTextChanged*: proc(d: Displayable, text: seq[Rune])
    kalign: TextAlignment

proc newTextEntry*(renderer: RendererPtr, fontCache: FontCache): TextEntry =
  new result
  result.cursorLine = newRectangle(renderer)
  result.cursorLine.width = 5
  result.kactive = false
  result.cursor = 0
  result.editCursor = 0
  result.runestr = @[]
  result.editstr = @[]
  result.committed = false
  result.visible = true
  result.kalpha = 255
  result.enabled = true
  result.doUpdate = true
  result.onEvent = onEventDoNothing
  result.fontCache = fontCache
  result.renderer = renderer
  result.cursorPx = 0
  result.onTextChanged = proc(d: Displayable, text: seq[Rune]) =
    discard

proc `active`*(d: TextEntry): bool = d.kactive
proc `active=`*(d: TextEntry, b: bool) =
  if d.kactive != b:
    d.doUpdate = true
  d.kactive = b

proc `align=`*(d: TextEntry, a: TextAlignment) =
  d.kalign = a

proc `runes`*(d: TextEntry): seq[Rune] = d.runestr

proc `value`*(d: TextEntry): string = $d.runestr
proc `value=`*(d: TextEntry, s: string) =
  d.runestr = s.toRunes

proc `font=`*(st: TextEntry, s: string) =
  st.font = s
  st.doUpdate = true

proc `fill=`*(d: TextEntry, c: (int, int, int)) =
  d.color.r = minmax(0, c[0], 255).uint8
  d.color.g = minmax(0, c[1], 255).uint8
  d.color.b = minmax(0, c[2], 255).uint8

proc `cursorFill=`*(d: TextEntry, c: (int, int, int)) =
  d.cursorLine.fill = c

proc `r=`*(d: TextEntry, c: int) =
  d.color.r = minmax(0, c, 255).uint8

proc `g=`*(d: TextEntry, c: int) =
  d.color.g = minmax(0, c, 255).uint8

proc `b=`*(d: TextEntry, c: int) =
  d.color.b = minmax(0, c, 255).uint8

proc `r`*(d: TextEntry): int = d.color.r.int
proc `g`*(d: TextEntry): int = d.color.g.int
proc `b`*(d: TextEntry): int = d.color.b.int

method `height=`*(t: TextEntry, h: int) =
  t.destRect.h = h.cint
  t.cursorLine.height = h

method `ypos=`*(t: TextEntry, y: int) =
  t.destRect.y = y.cint
  t.cursorLine.ypos = y

method `yoffset=`*(t: TextEntry, y: int) =
  t.yoff = y.cint
  t.cursorLine.yoffset = y

method `xpos=`*(t: TextEntry, x: int) =
  t.destRect.x = x.cint
  t.cursorLine.xoffset = x

method `bold=`*(t: TextEntry, b: bool) =
  setBold(t.style, b):
    t.doUpdate = true

method `italic=`*(t: TextEntry, b: bool) =
  setItalic(t.style, b):
    t.doUpdate = true

proc underlinify(fk: FontKind): FontKind =
  case fk:
  of fkRegular: fkUnderline
  of fkBold: fkBoldUnderline
  of fkItalic: fkItalicUnderline
  of fkBoldItalic: fkBoldItalicUnderline
  else: fk

method `size=`*(t: TextEntry, i: int) =
  if i != t.textSize:
    t.doUpdate = true
  t.textSize = minmax(0, i, 2160)
  t.height = max(t.height, t.textSize)

method `size`*(t: TextEntry): int = t.textSize


method event*(d: TextEntry, event: ChromaEvent): bool =
  case event.kind:
  of eventTextInput:
    result = true
    echo "handling input ", event
    d.doUpdate = true
    d.committed = true
    d.editCursor = 0
    if event.text.len > 0:
      echo event.text.validateUtf8
      for r in runes(event.text):
        d.runestr.insert(r, d.cursor)
        d.cursor += 1
        echo "cursor advance ", r
  of eventTextEditing:
    result = true
    d.committed = false
    d.editstr = event.text.toRunes()
    d.editCursor = d.editStr.len
    d.doUpdate = true
  of eventKeyDown:
    if not d.kactive:
      result = d.onEvent(d, event)
    else:
      result = true
      case event.keycode:
      of keyLeft:
        if d.committed and d.cursor > 0:
          d.cursor -= 1
          d.doUpdate = true
        elif (not d.committed) and d.editCursor > 0:
          d.editCursor -= 1
          d.doUpdate = true
      of keyRight:
        if d.committed and d.cursor < d.runestr.len:
          d.cursor += 1
          d.doUpdate = true
        elif (not d.committed) and d.editCursor < d.editstr.len:
          d.editCursor += 1
          d.doUpdate = true
      of keyBackspace:
        if d.committed and d.cursor > 0:
          d.runestr.delete(d.cursor - 1)
          d.cursor -= 1
          d.doUpdate = true
        elif (not d.committed) and d.editCursor > 0:
          d.editCursor -= 1
          d.doUpdate = true
      of keyDelete:
        if d.committed and d.cursor < d.runestr.len:
          d.runestr.delete(d.cursor)
          d.doUpdate = true
        elif (not d.committed) and d.editCursor < d.editstr.len:
          d.doUpdate = true
      of keyHome:
        if d.committed:
          d.cursor = 0
          d.doUpdate = true
        else:
          d.editCursor = 0
          d.doUpdate = true
      of keyEnd:
        if d.committed:
          d.cursor = d.runestr.len
          d.doUpdate = true
        else:
          d.editCursor = d.editstr.len
          d.doUpdate = true
      else: discard
  else:
    result = d.onEvent(d, event)

proc renderString*(
    d: TextEntry,
    font: var FontPtr,
    swRenderer: var RendererPtr,
    s: seq[Rune]): tuple[texture: TexturePtr, width: cint, height: cint] =
  result.width = 0.cint
  var sstr = $s
  if s.len > 0:
    discard font.sizeUtf8(sstr, addr result.width, addr result.height)
  if result.width > 0.cint:
    var baseColor = color(255, 255, 255, 255)
    var lineSurf = font.renderUtf8Blended(sstr, baseColor)
    result.texture = swRenderer.createTextureFromSurface(lineSurf)
    lineSurf.destroy()

method render*(d: TextEntry) =
  if d.doUpdate or d.texture == nil:
    d.printDebug "updating ", d.debugName
    var
      font = d.fontCache.get(d.font, d.textSize, d.style)
      ulFont = d.fontCache.get(d.font, d.textSize, underlinify(d.style))
      surface = createRgbSurface(0,
        d.width.cint,
        d.height.cint,
        32, 0xff.uint32, 0xff00.uint32, 0xff0000.uint32, 0xff000000.uint32)
      swRenderer = surface.createSoftwareRenderer()
    defer:
      surface.destroy()
      swRenderer.destroy()
    
    if d.texture != nil:
      d.texture.destroy()
      d.texture = nil
    
    var
      beforeCursor = d.renderString(font, swRenderer, d.runestr[0..d.cursor - 1])
      afterCursor = d.renderString(font, swRenderer, d.runestr[d.cursor..^1])
      beforeEditCursor = d.renderString(ulFont, swRenderer, d.editstr[0..d.editCursor - 1])
      afterEditCursor = d.renderString(ulFont, swRenderer, d.editstr[d.editCursor..^1])
    
    let newCursorRel = beforeCursor.width + beforeEditCursor.width
    let dCursor = newCursorRel - d.cursorRel
    let cursorCenter =
      if d.kalign == Center:
        d.width div 2
      else:
        if d.cursorPx + dCursor <= 0:
          min(beforeEditCursor.width + beforeCursor.width, d.width div 2)
        else:
          min(d.width, d.cursorPx + dCursor)
    d.cursorPx = cursorCenter
    d.cursorRel = newCursorRel
    
    d.cursorLine.height = font.fontHeight().int
    d.cursorLine.xpos = cursorCenter
    
    let
      becXoff = cursorCenter - beforeEditCursor.width
      bcXoff = becXoff - beforeCursor.width
      aecXoff = cursorCenter
      acXoff = aecXoff + afterEditCursor.width
    
    var
      becRect = rect(becXoff.cint,
        0.cint, beforeEditCursor.width.cint, beforeEditCursor.height.cint)
      bcRect = rect(bcXoff.cint,
        0.cint, beforeCursor.width.cint, beforeCursor.height.cint)
      aecRect = rect(aecXoff.cint,
        0.cint, afterEditCursor.width.cint, afterEditCursor.height.cint)
      acRect = rect(acXoff.cint,
        0.cint, afterCursor.width.cint, afterCursor.height.cint)
    
    if beforeCursor.texture != nil:
      swRenderer.copy(beforeCursor.texture, nil, addr bcRect)
    if beforeEditCursor.texture != nil:
      swRenderer.copy(beforeEditCursor.texture, nil, addr becRect)
    if afterEditCursor.texture != nil:
      swRenderer.copy(afterEditCursor.texture, nil, addr aecRect)
    if afterCursor.texture != nil:
      swRenderer.copy(afterCursor.texture, nil, addr acRect)
    
    d.texture = d.renderer.createTextureFromSurface(surface)
    d.doUpdate = false
  
  if d.visible:
    d.texture.setTextureAlphaMod(d.kalpha)
    discard d.texture.setTextureColorMod(d.color.r, d.color.g, d.color.b)
    var dest = d.destRect.deepcopy()
    d.renderer.copyEx(d.texture, nil, addr dest, angle = 0.0, center = nil, flip = SDL_FLIP_NONE)
    if d.kactive:
      d.cursorLine.render()
  
