#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import sdl2, sdl2/[ttf, image]
import chroma/jankcrypt, chroma/displayable

type
  Frame* = ref object of ContainerDisplayable
    isRoot: bool
    renderedWidth: int
    renderedHeight: int



proc adjustEvent(d: Frame, event: ChromaEvent): ChromaEvent =
  result = event
  case event.kind:
  of eventMouseDown, eventMouseUp, eventMouseWheel, eventMouseMotion:
    result.position.x -= d.xpos
    result.position.y -= d.ypos
  of eventKeyUp, eventKeyDown:
    result.mousePos.x -= d.xpos
    result.mousePos.y -= d.ypos
  else:
    discard


method event*(d: Frame, event: ChromaEvent): bool =
  result = false
  if d.enabled and (d.tenabled <= event.timestamp) and (event in d):
    d.printDebug d.debugName, " Received event ", event, " ", d.children.len
    let adjevt = adjustEvent(d, event)
    var i = d.children.len
    while i > 0:
      d.printdebug "event..."
      result = result or d.children[i - 1].event(adjevt)
      if result:
        break
      else:
        i -= 1
    if d.onEvent != nil and not result:
      result = result or d.onEvent(d, event)

proc newFrame*(renderer: RendererPtr, width: int = -1, height: int = -1): Frame =
  new result
  result.parent = nil
  result.renderer = renderer
  result.children = @[]
  result.srcRect = rect(0.cint, 0.cint,
    if width >= 0: width.cint else: 3840.cint,
    if height >= 0: height.cint else: 2160.cint)
  result.destRect = result.srcRect
  result.xoff = 0.cint
  result.yoff = 0.cint
  result.onEvent = onEventContainerDefault
  result.kalpha = 255
  result.visible = true
  result.enabled = true

proc shadow(p, c: Displayable): tuple[xpos, ypos, xoffset, yoffset, width, height: int, alpha: uint8] =
  result.xpos = c.xpos
  result.ypos = c.ypos
  result.xoffset = c.xoffset
  result.yoffset = c.yoffset
  result.width = c.width
  result.height = c.height
  result.alpha = c.alpha
  c.xpos = c.xpos + p.xpos
  c.ypos = c.ypos + p.ypos
  c.xoffset = c.xoffset + p.xoffset
  c.yoffset = c.yoffset + p.yoffset
  c.alpha = (c.alpha.uint32 * p.alpha.uint32 div 255).uint8

proc unshadow(c: Displayable, prop: tuple[xpos, ypos, xoffset, yoffset, width, height: int, alpha: uint8]) =
  c.xpos = prop.xpos
  c.ypos = prop.ypos
  c.xoffset = prop.xoffset
  c.yoffset = prop.yoffset
  c.width = prop.width
  c.height = prop.height
  c.alpha = prop.alpha

method render*(d: Frame) =
  d.printDebug "rendering ", d.debugName
  for c in d.children:
    if c.visible:
      c.printDebug "rendering child ", c.debugName
      let s = shadow(d, c)
      c.render()
      unshadow(c, s)
    else:
      c.printDebug "invisible child ", c.debugName
  d.printDebug "finished rendering ", d.debugName
