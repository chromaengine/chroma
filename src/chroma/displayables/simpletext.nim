#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import sdl2, sdl2/ttf
import chroma/displayable
import chroma/private/[fontcache]
import sequtils, strutils

type
  SimpleText* = ref object of Displayable
    fontCache: FontCache
    font: string
    color: Color
    fullStr: string
    clipHeight: int
    baseLineSpacing: int
    renderedHeight: int
    indent: int
    textSize: int
    kalign: TextAlignment
    style: FontKind
    endw: int
  
  SimpleTextState* = object
    ## A serializable object that represents a SimpleText's state.
    ## Used by the loading/saving/rollback system.
    font*, fullStr*: string
    xpos*, ypos*, width*, height*, indent*, size*: int
    spacing*, r*, g*, b*, alpha*, clipHeight*: int
    align*: TextAlignment
    style*: FontKind
    enabled*, visible*: bool

proc newSimpleText*(renderer: RendererPtr, fontCache: FontCache): SimpleText =
  new result
  result.renderer = renderer
  result.fontCache = fontCache
  result.doUpdate = true
  result.style = fkRegular
  result.textSize = 100
  result.kalpha = 255
  result.indent = 0
  result.width = 3840
  result.height = 100
  result.color = color(0, 0, 0, 255)
  result.enabled = true
  result.visible = true
  result.kalign = Left
  result.font = "default"

proc `text=`*(st: SimpleText, s: string) =
  st.fullStr = s
  st.doUpdate = true

proc `text`*(st: SimpleText): string = st.fullStr

proc `value=`*(st: SimpleText, s: string) = st.text = s
proc `value`*(st: SimpleText): string = st.text


proc `endx`*(st: SimpleText): int = st.endw

proc `renderedHeight`*(st: SimpleText): int = st.renderedHeight

proc `appendpos`*(st: SimpleText): (int, int) =
  if st.endw == 0:
    (st.xpos, st.ypos + st.height)
  else:
    (st.xpos, st.ypos + st.renderedHeight - st.baseLineSpacing)


proc `indent=`*(st: SimpleText, i: int) =
  st.indent = i
  st.doUpdate = true

proc `font=`*(st: SimpleText, s: string) =
  st.font = s
  st.doUpdate = true

proc `fill=`*(d: SimpleText, c: (int, int, int)) =
  d.color.r = minmax(0, c[0], 255).uint8
  d.color.g = minmax(0, c[1], 255).uint8
  d.color.b = minmax(0, c[2], 255).uint8

proc `r=`*(d: SimpleText, c: int) =
  d.color.r = minmax(0, c, 255).uint8

proc `g=`*(d: SimpleText, c: int) =
  d.color.g = minmax(0, c, 255).uint8

proc `b=`*(d: SimpleText, c: int) =
  d.color.b = minmax(0, c, 255).uint8

proc `r`*(d: SimpleText): int = d.color.r.int
proc `g`*(d: SimpleText): int = d.color.g.int
proc `b`*(d: SimpleText): int = d.color.b.int

proc `align`*(d: SimpleText): TextAlignment = d.kalign
proc `align=`*(d: SimpleText, a: TextAlignment) =
  if d.kalign != a:
    d.kalign = a
    d.doUpdate = true

proc `spacing`*(d: SimpleText): int = d.baseLineSpacing
proc `spacing=`*(d: SimpleText, i: int) =
  d.baseLineSpacing = i

proc `bold=`*(t: SimpleText, b: bool) =
  setBold(t.style, b):
    t.doUpdate = true

proc `italic=`*(t: SimpleText, b: bool) =
  setItalic(t.style, b):
    t.doUpdate = true

proc `underline=`*(t: SimpleText, b: bool) =
  setUnderline(t.style, b):
    t.doUpdate = true

proc `size=`*(t: SimpleText, i: int) =
  t.textSize = minmax(0, i, 2160)

proc `size`*(t: SimpleText): int = t.textSize


proc makeValidLine(
    words: var seq[tuple[str: string, width: cint]],
    font: FontPtr,
    spWidth: cint,
    maxWidth: int): tuple[str: string, width: cint] =
  if words.len > 0:
    if words[0].width > maxWidth:
      let w = words[0]
      words.delete(0, 0)
      for i in countdown(w.str.len - 1, 1):
        result.str = w.str[0..i - 1]
        result.width = font.stringWidth(result.str)
        if result.width <= maxWidth:
          let s = w.str[i..^1]
          words.insert(@[(str: s, width: font.stringWidth(s))], 0)
          break
    else:
      result.str = words[0].str
      result.width = words[0].width
      words.delete(0, 0)
      var i = 0
      while i < words.len:
        let w = words[i]
        if result.width + spWidth + w.width <= maxWidth:
          result.width += spWidth + w.width
          result.str.add " "
          result.str.add w.str
          i += 1
        elif w.width > maxWidth:
          for j in countdown(w.str.len - 2, 2):
            let w0 = w.str[0..j-1]
            let w1 = w.str[j..^1]
            let w0width = font.stringWidth(w0)
            if result.width + spWidth + w0width <= maxWidth:
              result.width += spWidth + w0width
              result.str.add " "
              result.str.add w0
              words[i].str = w1
              words[i].width = font.stringWidth(w1)
              break
          break
        else:
          break
      if i > 0:
        words.delete(0, i - 1)


proc createValidLines(st: SimpleText, font: FontPtr): seq[tuple[str: string, width: cint]] =
  result = @[]
  let
    spWidth = font.spaceWidth()
    paragraphs = st.fullStr.splitlines().mapIt(it.split())
  
  if st.fullStr.len > 0:
    var indent = if st.indent < st.width: st.indent else: 0
    for strs in paragraphs:
      var words = strs.mapIt (str: it, width: font.stringWidth(it))
      while words.len > 0:
        let l = makeValidLine(words, font, spWidth, st.width - indent)
        if l.width > 0:
          result.add l
        indent = 0


proc renderLeft(st: SimpleText, font: var FontPtr): TexturePtr =
  let
    lineSkip = font.fontLineSkip()
    lines = st.createValidLines(font)
  st.baseLineSpacing = lineSkip
  st.renderedHeight = lineSkip * lines.len
  var surface = createRgbSurface(0,
    st.width.cint,
    st.height.cint,
    32, 0xff.uint32, 0xff00.uint32, 0xff0000.uint32, 0xff000000.uint32)
  defer: surface.freeSurface()
  for i, line in lines:
    var
      c = color(255, 255, 255, 255)
      lineSurf = font.renderUtf8BlendedWrapped(
        line.str, c, st.width.uint32)
      destRect = rect(
        if i == 0: st.indent.cint else: 0.cint,
        i.cint * lineSkip.cint,
        line.width.cint, lineSkip.cint)
      srcRect = rect(
        0.cint,
        0.cint,
        line.width.cint, lineSkip.cint)
    st.endw = if i == 0: line.width + st.indent else: line.width
    assert lineSurf != nil
    assert lineSurf.blitSurface(addr srcRect, surface, addr destRect)
    lineSurf.freeSurface()
  result = st.renderer.createTextureFromSurface(surface)

proc renderRight(st: SimpleText, font: var FontPtr): TexturePtr =
  let
    lineSkip = font.fontLineSkip()
    lines = st.createValidLines(font)
  st.baseLineSpacing = lineSkip
  st.renderedHeight = lineSkip * lines.len
  var surface = createRgbSurface(0,
    st.width.cint,
    st.height.cint,
    32, 0xff.uint32, 0xff00.uint32, 0xff0000.uint32, 0xff000000.uint32)
  defer: surface.freeSurface()
  for i, line in lines:
    var
      c = color(255, 255, 255, 255)
      lineSurf = font.renderUtf8BlendedWrapped(
        line.str, c, st.width.uint32)
      destRect = rect(
        st.width.cint - line.width.cint,
        i.cint * lineSkip.cint,
        line.width.cint, lineSkip.cint)
      srcRect = rect(
        0.cint,
        0.cint,
        line.width.cint, lineSkip.cint)
    st.endw = line.width
    assert lineSurf != nil
    assert lineSurf.blitSurface(addr srcRect, surface, addr destRect)
    lineSurf.freeSurface()
  result = st.renderer.createTextureFromSurface(surface)

proc renderCenter(st: SimpleText, font: var FontPtr): TexturePtr =
  let
    lineSkip = font.fontLineSkip()
    lines = st.createValidLines(font)
  st.baseLineSpacing = lineSkip
  st.renderedHeight = lineSkip * lines.len
  var surface = createRgbSurface(0,
    st.width.cint,
    st.height.cint,
    32, 0xff.uint32, 0xff00.uint32, 0xff0000.uint32, 0xff000000.uint32)
  defer: surface.freeSurface()
  for i, line in lines:
    var
      c = color(255, 255, 255, 255)
      lineSurf = font.renderUtf8BlendedWrapped(
        line.str, c, st.width.uint32)
      destRect = rect(
        (st.width.cint - line.width.cint) div 2,
        i.cint * lineSkip.cint,
        line.width.cint, lineSkip.cint)
      srcRect = rect(
        0.cint,
        0.cint,
        line.width.cint, lineSkip.cint)
    st.endw = line.width
    assert lineSurf != nil
    assert lineSurf.blitSurface(addr srcRect, surface, addr destRect)
    lineSurf.freeSurface()
  result = st.renderer.createTextureFromSurface(surface)

method render*(st: SimpleText) =
  if (st.doUpdate or st.texture == nil) and st.fullStr.len > 0:
    var font = st.fontCache.get(st.font, st.textSize, st.style)
    if st.texture != nil:
      st.texture.destroy()
      st.texture = nil
    st.texture = case st.kalign:
      of Left: st.renderLeft(font)
      of Right: st.renderRight(font)
      of Center: st.renderCenter(font)
    st.doUpdate = false
  if st.visible and st.fullStr.len > 0:
    var
      clipRect = rect(st.xpos.cint, st.ypos.cint, st.width.cint, st.height.cint)
      srcRect = rect(0.cint, 0.cint, st.width.cint, st.height.cint)
      destRect = rect(st.xpos.cint + st.xoff.cint, st.ypos.cint + st.yoff.cint, st.width.cint, st.height.cint)
      shadowClipRect: Rect
    st.texture.setTextureAlphaMod(st.kalpha)
    discard st.texture.setTextureColorMod(st.color.r, st.color.g, st.color.b)
    st.renderer.copyEx(st.texture,
      srcRect, destRect, angle = 0.0, center = nil, flip = SDL_FLIP_NONE)
    

proc getState*(st: SimpleText): SimpleTextState =
  result.font = st.font
  result.fullStr = st.fullStr
  result.xpos = st.xpos
  result.ypos = st.ypos
  result.width = st.destRect.w
  result.height = st.destRect.h
  result.indent = st.indent
  result.size = st.size
  result.spacing = st.spacing
  result.r = st.r
  result.g = st.g
  result.b = st.b
  result.alpha = st.alpha.int
  result.clipHeight = st.clipHeight
  result.align = st.align
  result.style = st.style
  result.enabled = st.enabled
  result.visible = st.visible

proc loadState*(st: SimpleText, state: SimpleTextState) =
  st.font = state.font
  st.fullStr = state.fullStr
  st.xpos = state.xpos
  st.ypos = state.ypos
  st.width = state.width
  st.height = state.height
  st.indent = state.indent
  st.size = state.size
  st.spacing = state.spacing
  st.r = state.r
  st.g = state.g
  st.b = state.b
  st.alpha = state.alpha.uint8
  st.clipHeight = state.clipHeight
  st.align = state.align
  st.style = state.style
  st.enabled = state.enabled
  st.visible = state.visible

proc `$`*(state: SimpleTextState): string =
  state.fullStr
