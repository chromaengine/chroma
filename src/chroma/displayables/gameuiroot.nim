#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import sdl2
import sequtils, macros
import chroma
import chroma/displayable, chroma/displayables/animation
import chroma/private/[imagecache, fontcache, asthelpers]

type
  GameUiRoot* = ref object of ContainerDisplayable
    childRenderer: RendererPtr
    imageCache: ImageCache
    fontCache: FontCache
    updateRecurse: int
    onUpdateHook: proc(internal: ChromaInternal[GameVar], argpack: ArgPack): bool
    activeAnimations: seq[Animation]
    lastRender: uint32
    bg: Displayable

proc newGameUiRoot*(
    renderer: RendererPtr,
    imageCache: ImageCache,
    fontCache: FontCache,
    width: range[1..3840],
    height: range[1..2160]): GameUiRoot =
  new result
  result.renderer = renderer
  result.destRect = rect(0.cint, 0.cint, width.cint, height.cint)
  result.srcRect = rect(0.cint, 0.cint, width.cint, height.cint)
  result.surface = createRgbSurface(0.cint,
    width.cint, height.cint, 32.cint,
    0xff.uint32, 0xff00.uint32, 0xff0000.uint32, 0xff000000.uint32)
  result.childRenderer = result.surface.createSoftwareRenderer()
  result.imageCache = imageCache
  result.fontCache = fontCache
  result.doUpdate = true
  result.visible = false
  result.enabled = false
  result.alpha = 255
  result.bg = nil
  result.updateRecurse = 0
  result.onEvent = proc(d: Displayable, e: ChromaEvent): bool = false
#     (e in d) and (e.kind notin {eventKeyDown, eventKeyUp}) # ui blocks should always capture any event that occurs within their bounds
  result.children = @[]
  result.onUpdateHook = proc(internal: ChromaInternal[GameVar], argpack: ArgPack): bool =
    false

method getRenderer*(d: GameUiRoot): RendererPtr = d.childRenderer

proc `background=`*(d: GameUiRoot, b: Displayable) =
  if d.bg != nil:
    d.bg.destroy()
  b.width = d.width
  b.height = d.height
  b.xpos = d.xpos
  b.ypos = d.ypos
  d.bg = b

proc `background=`*(d: GameUiRoot, path: string) =
  if d.bg != nil:
    d.bg.destroy()
  var img = newSimpleImage(d.renderer, d.imageCache)
  img.path = path
  img.preserveAspectRatio = false
  img.width = d.width
  img.height = d.height
  img.xpos = d.xpos
  img.ypos = d.ypos
  d.bg = img

proc `background=`*(d: GameUiRoot, rct: tuple[fill: (int, int, int), alpha: int, radius: int]) =
  if d.bg != nil:
    d.bg.destroy()
  var r = newRectangle(d.renderer)
  r.fill = rct.fill
  r.radius = rct.radius
  r.width = d.width
  r.height = d.height
  r.alpha = minmax(0, rct.alpha, 255).uint8
  r.xpos = d.xpos
  r.ypos = d.ypos
  d.bg = r

proc adjustEvent(d: GameUiRoot, event: ChromaEvent): ChromaEvent =
  result = event
  case event.kind:
  of eventMouseDown, eventMouseUp, eventMouseWheel, eventMouseMotion:
    result.position.x -= d.xpos
    result.position.y -= d.ypos
  of eventKeyUp, eventKeyDown:
    result.mousePos.x -= d.xpos
    result.mousePos.y -= d.ypos
  else:
    discard

method event*(d: GameUiRoot, evt: ChromaEvent): bool =
  result = false
  if d.enabled and (d.tenabled <= evt.timestamp):
    let adjEvt = d.adjustEvent(evt)
    for child in d.children:
      result = result or child.event(adjEvt)
      if result:
        break
    if d.onEvent != nil and not result:
      result = result or d.onEvent(d, evt)
      if d.visible and d.bg != nil and evt.kind in {eventMouseDown, eventMouseUp, eventMouseWheel}:
        result = result or (evt in d)

proc `onUpdate=`*(d: GameUiRoot, hook: proc(internal: ChromaInternal[GameVar], argpack: ArgPack): bool) =
  d.onUpdateHook = hook

proc update*(d: GameUiRoot, internal: ChromaInternal[GameVar], argpack: ArgPack): bool =
  d.updateRecurse += 1
  defer: d.updateRecurse -= 1
  if d.updateRecurse > 0xFFFF:
    raise Exception.newException("error: infinite update loop detected")
  d.destroyAllChildren()
  if d.onUpdateHook != nil:
    result = d.onUpdateHook(internal, argpack)

method refresh*(d: GameUiRoot, internal: ChromaInternal[GameVar], animations: seq[Animation], argpack: ArgPack) {.base.} =
  discard d.update(internal, argpack)
  for a in animations:
    internal.animations.add a
  d.doUpdate = true

method show*(d: GameUiRoot, internal: ChromaInternal[GameVar], animations: seq[Animation], argpack: ArgPack) {.base.} =
  if not d.visible:
    discard d.update(internal, argpack)
  d.visible = true
  d.enabled = true
  d.alpha = 255
  d.doUpdate = true
  assert internal != nil
  assert internal.animations != nil
  for a in animations:
    assert a != nil
    internal.animations.add a
    a.run()

method hide*(d: GameUiRoot, internal: ChromaInternal[GameVar], animations: seq[Animation]) {.base.} =
  for a in animations:
    internal.animations.add a
  internal.animations.add newHideAnimation(d, internal)

method hide*(d: GameUiRoot, internal: ChromaInternal[GameVar], showArgs: seq[ShowArg]) {.base.} =
  for a in showArgs:
    case a.kind:
    of sarAnimation:
      internal.animations.add a.animation
      a.animation.run()
    of sarNothing:
      discard
    else:
      echo "WARNING: invalid showArg given to ui.hide call"
  internal.animations.add newHideAnimation(d, internal)

method hideDontDisable*(d: GameUiRoot, internal: ChromaInternal[GameVar], animations: seq[Animation]) {.base.} =
  for a in animations:
    internal.animations.add a
  internal.animations.add newHideDontDisableAnimation(d, internal)

method hideDontDisable*(d: GameUiRoot, internal: ChromaInternal[GameVar], showArgs: seq[ShowArg]) {.base.} =
  for a in showArgs:
    case a.kind:
    of sarAnimation:
      internal.animations.add a.animation
      a.animation.run()
    of sarNothing:
      discard
    else:
      echo "WARNING: invalid showArg given to ui.hideDontDisable call"
  internal.animations.add newHideDontDisableAnimation(d, internal)

method init*(d: GameUiRoot, internal: ChromaInternal[GameVar]) {.base.} =
  assert false

method cull*(d: GameUiRoot) =
  if d.lastRender + 10000'u32 < gfxTime():
    if d.texture != nil:
      d.texture.destroy()
    d.texture = nil

method render*(d: GameUiRoot) =
  if d.visible:
    d.lastRender = gfxTime()
    if d.doUpdate or d.texture == nil:
      d.printDebug "redrawing ", d.debugName
      d.childRenderer.clear()
      if d.texture != nil:
        d.texture.destroy()
      for child in d.children:
        child.render()
      d.texture = d.renderer.createTextureFromSurface(d.surface)
      d.doUpdate = false
    d.printDebug "rendering ", d.debugName
    if d.bg != nil:
      d.bg.width = d.width
      d.bg.height = d.height
      d.bg.xpos = d.xpos
      d.bg.ypos = d.ypos
      let shadowAlpha = d.bg.alpha
      d.bg.alpha = uint8((d.bg.alpha.int * d.alpha.int) div 255)
      d.bg.render()
      d.bg.alpha = shadowAlpha
    d.texture.setTextureAlphaMod(d.kalpha)
    var shadow = rect(
      d.destRect.x + d.xoff,
      d.destRect.y + d.yoff,
      d.destRect.w,
      d.destRect.h)
    d.renderer.copyEx(d.texture, nil,
      addr shadow,
      angle = 0.0.cdouble,
      center = nil,
      flip = SDL_FLIP_NONE)
  else:
    d.printDebug "not rendering ui ", d.debugName, " is not visible"

method `doUpdate=`*(d: GameUiRoot, y: bool) =
  d.kDoUpdate = y
  if y:
    d.printDebug "doUpdate=true for ", d.debugName

method destroy*(d: GameUiRoot) =
  if d.children.len > 0:
    for c in d.children:
      c.parent = nil
      c.destroy()
    d.children = @[]
  if d.surface != nil:
    d.surface.destroy()
    d.surface = nil
  if d.texture != nil:
    d.texture.destroy()
    d.texture = nil
  if d.childRenderer != nil:
    d.childRenderer.destroy()
    d.childRenderer = nil




proc escapeToClose*[T](internal: ChromaInternal[T], uiElement: GameUiRoot): proc(d: Displayable, e: ChromaEvent): bool =
  return proc(d: Displayable, e: ChromaEvent): bool =
    # if the event is a key-down, and the keycode is Escape...
    if e.kind == eventKeyDown and e.keycode == keyEscape:
      # ... then check if the settings are currently visible.
      if uiElement.visible:
        # if they are visible, hide them with a fade out,
        # but don't disable it, since disabling them would mean
        # that this event handler wouldn't get to see future
        # esc key presses.
        uiElement.hide(internal, seq[Animation](@[]))
        return true # return true since we did something with this event.
    elif e.kind in {eventMouseDown, eventMouseUp, eventMouseWheel, eventMouseMotion} and d.visible:
      return true # capture any mouse clicks




