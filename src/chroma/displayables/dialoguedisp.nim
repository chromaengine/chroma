#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import sdl2, sdl2/ttf
import macros
import chroma/displayable
import chroma/displayables/[rectangle, dialoguetext, simpletext]
import chroma/private/[fontcache, imageCache]

type
  DialogueDisp* = ref object of ContainerDisplayable
    dialogueText*: DialogueText
    nameText*: SimpleText
    fontCache*: FontCache
    imageCache*: ImageCache
    lastRender*: uint32
  
  DialogueDispState* = object
    ntState*: SimpleTextState
    dtState*: DialogueTextState
    xpos*, ypos*, xoffset*, yoffset*, width*, height*: int
    visible*, enabled*: bool
    alpha*: uint8



proc adjustEvent(d: DialogueDisp, event: ChromaEvent): ChromaEvent =
  result = event
  case event.kind:
  of eventMouseDown, eventMouseUp, eventMouseWheel, eventMouseMotion:
    result.position.x -= d.xpos
    result.position.y -= d.ypos
  of eventKeyUp, eventKeyDown:
    result.mousePos.x -= d.xpos
    result.mousePos.y -= d.ypos
  else:
    discard


method event*(d: DialogueDisp, event: ChromaEvent): bool =
  result = false
  d.printDebug d.debugName, " Received event ", d.destRect
  if d.enabled and (d.tenabled <= event.timestamp) and (event in d):
    let adjevt = adjustEvent(d, event)
    var i = d.children.len
    while i > 0:
      result = result or d.children[i - 1].event(adjevt)
      if result:
        break
      else:
        i -= 1
    if d.onEvent != nil and not result:
      result = result or d.onEvent(d, event)

proc newDialogueDisp*(renderer: RendererPtr, fontCache: FontCache, imageCache: ImageCache): DialogueDisp =
  new result
  result.children = @[]
  result.renderer = renderer
  result.fontCache = fontCache
  result.imageCache = imageCache
  result.nameText = newSimpleText(renderer, fontCache)
  result.nameText.parent = result
  result.dialogueText = newDialogueText(renderer, fontCache,
    DialogueTextFlags(
      bold: off, italic: off, underline: off,
      font: "default",
      color: (0, 0, 0),
      size: 75))
  result.dialogueText.parent = result
  result.enabled = true
  result.visible = true
  result.doUpdate = true
  result.alpha = 255

proc getState*(d: DialogueDisp): DialogueDispState =
  result.xpos = d.xpos
  result.ypos = d.ypos
  result.xoffset = d.xoffset
  result.yoffset = d.yoffset
  result.width = d.width
  result.height = d.height
  result.alpha = d.alpha
  result.visible = d.visible
  result.enabled = d.enabled
  result.ntState = d.nameText.getState()
  result.dtState = d.dialogueText.getState()

proc loadState*(d: DialogueDisp, state: DialogueDispState) =
  d.nameText.loadState(state.ntState)
  d.dialogueText.loadState(state.dtState)
  d.visible = state.visible
  d.enabled = state.enabled
  d.alpha = state.alpha
  d.xpos = state.xpos
  d.ypos = state.ypos
  d.xoffset = state.xoffset
  d.yoffset = state.yoffset
  d.width = state.width
  d.height = state.height

proc shadow(p, c: Displayable): tuple[xpos, ypos, xoffset, yoffset, width, height: int, alpha: uint8] =
  result.xpos = c.xpos
  result.ypos = c.ypos
  result.xoffset = c.xoffset
  result.yoffset = c.yoffset
  result.width = c.width
  result.height = c.height
  result.alpha = c.alpha
  c.xpos = c.xpos + p.xpos
  c.ypos = c.ypos + p.ypos
  c.xoffset = c.xoffset + p.xoffset
  c.yoffset = c.yoffset + p.yoffset
  c.alpha = (c.alpha.uint32 * p.alpha.uint32 div 255).uint8

proc unshadow(c: Displayable, prop: tuple[xpos, ypos, xoffset, yoffset, width, height: int, alpha: uint8]) =
  c.xpos = prop.xpos
  c.ypos = prop.ypos
  c.xoffset = prop.xoffset
  c.yoffset = prop.yoffset
  c.width = prop.width
  c.height = prop.height
  c.alpha = prop.alpha

method render*(d: DialogueDisp) =
  if d.visible:
    d.lastRender = gfxTime()
    for child in d.children:
      let prop = shadow(d, child)
      child.render()
      unshadow(child, prop)
    let nprop = shadow(d, d.nameText)
    d.nameText.render()
    unshadow(d.nameText, nprop)
    let prop = shadow(d, d.dialogueText)
    d.dialogueText.render()
    unshadow(d.dialogueText, prop)

# method `alpha=`*(d: DialogueDisp, a: uint8) =
#   d.kalpha = a
# 
# proc newDialogueDisp*(renderer: RendererPtr, fontCache: FontCache): DialogueDisp =
#   new result
#   result.renderer = renderer
#   result.nameRect = newRectangle(renderer)
#   result.destRect = rect(0.cint, 0.cint, 0.cint, 0.cint)
#   result.dialogueRect = newRectangle(renderer)
#   result.dialogueText = newDialogueText(renderer, fontCache,
#     DialogueTextFlags(
#       bold: off, italic: off, underline: off,
#       font: "default",
#       color: (0, 0, 0),
#       size: 75))
#   result.nameText = newSimpleText(renderer, fontCache)
#   result.nameText.font = "default"
#   result.enabled = true
#   result.visible = true
#   result.doUpdate = true
#   result.alpha = 255
# 
# method render*(d: DialogueDisp) =
#   let tempAlphas = (d.nameRect.alpha, d.nameText.alpha, d.dialogueRect.alpha, d.dialogueText.alpha)
#   d.nameRect.alpha = ((d.nameRect.alpha.int * d.alpha.int) / 256).uint8
#   d.nameText.alpha = ((d.nameText.alpha.int * d.alpha.int) / 256).uint8
#   d.dialogueRect.alpha = ((d.dialogueRect.alpha.int * d.alpha.int) / 256).uint8
#   d.dialogueText.alpha = ((d.dialogueText.alpha.int * d.alpha.int) / 256).uint8
#   d.nameRect.render()
#   d.dialogueRect.render()
#   d.nameText.render()
#   d.dialogueText.render()
#   (d.nameRect.alpha, d.nameText.alpha, d.dialogueRect.alpha, d.dialogueText.alpha) = tempAlphas
# 
# proc getState*(d: DialogueDisp): DialogueDispState =
#   result.ntState = d.nameText.getState()
#   result.dtState = d.dialogueText.getState()
#   result.visible = d.visible
#   result.enabled = d.enabled
#   result.alpha = d.alpha
# 
# proc loadState*(d: DialogueDisp, state: DialogueDispState) =
#   d.nameText.loadState(state.ntState)
#   d.dialogueText.loadState(state.dtState)
#   d.visible = state.visible
#   d.enabled = state.enabled
#   d.alpha = state.alpha
