#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import chroma/displayable
import math, tables, sequtils

type
  Animation* = ref object of RootObj
    displayable*: Displayable
    done*: bool
    nruns*: int
    tCreated*: uint32
    blocking*: bool
    sems*: AnimationSemaphore
  
  AnimationSemaphore* = object
    # used to flag "what this animation manipulates."
    # do NOT set `hide` to true unless you want to break stuff.
    x*, y*, w*, h*, alpha*, color*, other*, hide*: bool
  
  AnimationTable* = ref object
    tbl: TableRef[Displayable, seq[Animation]]

  MoveAnimation* = ref object of Animation
    xdest*, ydest*: int
    t*: int
    delayto: int
    xacc, yacc, wacc, hacc: float
    xvec, yvec, wvec, hvec: float

  FadeAnimation* = ref object of Animation
    t*: int
    aval: float
    avec: float
    to: uint8
  
  PidMoveAnimation* = ref object of Animation
    kp*, ki*, kd*: float
    xdest, ydest, wdest, hdest: int
    lastxerr, lastyerr, lastwerr, lastherr: float
    t*: uint32
    nVeryClose: int



func overlaps*(a, b: AnimationSemaphore): bool =
  (a.x and b.x) or
  (a.y and b.y) or
  (a.w and b.w) or
  (a.h and b.h) or
  (a.alpha and b.alpha) or
  (a.color and b.color) or
  (a.other and b.other)

func overlaps*(a, b: Animation): bool =
  a.sems.overlaps b.sems


proc len*(atbl: AnimationTable): int =
  result = 0
  for v in atbl.tbl.values:
    result += v.len


method run*(a: Animation) {.base.} =
  a.done = true


proc adjX(a: PidMoveAnimation, dt: float) =
  let
    xerr = (a.xdest.float - a.displayable.xpos.float) / 100.float
    dx = xerr - a.lastxerr
    xadj = 
      (a.kp * xerr) +
      (a.ki * (xerr * dt)) +
      (a.kd * (dx / dt))
  # echo "xadj=", xadj, "\t\terr=", xerr
  a.displayable.xpos = a.displayable.xpos + round(xadj).int
  a.lastxerr = xerr

proc adjY(a: PidMoveAnimation, dt: float) =
  let
    yerr = (a.ydest.float - a.displayable.ypos.float) / 100.float
    dy = yerr - a.lastyerr
    yadj = 
      (a.kp * yerr) +
      (a.ki * (yerr * dt)) +
      (a.kd * (dy / dt))
  # echo "yadj=", yadj, "\t\terr=", yerr
  a.displayable.ypos = a.displayable.ypos + round(yadj).int
  a.lastyerr = yerr

proc adjW(a: PidMoveAnimation, dt: float) =
  let
    werr = (a.wdest.float - a.displayable.width.float) / 100.float
    dw = werr - a.lastwerr
    wadj = 
      (a.kp * werr) +
      (a.ki * (werr * dt)) +
      (a.kd * (dw / dt))
  # echo "wadj=", wadj, "\t\terr=", werr
  a.displayable.width = a.displayable.width + round(wadj).int
  a.lastwerr = werr

proc adjH(a: PidMoveAnimation, dt: float) =
  let
    herr = (a.hdest.float - a.displayable.height.float) / 100.float
    dh = herr - a.lastherr
    hadj = 
      (a.kp * herr) +
      (a.ki * (herr * dt)) +
      (a.kd * (dh / dt))
  # echo "hadj=", hadj, "\t\terr=", herr
  a.displayable.height = a.displayable.height + round(hadj).int
  a.lastherr = herr


method run*(a: PidMoveAnimation) =
  if not a.done and gfxTime() > a.t:
    let curTime = gfxTime()
    let dt = curTime - a.t
    
    if a.sems.x:
      a.adjx(dt.float / 10)
    if a.sems.y:
      a.adjy(dt.float / 10)
    if a.sems.w:
      a.adjw(dt.float / 10)
    if a.sems.h:
      a.adjh(dt.float / 10)
    
    a.t = curTime
    a.nruns += 1
    
    if abs(a.lastxerr) < 0.1 and
       abs(a.lastyerr) < 0.1 and
       abs(a.lastwerr) < 0.3 and
       abs(a.lastherr) < 0.3:
      a.nVeryClose += 1
    else:
      a.nVeryClose = 0
    
    if a.nVeryClose == 10 or a.nruns > 1000:
      a.displayable.xpos = a.xdest
      a.displayable.ypos = a.ydest
      a.displayable.width = a.wdest
      a.displayable.height = a.hdest
      a.done = true

proc newPidMoveAnimation*(d: Displayable, x, y, w, h: int, kp = 0.1, ki = 0.01, kd = 0.001): PidMoveAnimation =
  new result
  result.tCreated = gfxTime()
  result.displayable = d
  result.xdest = x
  result.ydest = y
  result.wdest = w
  result.hdest = h
  result.kp = kp
  result.ki = ki
  result.kd = kd
  result.lastxerr = (x.float - d.xpos.float) / 100.float
  result.lastyerr = (y.float - d.ypos.float) / 100.float
  result.lastwerr = (w.float - d.width.float) / 100.float
  result.lastherr = (h.float - d.height.float) / 100.float
  result.sems = AnimationSemaphore(
    x: abs(result.lastxerr) > 0.01,
    y: abs(result.lastyerr) > 0.01,
    w: abs(result.lastwerr) > 0.01,
    h: abs(result.lastherr) > 0.01)
  result.t = gfxTime()
  result.blocking = true
  echo result.lastxerr, " ", result.lastyerr, " ", result.lastwerr, " ", result.lastherr

# MoveAnimation

method run*(a: MoveAnimation) =
  if not a.done:
    if a.nruns < a.delayto:
      a.nruns += 1
    else:
      a.xacc += a.xvec
      a.yacc += a.yvec
      a.wacc += a.wvec
      a.hacc += a.hvec
      a.displayable.xpos = a.xacc.int
      a.displayable.ypos = a.yacc.int
      a.displayable.width = a.wacc.int
      a.displayable.height = a.hacc.int
      a.nruns += 1
      if a.nruns == (a.t + a.delayto):
        a.done = true

proc newMoveAnimation*(d: Displayable, x, y, w, h, t, delay: int): MoveAnimation =
  new result
  result.tCreated = gfxTime()
  result.displayable = d
  result.xdest = x
  result.ydest = y
  result.t = if t > 0: t else: 1
  result.delayto = if delay > 0: delay else: 0
  result.xacc = d.xpos.float
  result.yacc = d.ypos.float
  result.wacc = d.width.float
  result.hacc = d.height.float
  result.xvec = (x.float - result.xacc) / result.t.float
  result.yvec = (y.float - result.yacc) / result.t.float
  result.wvec = (w.float - result.wacc) / result.t.float
  result.hvec = (h.float - result.hacc) / result.t.float
  result.done = false
  result.nruns = 0
  result.blocking = true
  result.sems.x = result.xvec > 0
  result.sems.y = result.yvec > 0
  result.sems.w = result.wvec > 0
  result.sems.h = result.hvec > 0


method run*(a: FadeAnimation) =
  if not a.done:
    a.aval += a.avec
    a.displayable.alpha =
      if a.aval > 255.0:
        255'u8
      elif a.aval < 0.0:
        0'u8
      else:
        a.aval.uint8
    a.nruns += 1
    if a.displayable.alpha == a.to:
      a.done = true

proc newFadeInAnimation*(d: Displayable, t: int, to: uint8 = 255): FadeAnimation =
  new result
  result.tCreated = gfxTime()
  result.displayable = d
  result.aval = d.alpha.float
  result.t =
    if t > 0:
      t
    else:
      1
  result.sems.alpha = true
  result.to = to
  result.avec = (to.float - d.alpha.float) / t.float
  result.blocking = true

proc newFadeOutAnimation*(d: Displayable, t: int, to: uint8 = 0): FadeAnimation =
  new result
  result.tCreated = gfxTime()
  result.displayable = d
  result.aval = d.alpha.float
  result.t =
    if t > 0:
      t
    else:
      1
  result.sems.alpha = true
  result.to = to
  result.avec = (to.float - d.alpha.float) / t.float
  result.blocking = true


proc newAnimationTable*(): AnimationTable =
  new result
  result.tbl = newTable[Displayable, seq[Animation]]()

proc add*(atbl: AnimationTable, a: Animation) =
  assert a.displayable != nil
  assert atbl.tbl != nil
  if not atbl.tbl.contains(a.displayable):
    atbl.tbl[a.displayable] = @[a]
  else:
    let aseq = atbl.tbl[a.displayable]
    for aa in aseq:
      if aa.sems.hide or (a.overlaps(aa) and (not aa.done) and (not a.done)):
        aa.done = true
    atbl.tbl[a.displayable].add a

iterator allAnimations*(atbl: AnimationTable): Animation =
  for k, v in atbl.tbl:
    for a in v:
      yield a

iterator animationsFor*(atbl: AnimationTable, d: Displayable): Animation =
  if d in atbl.tbl:
    for a in atbl.tbl[d]:
      yield a

proc drive*(atbl: AnimationTable) =
  var emptyKeys: seq[Displayable] = @[]
  for k, v in atbl.tbl.mpairs:
    if v.len > 0:
      var i = v.high
      while i >= 0:
        if not v[i].done:
          v[i].run()
        if v[i].done:
          v.del i
        i -= 1
    if v.len == 0:
      emptyKeys.add k
  for k in emptyKeys:
    atbl.tbl.del k

proc isBusy*(atbl: AnimationTable): bool =
  result = false
  if atbl.tbl.len > 0:
    for animset in atbl.tbl.values:
      for a in animset:
        result = result or a.blocking
  
