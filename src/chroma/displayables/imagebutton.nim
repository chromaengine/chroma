#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import sdl2
import chroma/displayable
import chroma/displayables/[simpleimage, simpletext]
import chroma/private/[fontcache, imagecache]

type
  ImageButton* = ref object of Displayable
    idleImg, hoverImg, disabledImg: SimpleImage
    label*: SimpleText
    hovered*: bool
    onClick*: proc(self: ImageButton)
    preserveAspectRatio: bool

  ImageButtonState* = object
    ## A serializable object that represents an ImageButton's state.
    ## Used by the loading/saving/rollback system.
    labelState*: SimpleTextState
    xpos*, ypos*, width*, height*: int
    alpha*: int
    idle*, hover*, disabled*: string

proc newImageButton*(renderer: RendererPtr, imageCache: ImageCache, fontCache: FontCache): ImageButton =
  new result
  result.renderer = renderer
  result.idleImg = newSimpleImage(renderer, imageCache)
  result.hoverImg = newSimpleImage(renderer, imageCache)
  result.disabledImg = newSimpleImage(renderer, imageCache)
  result.label = newSimpleText(renderer, fontCache)
  result.label.visible = false
  result.kalpha = 255
  result.enabled = true
  result.visible = true
  result.preserveAspectRatio = true
  result.onEvent = onEventDoNothing
  result.onClick = proc(self: ImageButton) = discard

method `idle=`*(d: ImageButton, path: string) {.base.} =
  d.idleImg.path = path
  d.idleImg.preserveAspectRatio = d.preserveAspectRatio
  d.idleImg.xpos = d.xpos
  d.idleImg.ypos = d.ypos
  d.idleImg.width = d.width
  d.idleImg.height = d.height
  if d.hoverImg.path == "":
    d.hoverImg.path = path
    d.hoverImg.preserveAspectRatio = d.preserveAspectRatio
    d.hoverImg.xpos = d.xpos
    d.hoverImg.ypos = d.ypos
    d.hoverImg.width = d.width
    d.hoverImg.height = d.height
  if d.disabledImg.path == "":
    d.disabledImg.path = path
    d.disabledImg.preserveAspectRatio = d.preserveAspectRatio
    d.disabledImg.xpos = d.xpos
    d.disabledImg.ypos = d.ypos
    d.disabledImg.width = d.width
    d.disabledImg.height = d.height

method `hover=`*(d: ImageButton, path: string) {.base.} =
  d.hoverImg.path = path
  d.hoverImg.preserveAspectRatio = d.preserveAspectRatio
  d.hoverImg.width = d.width
  d.hoverImg.height = d.height
  d.hoverImg.xpos = d.xpos
  d.hoverImg.ypos = d.ypos

method `disabled=`*(d: ImageButton, path: string) {.base.} =
  d.disabledImg.path = path
  d.disabledImg.preserveAspectRatio = d.preserveAspectRatio
  d.disabledImg.width = d.width
  d.disabledImg.height = d.height
  d.disabledImg.xpos = d.xpos
  d.disabledImg.ypos = d.ypos

proc `preserveAspectRatio=`*(d: ImageButton, b: bool) =
  d.preserveAspectRatio = b
  d.idleImg.preserveAspectRatio = b
  d.hoverImg.preserveAspectRatio = b
  d.disabledImg.preserveAspectRatio = b

method event*(d: ImageButton, event: ChromaEvent): bool =
  result = false
  if d.onEvent != nil and d.enabled and (d.tenabled <= event.timestamp):
    if event.kind == eventMouseMotion:
      d.doUpdate = d.hovered != (event in d)
      if d.doUpdate:
        d.printDebug "got motion on ", d.debugName
      d.hovered = event in d
    if event in d and event.kind == eventMouseDown:
      d.printDebug "got click on ", d.debugName
      result = true
      d.onClick(d)
    else:
      result = d.onEvent(d, event)

method `width=`*(d: ImageButton, width: int) =
  d.destRect.w = width.cint
  d.hoverImg.width = width
  d.idleImg.width = width
  d.disabledImg.width = width
  d.label.width = width

method `height=`*(d: ImageButton, height: int) =
  d.destRect.h = height.cint
  d.hoverImg.height = height
  d.idleImg.height = height
  d.disabledImg.height = height
  d.label.height = height
  
method `alpha=`*(d: ImageButton, alpha: uint8) =
  d.kalpha = alpha
  d.hoverImg.alpha = alpha
  d.idleImg.alpha = alpha
  d.disabledImg.alpha = alpha
  d.label.alpha = alpha
  
method `xpos=`*(d: ImageButton, x: int) =
  d.destRect.x = x.cint
  d.hoverImg.xpos = x
  d.idleImg.xpos = x
  d.disabledImg.xpos = x
  d.label.xpos = x
  
method `ypos=`*(d: ImageButton, y: int) =
  d.destRect.y = y.cint
  d.hoverImg.ypos = y
  d.idleImg.ypos = y
  d.disabledImg.ypos = y
  d.label.ypos = y

method `xoffset=`*(d: ImageButton, x: int) =
  d.xoff = x.cint
  d.hoverImg.xoffset = x
  d.idleImg.xoffset = x
  d.disabledImg.xoffset = x
  d.label.xoffset = x
  
method `yoffset=`*(d: ImageButton, y: int) =
  d.yoff = y.cint
  d.hoverImg.yoffset = y
  d.idleImg.yoffset = y
  d.disabledImg.yoffset = y

proc `labelText=`*(d: ImageButton, s: string) =
  d.label.value = s

proc `labelFont=`*(d: ImageButton, s: string) =
  d.label.font = s

proc `labelSize=`*(d: ImageButton, i: int) =
  d.label.size = i

proc `labelFill=`*(d: ImageButton, c: (int, int, int)) =
  d.label.fill = c

proc `labelBold=`*(d: ImageButton, b: bool) =
  d.label.bold = b

proc `labelItalic=`*(d: ImageButton, b: bool) =
  d.label.italic = b

proc `labelUnderline=`*(d: ImageButton, b: bool) =
  d.label.underline = b

proc `labelYoffset=`*(d: ImageButton, i: int) =
  d.label.yoffset = i

proc `labelXoffset=`*(d: ImageButton, i: int) =
  d.label.xoffset = i

proc `labelAlign=`*(d: ImageButton, a: TextAlignment) =
  d.label.align = a

method cull*(d: ImageButton) =
  if not d.visible:
    d.hoverImg.cull()
    d.idleImg.cull()
    d.disabledImg.cull()
    d.label.cull()

method render*(d: ImageButton) =
  if d.visible:
    d.printDebug "rendering ", d.debugName
    if not d.enabled and d.disabledImg.path != "":
      d.disabledImg.render()
    else:
      if d.hovered:
        d.hoverImg.render()
      else:
        d.idleImg.render()
    if d.label.value.len > 0:
      d.label.visible = true
    d.label.render()

method destroy*(d: ImageButton) =
  d.idleImg.destroy()
  d.hoverImg.destroy()
  d.disabledImg.destroy()
  d.label.destroy()

proc getState*(d: ImageButton): ImageButtonState =
  result.labelState = d.label.getState()
  result.xpos = d.xpos
  result.ypos = d.ypos
  result.width = d.width
  result.height = d.height
  result.alpha = d.alpha.int
  result.idle = d.idleImg.path
  result.hover = d.hoverImg.path
  result.disabled = d.disabledImg.path

proc loadState*(d: ImageButton, state: ImageButtonState) =
  d.label.loadState(state.labelState)
  d.idleImg.path = state.idle
  d.hoverImg.path = state.hover
  d.disabledImg.path = state.disabled
  d.xpos = state.xpos
  d.ypos = state.ypos
  d.width = state.width
  d.height = state.height
  d.alpha = state.alpha.uint8
