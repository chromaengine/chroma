#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import sdl2

# these algorithms are derived from the one demonstrated here:
# https://gist.github.com/Gumichan01/332c26f6197a432db91cc4327fcabb1c

proc drawCircle*(
    renderer: var RendererPtr,
    x, y, radius: int,
    c: Color) =
  var
    offsetx = 0
    offsety = radius
    d = radius - 1
    status = 0
  
  var
    rShadow: uint8
    gShadow: uint8
    bShadow: uint8
    aShadow: uint8
  renderer.getDrawColor(rShadow, gShadow, bShadow, aShadow)
  defer: renderer.setDrawColor(rShadow, gShadow, bShadow, aShadow)
  renderer.setDrawColor(c)
  
  while offsety >= offsetx:
    status += renderer.drawPoint((x + offsetx).cint, (y + offsety).cint).int
    status += renderer.drawPoint((x + offsety).cint, (y + offsetx).cint).int
    status += renderer.drawPoint((x - offsetx).cint, (y + offsety).cint).int
    status += renderer.drawPoint((x - offsety).cint, (y + offsetx).cint).int
    status += renderer.drawPoint((x + offsetx).cint, (y - offsety).cint).int
    status += renderer.drawPoint((x + offsety).cint, (y - offsetx).cint).int
    status += renderer.drawPoint((x - offsetx).cint, (y - offsety).cint).int
    status += renderer.drawPoint((x - offsety).cint, (y - offsetx).cint).int
    
    if status < 0: break
    
    if d >= offsetx * 2:
      d -= (2 * offsetx) + 1
      offsetx += 1
    elif d < 2 * (radius - offsety):
      d += (2 * offsety) - 1
      offsety -= 1
    else:
      d += 2 * (offsety - offsetx - 1)
      offsety -= 1
      offsetx += 1


proc drawFilledCircle*(
    renderer: var RendererPtr,
    x, y, radius: int,
    c: Color) =
  var
    offsetx = 0
    offsety = radius
    d = radius - 1
    status = 0
  
  var
    rShadow: uint8
    gShadow: uint8
    bShadow: uint8
    aShadow: uint8
  renderer.getDrawColor(rShadow, gShadow, bShadow, aShadow)
  defer: renderer.setDrawColor(rShadow, gShadow, bShadow, aShadow)
  renderer.setDrawColor(c)
  
  while offsety >= offsetx:
    status += renderer.drawLine(
      (x - offsety).cint, (y + offsetx).cint,
      (x + offsety).cint, (y + offsetx).cint).int
    status += renderer.drawLine(
      (x - offsetx).cint, (y + offsety).cint,
      (x + offsetx).cint, (y + offsety).cint).int
    status += renderer.drawLine(
      (x - offsetx).cint, (y - offsety).cint,
      (x + offsetx).cint, (y - offsety).cint).int
    status += renderer.drawLine(
      (x - offsety).cint, (y - offsetx).cint,
      (x + offsety).cint, (y - offsetx).cint).int
    
    if status < 0: break
    
    if d >= offsetx * 2:
      d -= (2 * offsetx) + 1
      offsetx += 1
    elif d < 2 * (radius - offsety):
      d += (2 * offsety) - 1
      offsety -= 1
    else:
      d += 2 * (offsety - offsetx - 1)
      offsety -= 1
      offsetx += 1
