#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import json, os, random
import chroma/jankcrypt

type
  ContextManager*[T] = ref object
    files: seq[string]
    path: string
    encKey: string

proc randomHexString(nchars: int = 32): string =
  result = newString(nchars)
  for n in 0..result.high:
    var x = rand(15)
    result[n] = char(if x < 10: x + 48 else: x + 55)

proc newContextManager*(path: string, encKey: string): ContextManager =
  new result
  result.files = @[]
  result.path = path
  result.encKey = encKey

proc push*[T](cm: ContextManager, it: T) =
  let fname = randomHexString()
  
