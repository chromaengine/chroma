#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import sdl2, sdl2/image
import tables, times
import chroma/displayable

type
  ImageCache* = ref object
    cache: TableRef[string, CachedImage]
  
  CachedImage* = ref object
    surf: SurfacePtr
    w, h: int
    ninuse: int
    tinuse: uint32
    valid*: bool

proc newImageCache*(): ImageCache
proc newCachedImage(path: string, encryptionKey: string = ""): CachedImage


proc newImageCache*(): ImageCache =
  new result
  result.cache = newTable[string, CachedImage]()

proc newCachedImage(path: string, encryptionKey: string = ""): CachedImage =
  new result
  result.surf = image.load(path)
  if result.surf == nil:
    raise Exception.newException("file does not exist: " & path)
  result.w = result.surf.w
  result.h = result.surf.h
  result.valid = true

proc load*(ic: ImageCache, path: string, encryptionKey: string = ""): CachedImage =
  if path in ic.cache:
    result = ic.cache[path]
  else:
    result = newCachedImage(path, encryptionKey)
    ic.cache[path] = result
    assert result.surf != nil
  result.ninuse += 1
  result.tinuse = gfxTime()

proc destroy(ic: CachedImage) =
  ic.surf.destroy()
  ic.valid = false

proc release*(ic: ImageCache, ci: CachedImage) =
  ci.ninuse -= 1

proc cull*(ic: ImageCache) =
  var keys: seq[string] = @[]
  let curT = gfxTime()
  for k, v in ic.cache:
    echo v.ninuse, '\t', v.tinuse, '\t', k
    if v.ninuse <= 0 or (curT - v.tinuse) > 10000'u32:
      keys.add k
      v.destroy()
  for k in keys:
    ic.cache.del k


proc texture*(ci: CachedImage, renderer: RendererPtr): TexturePtr =
  assert ci != nil
  assert renderer != nil
  result = renderer.createTextureFromSurface(ci.surf)

proc `w`*(ci: CachedImage): int = ci.w
proc `h`*(ci: CachedImage): int = ci.h

proc destroy*(ic: ImageCache) =
  for k, v in ic.cache.pairs:
    v.destroy()
  ic.cache.clear()
