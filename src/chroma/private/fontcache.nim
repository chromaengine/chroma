#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import tables, os
import sdl2, sdl2/ttf

const
  cachedPtrsPerFont = 16

type
  FontVariant = object
    ptrs: OrderedTableRef[int, FontPtr]
    path: string
    hasPath: bool
  
  FontKind* = enum
    fkRegular,
    fkBold,
    fkItalic,
    fkUnderline,
    fkBoldItalic,
    fkBoldUnderline,
    fkItalicUnderline,
    fkBoldItalicUnderline
    
  CacheLine = ref object
    defaultKind: FontKind
    variant: array[FontKind, FontVariant]
  
  FontCache* = ref object
    lines: Table[string, CacheLine]


proc initFontCache*: FontCache =
  echo "creating new font cache"
  new result
  result.lines = initTable[string, CacheLine]()


proc initFontVariant(path: string): FontVariant =
  result = FontVariant(
    path: path,
    hasPath: true,
    ptrs: newOrderedTable[int, FontPtr](initialSize = cachedPtrsPerFont))


proc initFontVariant: FontVariant =
  result = FontVariant(
    hasPath: false,
    ptrs: newOrderedTable[int, FontPtr](initialSize = cachedPtrsPerFont))

proc add*(
    fc: var FontCache,
    name: string,
    default: (FontKind, string),
    variants: varargs[(FontKind, string)]) =
  for k in fc.lines.keys:
    assert k != name
  echo "adding cache line with name ", name
  fc.lines[name] = CacheLine(
    defaultKind: default[0])
  
  assert fileExists(default[1])
  
  fc.lines[name].variant[default[0]] = initFontVariant(default[1])
  
  for v in variants:
    assert fileExists(v[1])
#     echo "adding ", name, " of kind ", $v[0], " with path ", v[1]
    fc.lines[name].variant[v[0]] = initFontVariant(v[1])
  
  for it in fc.lines[name].variant.mitems:
    if not it.hasPath:
      it = initFontVariant()


proc regularFontVariant*(path: string): (FontKind, string) = (fkRegular, path)
proc boldFontVariant*(path: string): (FontKind, string) = (fkBold, path)
proc italicFontVariant*(path: string): (FontKind, string) = (fkItalic, path)
proc boldItalicFontVariant*(path: string): (FontKind, string) = (fkBoldItalic, path)

func bits(fk: FontKind): int =
  case fk:
  of fkRegular:
    TTF_STYLE_NORMAL
  of fkBold:
    TTF_STYLE_BOLD
  of fkItalic:
    TTF_STYLE_ITALIC
  of fkUnderline:
    TTF_STYLE_UNDERLINE
  of fkBoldItalic:
    TTF_STYLE_BOLD or TTF_STYLE_ITALIC
  of fkBoldUnderline:
    TTF_STYLE_BOLD or TTF_STYLE_UNDERLINE
  of fkItalicUnderline:
    TTF_STYLE_ITALIC or TTF_STYLE_UNDERLINE
  of fkBoldItalicUnderline:
    TTF_STYLE_BOLD or TTF_STYLE_ITALIC or TTF_STYLE_UNDERLINE

proc get*(fc: var FontCache, fontName: string, size: int, style = fkRegular): FontPtr =
  assert size >= 0
  var
    fl = fc.lines[fontName]
    f = fl.variant[style]
  
  if f.ptrs.hasKey(size): # cache hit
    result = f.ptrs[size]
    # move the hit member to the top of the cache
    f.ptrs.del size
    f.ptrs[size] = result
  elif f.ptrs.len < cachedPtrsPerFont:  # cache miss with open spot in cache
    result =
      if f.hasPath:
        openFont(f.path, size.cint)
      else:
        openFont(fl.variant[fl.defaultKind].path, size.cint)
    if not f.hasPath:
      # need to manually apply the font style since it wasn't
      # loaded from a TTF that already has that style
      result.setFontStyle style.bits().cint
    f.ptrs[size] = result
  else:  # miss with full cache
    # HACK to get the first key in the table
    var kick = -1
    for k in f.ptrs.keys:
      kick = k
      break
    
    # destroy the least-recently-used cached font
    f.ptrs[kick].close()
    f.ptrs.del kick
    
    result =
      if f.hasPath:
        openFont(f.path, size.cint)
      else:
        openFont(fl.variant[fl.defaultKind].path, size.cint)
    if not f.hasPath:
      # need to manually apply the font style since it wasn't
      # loaded from a TTF that already has that style
      result.setFontStyle style.bits().cint
    f.ptrs[size] = result
  

proc stringWidth*(f: FontPtr, s: string): cint =
  assert f.sizeUtf8(s, addr result, nil) == 0

proc spaceWidth*(f: FontPtr): cint =
  assert f.glyphMetrics(' '.uint16, nil, nil, nil, nil, addr result) == 0


template setUnderline*(fk: var FontKind, b: bool, ifChanged: untyped): untyped =
  ## set whether fk is underline. ifChanged runs if fk changed.
  let tempFk =
    if b: 
      case fk:
      of fkRegular: fkUnderline
      of fkBold: fkBoldUnderline
      of fkItalic: fkItalicUnderline
      of fkBoldItalic: fkBoldItalicUnderline
      else: fk
    else:
      case fk:
      of fkUnderline: fkRegular
      of fkBoldUnderline: fkBold
      of fkItalicUnderline: fkItalic
      of fkBoldItalicUnderline: fkBoldItalic
      else: fk
  if tempFk != fk:
    fk = tempFk
    ifChanged

template setBold*(fk: var FontKind, b: bool, ifChanged: untyped): untyped =
  ## set whether fk is bold. ifChanged runs if fk changed.
  let tempFk =
    if b: 
      case fk:
      of fkRegular: fkBold
      of fkItalic: fkBoldItalic
      of fkUnderline: fkBoldUnderline
      of fkItalicUnderline: fkBoldItalicUnderline
      else: fk
    else:
      case fk:
      of fkBold: fkRegular
      of fkBoldItalic: fkItalic
      of fkBoldUnderline: fkUnderline
      of fkBoldItalicUnderline: fkItalicUnderline
      else: fk
  if tempFk != fk:
    fk = tempFk
    ifChanged

template setItalic*(fk: var FontKind, b: bool, ifChanged: untyped): untyped =
  ## set whether fk is italic. ifChanged runs if fk changed.
  let tempFk =
    if b: 
      case fk:
      of fkRegular: fkItalic
      of fkBold: fkBoldItalic
      of fkUnderline: fkItalicUnderline
      of fkBoldUnderline: fkBoldItalicUnderline
      else: fk
    else:
      case fk:
      of fkItalic: fkRegular
      of fkBoldItalic: fkBold
      of fkItalicUnderline: fkUnderline
      of fkBoldItalicUnderline: fkBoldUnderline
      else: fk
  if tempFk != fk:
    fk = tempFk
    ifChanged
