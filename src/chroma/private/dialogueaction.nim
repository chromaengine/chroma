#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import unicode

type
  DialogueActionKind* = enum
    dakText,
    dakFont,
    dakBold,
    dakItalic,
    dakUnderline,
    dakWait,
    dakColor,
    dakSize,
    dakPause,
    dakRaw
  
  DialogueAction* = ref object
    case kind*: DialogueActionKind
    of dakText:
      tText*: seq[Rune]
    of dakFont:
      fFont*: string
    of dakBold:
      bBold*: bool
    of dakItalic:
      iItalic*: bool
    of dakUnderline:
      uUnderline*: bool
    of dakWait:
      wTime*: float
    of dakSize:
      sSize*: int
    of dakColor:
      cRgb*: (int, int, int)
    of dakPause, dakRaw:
      discard


# creating a new DialogueAction:
# if your action has syntax like this:
#     alex "hello", foo(1)
# then define this proc:
#     fooDialogueAction(num: int): DialogueAction

proc textDialogueAction*(s: string): DialogueAction =
  DialogueAction(kind: dakText, tText: s.toRunes)

proc fontDialogueAction*(fontId: string): DialogueAction =
  DialogueAction(kind: dakFont, fFont: fontId)

proc boldDialogueAction*(value: bool): DialogueAction =
  DialogueAction(kind: dakBold, bBold: value)

proc italicDialogueAction*(value: bool): DialogueAction =
  DialogueAction(kind: dakItalic, iItalic: value)

proc underlineDialogueAction*(value: bool): DialogueAction =
  DialogueAction(kind: dakUnderline, uUnderline: value)

proc colorDialogueAction*(r, g, b: int): DialogueAction =
  DialogueAction(kind: dakColor, cRgb: (r, g, b))

proc waitDialogueAction*(waitTime: float): DialogueAction =
  DialogueAction(kind: dakWait, wTime: waitTime)

proc waitDialogueAction*(waitTime: int): DialogueAction =
  DialogueAction(kind: dakWait, wTime: waitTime.float)

proc sizeDialogueAction*(size: int): DialogueAction =
  DialogueAction(kind: dakSize, sSize: size)

proc pauseDialogueAction*(): DialogueAction =
  DialogueAction(kind: dakPause)
