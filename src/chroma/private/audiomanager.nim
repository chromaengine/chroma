#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import sdl2, sdl2/audio
import tables, hashes, math
import chroma/displayable

type
  AudioManager* = ref object
    defaultSpec: ChromaAudioSpec
  
  AvailableAudioDevice* = ref object of RootObj
    deviceName: string
    isCapture: bool
    manager: AudioManager
  
  AvailableAudioInputDevice* = ref object of AvailableAudioDevice
  AvailableAudioOutputDevice* = ref object of AvailableAudioDevice
  
  ChromaAudioDevice* = ref object of RootObj
    deviceName: string
    isCapture: bool
    spec: ChromaAudioSpec
    manager: AudioManager
    devId: AudioDeviceID
    channels: seq[AudioChannel]
#     queueChan, musicChan, soundChan, voiceChan: AudioChannel
    lastrun: uint32
  
  AudioChannel* = ref object
    mode: AudioChannelMode
    wavDatas*: seq[WavData]
    repeat, paused: bool
    amplitude: range[0..SDL_MIX_MAXVOLUME]
    device: ChromaAudioDevice
  
  AudioChannelMode* = enum
    # replace the currently-playing sound with the new one
    acmReplaceOnLoad,
    # add the sound to a queue
    acmQueue,
    # overlap the sounds
    acmMix
#   
#   AudioChannelState* = ref object
#     
  
  AudioInputDevice* = ref object of ChromaAudioDevice
  AudioOutputDevice* = ref object of ChromaAudioDevice

  ChromaAudioSpec* = audio.AudioSpec
  
  WavData* = ref object of Displayable
    spec: AudioSpec
    buf: ptr uint8
    buflen: uint32
    bufpos: cint
    stream: AudioStreamPtr
    amplitude: range[0..SDL_MIX_MAXVOLUME]
    stopped: bool

const
  queueChannelIndex* = 0
  musicChannelIndex* = 1
  soundChannelIndex* = 2
  voiceChannelIndex* = 3


  


# proc chromaAudioCallback(userdata: pointer, stream: ptr uint8, len: cint) {.cdecl.}


proc hash*(x: AvailableAudioDevice | ChromaAudioDevice): Hash =
  result = 0
  result = result !& hash(x.deviceName)
  result = result !& hash(x.isCapture)

proc `$`*(x: AvailableAudioDevice | ChromaAudioDevice): string = x.deviceName

proc `==`*(x, y: AvailableAudioDevice | ChromaAudioDevice): bool =
  x.hash == y.hash



proc newAudioManager*: AudioManager =
  new result
  result.defaultSpec.freq = 44100
  result.defaultSpec.channels = 2
  result.defaultSpec.format = AUDIO_S16
  result.defaultSpec.samples = 16384

proc newAudioChannel(device: ChromaAudioDevice, kind: AudioChannelMode): AudioChannel =
  new result
  result.mode = kind
  result.wavDatas = @[]
  result.repeat = off
  result.amplitude = SDL_MIX_MAXVOLUME
  result.device = device

proc newAvailableInput(am: AudioManager, deviceName: string): AvailableAudioInputDevice =
  new result
  result.deviceName = deviceName
  result.isCapture = true
  result.manager = am

proc newAvailableOutput(am: AudioManager, deviceName: string): AvailableAudioOutputDevice =
  new result
  result.deviceName = deviceName
  result.isCapture = false
  result.manager = am

iterator availableInputs*(am: AudioManager): AvailableAudioInputDevice =
  let ndevices = getNumAudioDevices(1.cint).int
  for i in 0..(ndevices - 1):
    let name = getAudioDeviceName(i.cint, 1.cint)
    yield newAvailableInput(am, $name)

iterator availableOutputs*(am: AudioManager): AvailableAudioOutputDevice =
  let ndevices = getNumAudioDevices(0.cint).int
  for i in 0..(ndevices - 1):
    let name = getAudioDeviceName(i.cint, 0.cint)
    yield newAvailableOutput(am, $name)


proc getAvailableInputs*(am: AudioManager): seq[AvailableAudioInputDevice] =
  result = @[]
  for a in am.availableInputs:
    result.add a

proc getAvailableOutputs*(am: AudioManager): seq[AvailableAudioOutputDevice] =
  result = @[]
  for a in am.availableOutputs:
    result.add a


proc open*(
    a: AvailableAudioOutputDevice,
    desiredSpec: ChromaAudioSpec): AudioOutputDevice =
  var dspec = deepCopy desiredSpec
  new result
  result.deviceName = a.deviceName
  result.isCapture = a.isCapture
  result.manager = a.manager
  result.channels = @[
    result.newAudioChannel(acmQueue),          # queue
    result.newAudioChannel(acmReplaceOnLoad),  # music
    result.newAudioChannel(acmMix),            # sound
    result.newAudioChannel(acmReplaceOnLoad)]  # voice
  result.devId = openAudioDevice(
    result.deviceName.cstring,
    result.isCapture.cint,
    addr dspec,
    addr result.spec,
    0.cint)
  if result.devId.uint32 <= 0.uint32:
    raise Exception.newException("failed to open audio output device: " & a.deviceName & "(" & $getError() & ")")


proc open*(a: AvailableAudioOutputDevice): AudioOutputDevice =
  a.open(a.manager.defaultSpec)

proc `name`*(a: AvailableAudioDevice | ChromaAudioDevice): string = a.deviceName

method lock*(d: ChromaAudioDevice) {.base.} =
  d.devId.lockAudioDevice()

method unlock*(d: ChromaAudioDevice) {.base.} =
  d.devId.unlockAudioDevice()



proc destroy(wf: WavData) =
  wf.buf.deallocShared()
  wf.stream.destroy()   

proc destroy(ch: AudioChannel) =
  for wd in ch.wavDatas:
    wd.destroy()

template withLock*(d: ChromaAudioDevice, body: untyped) =
  try:
    d.lock()
    body
  finally:
    d.unlock()

method `pause=`*(d: ChromaAudioDevice, b: bool) {.base.} =
  if d != nil:
    d.devId.pauseAudioDevice(if b: 1.cint else: 0.cint)

method close*(d: ChromaAudioDevice) {.base.} =
  d.lock()
  d.devId.closeAudioDevice()

proc getSubBuf(wd: WavData, maxbytes: uint32): tuple[buf: ptr uint8, length: cint] =
  result.buf = nil
  result.length = 0
  if wd.buflen > wd.bufpos.uint32:
    result.length = min(maxbytes.cint, wd.buflen.cint - wd.bufpos.cint)
    result.buf = cast[ptr uint8](cast[uint64](wd.buf) + wd.bufpos.uint64)

# 
# proc chromaAudioCallback(userdata: pointer, stream: ptr uint8, len: cint) {.cdecl.} =
#   assert stream != nil
#   for i in 0..(len - 1):
#     cast[ptr uint8](cast[uint64](stream) + i.uint64)[] = 0
# 
#   case musicChan.mode:
#   of acmQueue, acmMix, acmReplaceOnLoad:
#     for wd in musicChan.wavDatas.mitems:
#       var subBuf = wd.getSubBuf(len)
#       if subBuf.length > 0:
#         mixAudioFormat(stream,
#           subBuf.buf,
#           wd.spec.format,
#           subBuf.length.uint32,
#           SDL_MIX_MAXVOLUME)
#         wd.bufpos += subBuf.length
#         if musicChan.repeat and wd.bufpos >= wd.buflen.cint:
#           wd.bufpos = 0

proc openWavFile*(d: AudioOutputDevice, path: string): WavData =
  new result
  var wavSpec = AudioSpec()
  var wavBuf: ptr uint8
  var wavBufLen: uint32
  echo "openWavFile=", path
  if loadWav(path, addr wavSpec, addr wavBuf, addr wavBufLen).isNil:
    raise Exception.newException("loading " & path & " failed: " & $getError())
  defer: freeWav(wavBuf)
  
  result.stream = newAudioStream(wavSpec, d.spec)
  if result.stream == nil:
    raise Exception.newException("creating audiostream failed: " & $getError())
  
  result.spec = d.spec
  result.bufpos = 0.cint
  result.amplitude = SDL_MIX_MAXVOLUME
  
  if 0 != result.stream.put(wavBuf, wavBufLen.cint):
    raise Exception.newException("streaming failed: " & $getError())
  
  discard result.stream.flush()
  
  result.buflen = result.stream.available().uint32
  result.buf = cast[ptr uint8](allocShared0(result.buflen))
  
  if 0 > result.stream.get(result.buf, result.buflen.cint):
    raise Exception.newException("stream get failed: " & $getError())

proc mixAmplitudes(chan: AudioChannel, wd: WavData): range[0..SDL_MIX_MAXVOLUME] =
  let cw: uint32 = chan.amplitude.uint32 * wd.amplitude.uint32
  result = cw div SDL_MIX_MAXVOLUME

proc runChannel(d: AudioOutputDevice, chan: AudioChannel, buffer: ptr uint8, nbytes: uint32) =
  case chan.mode:
  of acmReplaceOnLoad:
    if chan.wavDatas.len > 0:
      # delete all wavs except the last one added
      while chan.wavDatas.len > 1 or (chan.wavDatas.len > 0 and chan.wavDatas[0].stopped):
        chan.wavDatas[0].destroy()
        chan.wavDatas.delete 0 # must use O(n) delete since order matters in a replaceOnLoad
    
    if chan.wavDatas.len > 0:
      # mix the next buffer chunk
      let sub = chan.wavDatas[0].getSubBuf(nbytes)
      buffer.mixAudioFormat(sub.buf, d.spec.format, sub.length.uint32,
        mixAmplitudes(chan, chan.wavDatas[0]))
      chan.wavDatas[0].bufpos += sub.length
      if chan.wavDatas[0].bufpos >= chan.wavDatas[0].buflen.cint:
        # reached the end of the current wav. repeat or destroy.
        if chan.repeat:
          chan.wavDatas[0].bufpos = 0
        else:
          chan.wavDatas[0].destroy()
          chan.wavDatas.delete 0 # must use O(n) delete since order matters in a replaceOnLoad
  of acmMix:
    var deli = -1
    var i = 0
    
    for wav in chan.wavDatas.mitems:
      if not wav.stopped:
        let sub = wav.getSubBuf(nbytes)
        buffer.mixAudioFormat(sub.buf, d.spec.format, sub.length.uint32,
          mixAmplitudes(chan, wav))
        wav.bufpos += sub.length
      if wav.bufpos >= wav.buflen.cint:
        # reached the end of the current wav. repeat or destroy.
        if chan.repeat:
          wav.bufpos = 0
        else:
          deli = i
      if wav.stopped:
        deli = i
      i += 1
    
    # destroy up to one completed wav item. since runTask runs much more often
    # than the user queues music, this should be sufficient.
    if deli > 0:
      chan.wavDatas[deli].destroy()
      chan.wavDatas.del deli
  of acmQueue:
    if chan.wavDatas.len > 0:
      while chan.wavDatas.len > 0 and chan.wavDatas[0].stopped:
        chan.wavDatas[0].destroy()
        chan.wavDatas.delete 0
    if chan.wavDatas.len > 0:
      # mix the next buffer chunk
      let sub = chan.wavDatas[0].getSubBuf(nbytes)
      buffer.mixAudioFormat(sub.buf, d.spec.format, sub.length.uint32,
        mixAmplitudes(chan, chan.wavDatas[0]))
      chan.wavDatas[0].bufpos += sub.length
      if chan.wavDatas[0].bufpos >= chan.wavDatas[0].buflen.cint:
        # end of current wav. destroy it or repeat
        if chan.repeat:
          chan.wavDatas[0].bufpos = 0
        else:
          chan.wavDatas[0].destroy()
          chan.wavDatas.delete 0 # must use O(n) delete since order matters in a queue

func bytesPerSample(spec: AudioSpec): uint32 =
  spec.channels.uint32 * (SDL_AUDIO_BITSIZE(spec.format.uint32) div 8).uint32

proc runTask*(d: AudioOutputDevice) =
  if d.lastrun == 0:
    d.lastrun = gfxTime()
  else:
    let nmillis = gfxTime() - d.lastrun
    d.lastrun += nmillis
    let nsamples = (nmillis * d.spec.freq.uint32) div 1000
    let bpSample = d.spec.bytesPerSample()
    
    if nsamples > 0'u32:
      let nbytes = (nsamples * bpSample)
      
      var buffer = cast[ptr uint8](allocShared0(nbytes))
      defer: deallocShared(buffer)
      
      for chan in d.channels:
        if not chan.paused:
          d.runChannel(chan, buffer, nbytes)
      
      discard d.devId.queueAudio(buffer, nbytes)


# procs used by the macros

proc getChannel*(d: ChromaAudioDevice, i: int): AudioChannel =
  if d != nil: d.channels[i] else: nil

proc loadWav*(chan: AudioChannel, path: string): WavData =
  if chan != nil:
    result = AudioOutputDevice(chan.device).openWavFile(path)
    chan.wavDatas.add result

proc `pause=`*(chan: AudioChannel, b: bool) =
  if chan != nil:
    chan.paused = b


func logify(value: float): range[0.0..100.0] =
  max(0.0,
  min(100.0,
    100.0 - (50.0 * log10(101 - value))))

func unlogify(value: float): range[0.0..100.0] =
  max(0.0,
  min(100.0,
    101 - pow(10.0, (100.0 - value) / 50.0)))

proc `volume=`*(chan: AudioChannel | WavData, level: range[0..100]) =
  if chan != nil:
    let pct = logify(level.float) / 100.0
    let conv = round(pct * SDL_MIX_MAXVOLUME.float).int
    let ampl: range[0..SDL_MIX_MAXVOLUME] =
      if conv > SDL_MIX_MAXVOLUME: SDL_MIX_MAXVOLUME
      elif conv < 0: 0
      else: conv
    chan.amplitude = ampl


proc `volume`*(chan: AudioChannel | WavData): range[0..100] =
  let ampl = chan.amplitude
  let conv = round(ampl.float * 100.0 / SDL_MIX_MAXVOLUME.float).int
  result = round(unlogify(
    if conv > 100: 100
    elif conv < 0: 0
    else: conv)).int


proc stop*(wd: WavData) =
  wd.stopped = true
