#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import chroma, json, marshal, os, streams, times, tables, strutils, sequtils, random, md5
import chroma/displayable, chroma/displayables/[characterd, backgrounddisp, dialoguedisp]
import chroma/jankcrypt


proc mergeJson(pri: JsonNode, sec: JsonNode): JsonNode =
  result = newJObject()
  for k, v in pairs(sec):
    result{k} = v
  for k, v in pairs(pri):
    result{k} = v

proc resolveDeletedPersistents(internal: ChromaInternal, jn: JsonNode) =
  # removes stuff from jn that's not actually associated with a variable
  # in the declaration of GameVar. Keep removing stuff until the deserialization
  # works. TODO: find a better way to do this.
  while true:
    try:
      internal.gamevar.persistent = to[Persistent]($jn)
      break
    except ValueError:
      let
        msg = getCurrentExceptionMsg()
        spl = msg.split("invalid field name: ")
      if spl.len != 2:
        echo msg
        raise newException(ValueError, msg)
      else:
        echo "removing persistent ", spl[1]
        jn.delete spl[1]

proc loadPersistents*(internal: ChromaInternal[GameVar]) =
  if not fileExists internal.persistsFile:
    writeFile(internal.persistsFile, jankEncrypt(internal.encKey, "{\n}"))
  let f = parseJson jankDecryptFile(internal.encKey, internal.persistsFile)
  let o = parseJson $$internal.gamevar.persistent
  let merged = mergeJson(f, o)
  resolveDeletedPersistents(internal, merged)
  loadDefaultPersistents(internal, f)


proc loadGameConf*[T](internal: ChromaInternal[T]) =
  if not fileExists("conf.json"):
    var outStr: string = ""
    toUgly outStr: %*{
      "audio": {
        "queue": 100,
        "voice": 100,
        "music": 100,
        "sound": 100
      },
      "fullscreen": false,
      "savedir": "saves.json"
    }
    writeFile "conf.json", outStr
  let jn = parseFile "conf.json"
  
  internal.audioDevice.getChannel(queueChannelIndex).volume = jn["audio"]["queue"].getInt
  internal.audioDevice.getChannel(voiceChannelIndex).volume = jn["audio"]["voice"].getInt
  internal.audioDevice.getChannel(musicChannelIndex).volume = jn["audio"]["music"].getInt
  internal.audioDevice.getChannel(soundChannelIndex).volume = jn["audio"]["sound"].getInt
  if jn["fullscreen"].getBool:
    internal.enableFullScreen()
  internal.saveFile = jn["savedir"].getStr


proc saveGameConf*[T](internal: ChromaInternal[T]) =
  var outStr: string = ""
  toUgly outStr: %*{
    "audio": {
      "queue": internal.audioDevice.getChannel(queueChannelIndex).volume,
      "voice": internal.audioDevice.getChannel(voiceChannelIndex).volume,
      "music": internal.audioDevice.getChannel(musicChannelIndex).volume,
      "sound": internal.audioDevice.getChannel(soundChannelIndex).volume
    },
    "fullscreen": internal.fullScreenEnabled,
    "savedir": "saves.json"
  }
  writeFile "conf.json", outStr


proc resolveDeletedGameVars(gv: var GameVar, jn: JsonNode) =
  # removes stuff from jn that's not actually associated with a variable
  # in the declaration of GameVar. Keep removing stuff until the deserialization
  # works. TODO: find a better way to do this.
  while true:
    try:
      gv = to[GameVar]($jn)
      break
    except ValueError:
      let
        msg = getCurrentExceptionMsg()
        spl = msg.split("invalid field name: ")
      if spl.len != 2:
        echo msg
        raise newException(ValueError, msg)
      else:
        echo "removing gamevar ", spl[1]
        jn.delete spl[1]


proc saveGame*(internal: ChromaInternal[GameVar]) =
  let context: seq[GameContext[GameVar]] =
    if internal.savedContexts.len > 0:
      internal.savedContexts
    else:
      var gc = GameContext[GameVar](
        counter: internal.counter,
        characters: internal.chrStates(),
        gamevar: deepCopy(internal.gamevar),
        dialogues: @[],
        background: internal.backgrounddisp.getState())
      for k, v in internal.dialoguedisp.pairs:
        gc.dialogues.add (k, v.getState())
      @[gc]
  let
    gameJson = parseJson $$context
    persistJson = parseJson $$internal.gamevar.persistent
  writeFile(internal.gameFile, jankEncrypt(internal.encKey, gameJson.pretty()))
  writeFile(internal.persistsFile, jankEncrypt(internal.encKey, persistJson.pretty()))

proc loadContext(internal: ChromaInternal[GameVar]) =
#   if not fileExists internal.gameFile:
#     let node = %*[[0, {
#       "gamevar": {},
#       "characters": @[],
#       "counter": script_label_start,
#       "dialogues": @[],
#       "background": defaultBackgroundState()
#     }]]
#     writeFile(internal.gameFile, jankEncrypt(internal.encKey, node.pretty()))
  let jn = parseJson jankDecryptFile(internal.encKey, internal.gameFile)
  let gvnNode = parseJson $$internal.gamevar
  assert jn.kind == JArray
  internal.savedContexts = @[]
  for node in jn:
    let n = node[1]
    let
      gvsNode = n["gamevar"]
      chNode = n["characters"]
      gcNode = n["counter"]
      ddNode = n["dialogues"]
      bgNode = n["background"]
    
    var gv: GameVar
    
    # merge saved gamevars with the new data structure. This allows devs to add
    # new gamevars, delete others, etc and still load an old save file.
    if gvsNode.hasKey "persistent":
      gvsNode.delete "persistent"
    if gvnNode.hasKey "persistent":
      gvnNode.delete "persistent"
    let gvNode = mergeJson(gvsNode, gvnNode)
    resolveDeletedGameVars(gv, gvNode)
    loadDefaultGamevars(gv, gvsNode) # generated by gamevars macro
    
    # load character states
    let chrstates = to[seq[CharacterState]]($chNode)
    
    # load dialogue displayables states
    let dialogues = to[seq[(string, DialogueDispState)]]($ddNode)
    for (k, v) in dialogues:
      internal.dialoguedisp[k].loadState(v)
    
    let bgstate = to[BackgroundState]($bgNode)
    
    internal.savedContexts.add GameContext[GameVar](
      counter: to[int]($gcNode),
      characters: chrstates,
      gamevar: gv,
      dialogues: dialogues,
      background: bgstate)
    echo $gv
#   internal.popContext()

proc toJson(sd: SaveData): JsonNode =
  %*{
    "timestamp": sd.timestamp.format("yyyy-MM-dd HH:mm:ss"),
    "name": sd.name,
    "path": sd.path
  }

proc timestampStr*(sd: SaveData): string =
  sd.timestamp.format("MMMM dd, yyyy 'at' h:mmtt")

proc fromJson(jn: JsonNode): SaveData =
  assert jn.kind == JObject
  result = SaveData(
    timestamp: parse(jn["timestamp"].getStr, "yyyy-MM-dd HH:mm:ss"),
    name: jn["name"].getStr,
    path: jn["path"].getStr)

proc loadSaves*(internal: ChromaInternal[Gamevar]) =
  if not fileExists(internal.saveFile):
    writeFile(internal.saveFile, "[]")
  let jnode = parseFile internal.saveFile
  assert jnode.kind == JArray
  internal.saves = @[]
  for jn in jnode:
    internal.saves.add(jn.fromJson)

proc newSave(internal: ChromaInternal[GameVar], name: string, path: string) =
  internal.saves.add SaveData(
    name: name,
    path: path,
    timestamp: now())
  var outjn = newJArray()
  for s in internal.saves:
    outjn.add s.toJson
  writeFile(internal.saveFile, outjn.pretty())

proc newSave(internal: ChromaInternal[GameVar], name: string): string =
  let r: int = rand(0x7FFFFFFF)
  let path = getMd5(name & $r)
  result = "saves" / path & ".json"
  newSave(internal, name, result)

proc startNewGame*(internal: ChromaInternal[Gamevar], jumpto: int, name: string) =
  internal.gameFile = internal.newSave(name)
  if not fileExists internal.gameFile:
    let node = %*[[0, {
      "gamevar": {},
      "characters": @[],
      "counter": jumpto,
      "dialogues": @[],
      "background": defaultBackgroundState()
    }]]
    writeFile(internal.gameFile, jankEncrypt(internal.encKey, node.pretty()))
  loadContext(internal)
  internal.counter = jumpto
  internal.pause = false
  internal.popContext()

proc loadGame*(internal: ChromaInternal[Gamevar], sd: SaveData) =
  assert fileExists(sd.path)
  internal.gameFile = sd.path
  loadContext(internal)
  internal.pause = false
  internal.popContext()

proc forkGame*(internal: ChromaInternal[Gamevar], name: string) =
  internal.gameFile = internal.newSave(name)
  saveGame(internal)

proc overwriteGame*(internal: ChromaInternal[Gamevar], sd: SaveData) =
  internal.gameFile = sd.path
  saveGame(internal)

proc returnToStart*(internal: ChromaInternal[Gamevar]) =
  internal.gameFile = ""
  internal.savedContexts = @[]
  internal.counter = script_label_start
  internal.pause = false
  internal.autoplayEnabled = true
