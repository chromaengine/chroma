#
# The Chroma Visual Novel Engine
# (c) Copyright 2020 Evan Grove
#
# This file is licensed under the 3-clause BSD license.
# See the file "LICENSE," included in this distribution,
# for details about the copyright.
#

import macros, strutils, tables, terminal

proc convTree*(internalIdent: NimNode, ast: NimNode): NimNode

proc convTree*(ast: NimNode): NimNode = convTree(ident("internal"), ast)

# the prefixer algorithm will ignore any idents in this list.
const reservedKeywords* = [
  "false",
  "true",
  "on",
  "off",
  "len",
  "max",
  "min",
  "assert",
  "nonblocking",
  "blocking",
  "dialogue",
  "background",
  "bg",
  "all",
  "any",
  "result",
  "gamevars",
  "font",
  "ui",
  "play",
  "pause",
  "resume",
  "stop"
]

# built-in commands in the script language, and any keywords that we want
# to reserve for future use in the script language.
const reservedCommands* = [
  "show",
  "label",
  "jump",
  "raw",
  "assert",
  "code",
  "ret",
  "menu",
  "background",
  "bg",
  "pause"
]

var characterNames* {.compileTime.}: seq[string] = @[]
var fontNames* {.compileTime.}: seq[string] = @[]
var gamevarNames* {.compileTime.}: seq[string] = @[]
var persistentNames* {.compileTime.}: seq[string] = @[]
var dialogueDispNames* {.compileTime.}: seq[string] = @[]
var backgroundNames* {.compileTime.}: seq[string] = @[]
var guiElementNames* {.compileTime.}: seq[string] = @[]
var chromaFunctions* {.compileTime.}: seq[string] = @[]
var guiTypeData* {.compileTime.}: seq[tuple[
  name: string,
  argTyIdent: NimNode,
  objTyIdent: NimNode]] = @[]

# used to track attribute names for each character, and they layer they are associated with.
# ex: assert characterAttrs["alice"]["happy"] == "face"
var characterAttrs* {.compileTime.}: TableRef[string, TableRef[string, string]] =
  newTable[string, TableRef[string, string]]()

# prefixes an ident with internal.gamevar
# proc applyGamevarPrefix*(ast: NimNode): NimNode =
#   assert ast.kind == nnkIdent
#   if ast.strVal in reservedKeywords or
#      ast.strVal in characterNames or
#      not validIdentifier(ast.strVal):
#     result = ast
#   elif ast.strVal in fontNames:
#     result = newLit(ast.strVal)
#   else:
#     result =
#       nnkDotExpr.newTree(
#         nnkDotExpr.newTree(
#           newIdentNode("internal"),
#           newIdentNode("gamevar")),
#         ast)

# traverses an AST, replacing all appropriate idents with internal.gamevar
# proc prefixGamevarIdents*(ast: NimNode): NimNode =
#   case ast.kind:
#   of nnkIdent:
#     result = applyGamevarPrefix(ast)
#   of nnkPar, nnkCommand, nnkBracket, nnkBracketExpr, nnkIfExpr, nnkIfStmt,
#      nnkElifExpr, nnkElifBranch, nnkElseExpr, nnkStmtList, nnkPrefix:
#     result = ast.kind.newTree()
#     for a in ast:
#       result.add prefixGamevarIdents(a)
#   of nnkCall:
#     if ast[0].kind == nnkIdent and ast[0].strVal == "raw":
#       result = ast[1]
#     else:
#       result = ast.kind.newTree()
#       for a in ast:
#         result.add prefixGamevarIdents(a)
#   of nnkInfix:
#     result = ast.kind.newTree()
#     result.add ast[0]
#     result.add prefixGamevarIdents(ast[1])
#     result.add prefixGamevarIdents(ast[2])
#   of nnkAsgn:
#     result = ast.kind.newTree()
#     result.add prefixGamevarIdents(ast[0])
#     result.add prefixGamevarIdents(ast[1])
#   of nnkDotExpr:
#     result = ast.kind.newTree()
#     result.add prefixGamevarIdents(ast[0])
#     result.add ast[1]
#   of nnkExprEqExpr:
#     result = ast.kind.newTree()
#     result.add ast[0]
#     result.add prefixGamevarIdents(ast[1])
#   else:
#     result = ast


proc collapseTree*(result: var NimNode, prefix: NimNode, ast: NimNode) =
  ## Collapses a tree of calls into dot expressions. This works recursively.
  ##
  ## This:
  ## .. code-block:: nim
  ##   foo = 3
  ##   bar:
  ##     fizz = "hello"
  ##     buzz = @[1, 2, 3]
  ##   fancy "lorem ipsum", 2338:
  ##     schmancy = 4
  ##
  ## Gets turned into this:
  ## .. code-block:: nim
  ##   prefix.foo = 3
  ##   prefix.bar.fizz = "hello"
  ##   prefix.bar.buzz = @[1, 2, 3]
  ##   prefix.fancy = ("lorem ipsum", 2338)
  ##   prefix.schmancy = 4
  assert result.kind == nnkStmtList
  if ast.kind == nnkCall and ast.len == 2 and ast[1].kind == nnkStmtList:
    let memb = ast[0]
    let newPrefix = nnkDotExpr.newTree(prefix, memb)
    for a in ast[1]:
      collapseTree(result, newPrefix, a)
  elif ast.kind == nnkCommand and ast.len >= 3 and ast[^1].kind == nnkStmtList:
    let memb = ast[0]
    let value = nnkPar.newTree()
    let newPrefix = nnkDotExpr.newTree(prefix, memb)
    for a in ast[1..^2]:
      value.add a
    result.add nnkAsgn.newTree(newPrefix, value)
    for a in ast[^1]:
      collapseTree(result, newPrefix, a)
  elif ast.kind == nnkAsgn:
    let memb = ast[0]
    let value = ast[1]
    result.add nnkAsgn.newTree(
      nnkDotExpr.newTree(prefix, memb),
      value)
  elif ast.kind == nnkDiscardStmt:
    discard
  else:
    echo ast.treeRepr
    error "invalid syntax", ast


proc processCallShowArg*(result: var NimNode, displayableNode, nameNode, a: NimNode) =
  # create a call to a ShowArg proc, and add it to the stmtlist result.
  if a[0].kind == nnkIdent and a[0].strVal == "raw":
    result.add a[1]
  else:
    # ShowArg(kind: <a[0]>ShowArg(`displayableNode`, `nameNode`, ...))
    var call = nnkCall.newTree(
      newIdentNode(a[0].strVal & "ShowArg"),
      displayableNode,
      nameNode)
    for aa in a[1..^1]:
      call.add convTree(ident("internal"), aa)
    result.add call

proc processCallPlayArg*(result: var NimNode, displayableNode, nameNode, a: NimNode) =
  # create a call to a PlayArg proc, and add it to the stmtlist result.
  if a[0].kind == nnkIdent and a[0].strVal == "raw":
    result.add a[1]
  else:
    # PlayArg(kind: <a[0]>PlayArg(`displayableNode`, `nameNode`, ...))
    var call = nnkCall.newTree(
      newIdentNode(a[0].strVal & "PlayArg"),
      displayableNode,
      nameNode)
    for aa in a[1..^1]:
      call.add convTree(ident("internal"), aa)
    result.add call

proc humanFriendlyName*(ast: NimNode, verbose = true): string =
  if ast.kind == nnkNone:
    "(none)"
  elif ast.kind == nnkEmpty:
    "(empty)"
  elif ast.kind == nnkStmtList:
    "list of statements"
  elif ast.kind in {nnkIdent, nnkSym}:
    if verbose: "identifier '" & ast.toStrLit.strVal & "'" 
    else: "identifier"
  elif ast.kind ==  nnkType:
    if verbose: "type '" & ast.toStrLit.strVal & "'" 
    else: "type"
  elif ast.kind ==  nnkCharLit:
    if verbose: "character '" & ast.toStrLit.strVal & "'" 
    else: "character"
  elif ast.kind in {nnkIntLit, nnkInt8Lit, nnkInt16Lit, nnkInt32Lit, nnkInt64Lit}:
    if verbose: "integer '" & ast.toStrLit.strVal & "'" 
    else: "integer"
  elif ast.kind in {nnkUIntLit, nnkUInt8Lit, nnkUInt16Lit, nnkUInt32Lit, nnkUInt64Lit}:
    if verbose: "unsigned integer '" & ast.toStrLit.strVal & "'" 
    else: "unsigned integer"
  elif ast.kind in {nnkFloatLit, nnkFloat32Lit, nnkFloat64Lit, nnkFloat128Lit}:
    if verbose: "float '" & ast.toStrLit.strVal & "'" 
    else: "float"
  elif ast.kind in {nnkStrLit, nnkRStrLit, nnkTripleStrLit}:
    if verbose: "string " & ast.toStrLit.strVal
    else: "string"
  elif ast.kind == nnkNilLit:
    "nil" 
  elif ast.kind in CallNodes:
    if verbose: "call to '" & ast[0].strVal & "'"
    else: "call"
  elif ast.kind in RoutineNodes:
    case ast.kind:
    of nnkProcDef: "proc"
    of nnkFuncDef: "func"
    of nnkMethodDef: "method"
    of nnkDo: "do"
    of nnkLambda: "lambda"
    of nnkIteratorDef: "iterator"
    of nnkTemplateDef: "template"
    of nnkConverterDef: "converter"
    else: ""
  elif ast.kind == nnkAsgn:
    if verbose: "assignment to '" & ast[0].toStrLit.strVal & "'"
    else: "assignment"
  elif ast.kind in {nnkIfExpr, nnkIfStmt}:
    "if"
  elif ast.kind in {nnkElifExpr, nnkElifBranch}:
    "elif"
  elif ast.kind in {nnkElse, nnkElseExpr}:
    "else"
  elif ast.kind in {nnkForStmt, nnkParForStmt}:
    "for"
  elif ast.kind == nnkExprColonExpr:
    "arg definition"
  else:
    ast.toStrLit.strVal

proc humanFriendlyName*(ast: NimNodeKind): string =
  if ast == nnkNone:
    "(none)"
  elif ast == nnkEmpty:
    "(empty)"
  elif ast == nnkStmtList:
    "list of statements"
  elif ast in {nnkIdent, nnkSym}:
    "identifier"
  elif ast ==  nnkType:
    "type"
  elif ast ==  nnkCharLit:
    "character"
  elif ast in {nnkIntLit, nnkInt8Lit, nnkInt16Lit, nnkInt32Lit, nnkInt64Lit}:
    "integer"
  elif ast in {nnkUIntLit, nnkUInt8Lit, nnkUInt16Lit, nnkUInt32Lit, nnkUInt64Lit}:
    "unsigned integer"
  elif ast in {nnkFloatLit, nnkFloat32Lit, nnkFloat64Lit, nnkFloat128Lit}:
    "float"
  elif ast in {nnkStrLit, nnkRStrLit, nnkTripleStrLit}:
    "string"
  elif ast == nnkNilLit:
    "nil" 
  elif ast in CallNodes:
    "call"
  elif ast in RoutineNodes:
    case ast:
    of nnkProcDef: "proc"
    of nnkFuncDef: "func"
    of nnkMethodDef: "method"
    of nnkDo: "do"
    of nnkLambda: "lambda"
    of nnkIteratorDef: "iterator"
    of nnkTemplateDef: "template"
    of nnkConverterDef: "converter"
    else: ""
  elif ast == nnkAsgn:
    "assignment"
  elif ast in {nnkIfExpr, nnkIfStmt}:
    "if"
  elif ast in {nnkElifExpr, nnkElifBranch}:
    "elif"
  elif ast in {nnkElse, nnkElseExpr}:
    "else"
  elif ast in {nnkForStmt, nnkParForStmt}:
    "for"
  elif ast == nnkExprColonExpr:
    "arg definition"
  else:
    $ast

proc errorGotExpected*(got, expected: NimNode) =
  error " expected " & humanFriendlyName(expected, verbose=false) & ", got: " & humanFriendlyName(got), got

proc errorGotExpected*(got: NimNode, expected: NimNodeKind) =
  error " expected " & humanFriendlyName(expected) & ", got: " & humanFriendlyName(got), got

proc expect*(got: NimNode, expect: NimNodeKind) =
  if got.kind != expect:
    errorGotExpected(got, expect)

proc exprColonExprToIdentDef*(ast: NimNode): NimNode =
  assert ast.kind == nnkExprColonExpr
  result = nnkIdentDefs.newTree(ast[0], ast[1], newEmptyNode())

template combineSeqs*[T](items: varargs[seq[T]]): untyped =
  var slen = 0
  for it in items:
    slen += it.len
  var sq = newSeq[T](slen)
  var n = 0
  for it in items:
    for i in it:
      sq[n] = i
      n += 1
  sq

proc makeSeqOfThings*(ast: NimNode | seq[NimNode]): NimNode =
  result = nnkBracket.newTree()
  for a in ast:
    result.add a
  result = nnkPrefix.newTree(
    ident("@"),
    result)

proc makeSeqOfShowArg*(ast: NimNode | seq[NimNode],
    displayableIdent, nameStrLit: NimNode): NimNode =
  result = nnkBracket.newTree()
  for a in ast:
    if a.kind != nnkCall or a[0].kind != nnkIdent:
      error "not an animation argument: " & a.toStrLit.strVal, a
    else:
      var call = nnkCall.newTree()
      call.add ident(a[0].strVal & "ShowArg")
      call.add displayableIdent
      call.add nameStrLit
      for aa in a[1..^1]:
        call.add aa
      result.add call
  result = nnkPrefix.newTree(
    ident("@"),
    result)

proc makeSeqOfPlayArg*(ast: NimNode | seq[NimNode],
    channelIdent, nameStrLit: NimNode): NimNode =
#     `chanRef`.wavDatas.mapIt(fadeOutPlayArg(it, `chanName`))
  result = nnkCall.newTree()
  result.add ident("combineSeqs")
  for a in ast:
    if a.kind != nnkCall or a[0].kind != nnkIdent:
      error "not an animation argument: " & a.toStrLit.strVal, a
    else:
      var call = nnkCall.newTree()
      call.add ident(a[0].strVal & "PlayArg")
      call.add ident("it")
      call.add nameStrLit
      echo ast.treeRepr
      for aa in a[1..^1]:
        echo aa.treeRepr
        call.add aa
      result.add quote do:
        `channelIdent`.wavDatas.mapIt(`call`)

proc makeSeqOfPlayArg2*(ast: NimNode | seq[NimNode],
    wavIdent, nameStrLit: NimNode): NimNode =
#     fadeOutPlayArg(`wavIdent`, `chanName`)
  result = nnkBracket.newTree()
  for a in ast:
    if a.kind != nnkCall or a[0].kind != nnkIdent:
      error "not an animation argument: " & a.toStrLit.strVal, a
    else:
      var call = nnkCall.newTree()
      call.add ident(a[0].strVal & "PlayArg")
      call.add wavIdent
      call.add nameStrLit
      for aa in a[1..^1]:
        call.add aa
      result.add call
  result = nnkPrefix.newTree(
    ident("@"),
    result)

proc findFirstPrefix*(s: var string, ast: NimNode): bool =
  case ast.kind:
  of nnkCall, nnkCommand, nnkDotExpr, nnkBracketExpr, nnkCurlyExpr, nnkAsgn:
    findFirstPrefix(s, ast[0])
  of nnkInfix:
    findFirstPrefix(s, ast[1])
  of nnkIdent:
    s = ast.strVal
    true
  else:
    false

proc findSecondPrefix*(s: var string, ast: NimNode): bool =
  case ast.kind:
  of nnkCall, nnkCommand, nnkBracketExpr, nnkCurlyExpr, nnkAsgn:
    findSecondPrefix(s, ast[0])
  of nnkInfix:
    findSecondPrefix(s, ast[1])
  of nnkDotExpr:
    if ast[0].kind == nnkDotExpr:
      findSecondPrefix(s, ast[0])
    else:
      s = ast[1].strVal
      true
  else:
    false

proc findLastPrefix*(s: var string, ast: NimNode): bool =
  case ast.kind:
  of nnkCall, nnkCommand:
    findLastPrefix(s, ast[0])
  of nnkDotExpr:
    s = ast[1].strVal
    true
  else:
    echo "!!!!\n", ast.toStrLit
    false

proc replaceDotExpr*(replacement, ast: NimNode): NimNode =
  case ast.kind:
  of nnkCall, nnkCommand, nnkBracketExpr, nnkCurlyExpr, nnkAsgn:
    result = ast.kind.newTree(replaceDotExpr(replacement, ast[0]))
    for a in ast[1..^1]:
      result.add a
  of nnkDotExpr:
    if ast[0].kind == nnkDotExpr and ast[0][0].kind == nnkIdent:
      result = nnkDotExpr.newTree(replacement)
      for a in ast[1..^1]:
        result.add a
    elif ast[0].kind == nnkIdent:
      result = replacement
    else:
      result = ast.kind.newTree(replaceDotExpr(replacement, ast[0]))
      for a in ast[1..^1]:
        result.add a
  of nnkInfix:
    result = nnkInfix.newTree(ast[0])
    if ast[1].kind == nnkDotExpr and ast[1][0].kind == nnkIdent:
      result.add replacement
    else:
      result.add replaceDotExpr(replacement, ast[1])
    for a in ast[2..^1]:
      result.add a
  else:
    assert false

proc isGameCall*(callId: var string, ast: NimNode): bool =
  result = false
  if findFirstPrefix(callId, ast):
    result = callId in ["ui", "audio", "character", "gamevar", "font", "persistent", "dialogue"]

proc gameCallConv*(callId: string, internalIdent, ast: NimNode): NimNode =
  case callId:
  of "ui":
    var uiName: string = ""
    assert findSecondPrefix(uiName, ast)
#     if uiName notin guiElementNames:
#       error "No UI element with name '" & uiName & "'", ast
    let tyIdent = ident("GameUiRoot_" & uiName)
    let replacement = quote do:
      `tyIdent`(`internalIdent`.guiElements[`uiName`])
    result = replaceDotExpr(replacement, ast)
  of "audio":
    var chanName: string = ""
    assert findSecondPrefix(chanName, ast)
    if chanName notin ["sound", "music", "voice", "queue"]:
      error "No audio channel with name '" & chanName & "'", ast
    let chanIndexIdent = ident(chanName & "ChannelIndex")
    let replacement = quote do:
      `internalIdent`.audioDevice.getChannel(`chanIndexIdent`)
    result = replaceDotExpr(replacement, ast)
  of "dialogue":
    var dName: string = ""
    assert findSecondPrefix(dName, ast)
#     if chanName notin ["sound", "music", "voice", "queue"]:
#       error "No audio channel with name '" & chanName & "'", ast
    let dIdent = newLit(dName)
    let replacement = quote do:
      `internalIdent`.dialoguedisp[`dIdent`]
    result = replaceDotExpr(replacement, ast)
  of "character":
    var chrName: string = ""
    assert findSecondPrefix(chrName, ast)
    if chrName notin characterNames:
      error "No character with name '" & chrName & "'", ast
    let replacement = quote do:
      `internalIdent`.character[`chrName`]
    result = replaceDotExpr(replacement, ast)
  of "gamevar":
    var gvName: string = ""
    assert findSecondPrefix(gvName, ast)
    let gvIdent = ident(gvName)
    let replacement = quote do:
      `internalIdent`.gamevar.`gvIdent`
    result = replaceDotExpr(replacement, ast)
  of "persistent":
    var pName: string = ""
    assert findSecondPrefix(pName, ast)
    let pIdent = ident(pName)
    let replacement = quote do:
      `internalIdent`.gamevar.persistent.`pIdent`
    result = replaceDotExpr(replacement, ast)
  of "font":
    var fName: string = ""
    assert findSecondPrefix(fName, ast)
    if fName notin fontNames:
      error "No font with name '" & fName & "'", ast
    let replacement = quote do:
      `fName`
    result = replaceDotExpr(replacement, ast)
  else:
    assert false



proc convTree*(
    internalIdent: NimNode,
    ast: NimNode): NimNode =
  case ast.kind:
  of nnkStmtList, nnkPar, nnkBracket, nnkAsgn, # process all children
     nnkVarTuple, nnkIfStmt, nnkIfExpr, nnkElifBranch,
     nnkElifExpr, nnkElse, nnkElseExpr, nnkForStmt,
     nnkTryStmt, nnkExceptBranch, nnkFinally, nnkLetSection,
     nnkVarSection, nnkIdentDefs, nnkCaseStmt, nnkOfBranch,
     nnkReturnStmt, nnkCurly:
    result = ast.kind.newTree()
    for a in ast:
      result.add convTree(internalIdent, a)
  of nnkInfix, nnkPrefix, nnkExprColonExpr, nnkExprEqExpr: # preserve first child, process rest
    result = ast.kind.newTree(ast[0])
    for a in ast[1..^1]:
      var idstr: string = ""
      if isGameCall(idstr, a):
        result.add gameCallConv(idstr, internalIdent, a)
      else:
        result.add convTree(internalIdent, a)
  of nnkLambda: # preserve all but the last child, process last
    result = ast.kind.newTree()
    for a in ast[0..^2]:
      result.add a
    result.add convTree(internalIdent, ast[^1])
  of nnkCall, nnkCommand, nnkBracketExpr: # complex processing for ui and audio calls
    result = ast.kind.newTree()
    var idstr = ""
    if isGameCall(idstr, ast): # if this is a call into the game
      case idstr:
      of "ui":  # process UI call
        result.add gameCallConv(idstr, internalIdent, ast[0])
        var callId = ""
        var uiName = ""
        assert findLastPrefix(callId, ast[0])
        assert findSecondPrefix(uiName, ast[0])
        case callId:
        of "show", "hide", "hideDontDisable", "refresh":
          result.add internalIdent
          let dispRef = gameCallConv(idstr, internalIdent, ast[0][0])
          if ast[^1].kind == nnkStmtList:
            # has a list of animations. make the sequence.
            result.add makeSeqOfShowArg(ast[^1], dispRef, newLit(uiName))
            for a in ast[1..^2]:
              result.add convTree(internalIdent, a)
          else:
            # no animation list. put an empty one.
            result.add quote do: seq[ShowArg](@[])
            for a in ast[1..^1]:
              result.add convTree(internalIdent, a)
        else:  # not a special call to ui.xxx, process normally
          for a in ast[1..^1]:
            result.add convTree(internalIdent, a)
      of "audio":  # process audio call
        result.add gameCallConv(idstr, internalIdent, ast[0])
        var callId = ""
        var chanName = ""
        assert findLastPrefix(callId, ast[0])
        assert findSecondPrefix(chanName, ast[0])
        case callId:
        of "stop":
          result.add internalIdent
          let chanRef = gameCallConv(idstr, internalIdent, ast[0][0])
          if ast[^1].kind == nnkStmtList:
            # has a list of animations. make the sequence.
            result.add makeSeqOfPlayArg(ast[^1], chanRef, newLit(chanName))
            for a in ast[1..^2]:
              result.add convTree(internalIdent, a)
          else:
            # no animation list. put an empty one.
            result.add quote do: @[]
            for a in ast[1..^1]:
              result.add convTree(internalIdent, a)
        of "play":
          if ast.len < 2:
            error "not enough arguments for play(<filename>)", ast
          elif ast.len > 3:
            error "too many arguments for play(<filename>)", ast
          result.add internalIdent
          let chanRef = gameCallConv(idstr, internalIdent, ast[0][0])
          let wavIdent = genSym(nskVar, "wavData")
          let wavFilePath = ast[1]
          let playargsBlock = nnkStmtList.newTree()
          playArgsBlock.add quote do:
            `wavIdent` = `chanRef`.loadWav(`wavFilePath`)
          if ast[^1].kind == nnkStmtList:
            # has a list of animations
            playArgsBlock.add makeSeqOfPlayArg2(ast[^1], wavIdent, newLit(chanName))
            result.add playArgsBlock
          else:
            if ast.len != 2:
              error "expected a file path for play()", ast
        else:  # not a special call to audio.xxx, process normally
          for a in ast[1..^1]:
            result.add convTree(internalIdent, a)
      else:        # gamevar, character, font
        result.add gameCallConv(idstr, internalIdent, ast[0])
        for a in ast[1..^1]:
          result.add convTree(internalIdent, a)
    elif ast[0].kind == nnkIdent and ast[0].strVal == "raw": # raw block
      result = ast[1]
    elif ast[0].kind == nnkIdent and ast[0].strVal in chromaFunctions:
      result.add ast[0]
      result.add internalIdent
      for a in ast[1..^1]:
        result.add convTree(internalIdent, a)
    else: # not a call we need to convert, process it normally
      for a in ast:
        result.add convTree(internalIdent, a)
  of nnkDotExpr:
    var idstr: string = ""
    if isGameCall(idstr, ast):
      result = gameCallConv(idstr, internalIdent, ast)
    else:
      result = ast
  of AtomicNodes - {nnkIdent}:
    result = ast
  of nnkIdent:
    if ast.strVal in gamevarNames:
      result = nnkDotExpr.newTree(
        nnkDotExpr.newTree(
          internalIdent,
          ident("gamevar")),
        ast)
    else:
      result = ast
  of nnkDiscardStmt:
    result = ast
  else:
    echo "!!! failed to translate syntax tree: !!!\n", ast.toStrLit
    echo ast.lineInfo
    echo ast.treeRepr
    assert false



macro finalizeAst*() =
  discard
