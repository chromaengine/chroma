# Package

version       = "0.1.0"
author        = "ryukoposting"
description   = "A Visual Novel Engine"
license       = "BSD-3-Clause"
srcDir        = "src"



# Dependencies

requires "nim >= 1.0.6"
requires "sdl2#head"
requires "nimAES"

task example, "run example game":
  exec "nim c -r -d:chromaDevel:on -p:./src game/startup.nim"

task buildex, "build example game":
  exec "nim c --opt:speed -d:chromaDevel:on -p:./src game/startup.nim"
